# README #

### Overivew ###

* Kanji Elements is a kanji flashcard app with a focus on writing the kanji and reading the kanji in context. 

* Current Version 0.8 - The new version of the kanji elements database has NOT been installed. An update to include the new database is planned. The new database includes corrections to multiple data issues plus an extension of study-able kanji and a condensed dictionary. 


### How do I get set up? ###


* Configuration  - Using the included build-grade should include all dependencies.



* Dependencies:
* * svg-android-1.1 (included)
* * MPAndroidChart-v2.0.7
* * sqliteassethelper-2.0.1
   

### Who do I talk to? ###

* Repo owner    
  Brian Foley
  brian.foley -at- gmail.com