package com.brian.kanjielementtests;

import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.os.Bundle;
import android.util.Log;

import com.readystatesoftware.sqliteasset.SQLiteAssetHelper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * Created by Brian on 4/21/2015.
 */
public class UserDatabase extends SQLiteAssetHelper {
    private static final String DATABASE_NAME = "userdata.sqlite";
    private static final int DATABASE_VERSION = 1;
    public UserDatabase(Context context, SQLiteDatabase kanjiDB){
        super(context, DATABASE_NAME,null, DATABASE_VERSION);
        addDatabase(kanjiDB, "kanji");
    }
    public void addDatabase(SQLiteDatabase db, String as){
        String rawSql = "ATTACH '" + db.getPath() + "' AS " + as;
        this.getReadableDatabase().execSQL(rawSql);
    }
    public void clearUserData(){
        this.getReadableDatabase().execSQL("delete from user_kanji");
        this.getReadableDatabase().execSQL("delete from user_history");
    }
    public void updateKanji(Bundle newValues){
        final int id = newValues.getInt(UserSettings.KEY_ID);
        final String updateStatement = "UPDATE user_kanji";
        final String whereStatement = " WHERE _id = " + Integer.toString(id);
        final String history = newValues.getString(UserSettings.KEY_HISTORY);
        final int needs_correct = newValues.getInt(UserSettings.KEY_NEEDS_CORRECT);
        final int pending = newValues.getInt(UserSettings.KEY_PENDING);
        final int stageId = newValues.getInt(UserSettings.KEY_STAGE);
        final int intervalId = newValues.getInt(UserSettings.KEY_INTERVAL);
        final long dueDate = newValues.getLong(UserSettings.KEY_DUE_DATE);

        //Add example and svg
        String setStatement = " SET";
        boolean needsComma = false;
        if (history != null) {
            needsComma = true;
            setStatement += " history = '" + history + "'";
        }
        if (newValues.containsKey(UserSettings.KEY_NEEDS_CORRECT)) {
            if (needsComma) setStatement += ",";
            needsComma = true;
            setStatement += " needs_correct = " + Integer.toString(needs_correct);
        }
        if (newValues.containsKey(UserSettings.KEY_PENDING)) {
            if (needsComma) setStatement += ",";
            needsComma = true;
            setStatement += " pending = " + Integer.toString(pending);
        }
        if (newValues.containsKey(UserSettings.KEY_STAGE)){
            if (needsComma) setStatement += ",";
            needsComma = true;
            setStatement += " stage_id = " + Integer.toString(stageId);
        }
        if (newValues.containsKey(UserSettings.KEY_INTERVAL)){
            if (needsComma) setStatement += ",";
            needsComma = true;
            setStatement += " interval_id = " + Integer.toString(intervalId);
        }
        if (newValues.containsKey(UserSettings.KEY_DUE_DATE)){
            if (needsComma) setStatement += ",";
            //needsComma = true;
            setStatement += " due_date = " + Long.toString(dueDate);
        }
        Log.i("MyActivity", "UPDATE SQL: " + updateStatement + setStatement + whereStatement);
        this.getWritableDatabase().execSQL(updateStatement + setStatement + whereStatement);
    }
    public void insertKanji(Bundle values){
        final String insertStatement = "INSERT INTO user_kanji ";
        String columns = "(_id,";
        String valueString = " VALUES (" + Integer.toString(values.getInt(UserSettings.KEY_ID)) + ",";

        if(values.containsKey(UserSettings.KEY_PENDING)) {
            columns += "pending,";
            valueString += Integer.toString(values.getInt(UserSettings.KEY_PENDING)) + ",";
        }
        if(values.containsKey(UserSettings.KEY_SVG)) {
            columns += "svg,";
            valueString += "'" + values.getString(UserSettings.KEY_SVG) + "',";
        }
        if(values.containsKey(UserSettings.KEY_EXAMPLE)) {
            columns += "example,";
            valueString += "'" + values.getString(UserSettings.KEY_EXAMPLE) + "',";
        }
        if(values.containsKey(UserSettings.KEY_HEISIG)) {
            columns += "heisig,";
            valueString += Integer.toString(values.getInt(UserSettings.KEY_HEISIG)) + ",";
        }
        columns = columns.substring(0,columns.length()-1) + ")";
        valueString = valueString.substring(0,valueString.length() -1) + ")";
        Log.i("UserDatabase.java", "INSERT KANJI: " + insertStatement + columns + valueString);
        this.getWritableDatabase().execSQL(insertStatement + columns + valueString);
    }
    public void insertHistoryEntry(long date){
        try {
            String[] args = new String[]{Long.toString(date)};
            Cursor c = this.getReadableDatabase().rawQuery("SELECT date FROM user_history WHERE date = ?", args);
            Log.i("UserDatabase", "HistoryEntry Cursor: " + DatabaseUtils.dumpCursorToString(c));
            final boolean found = c.getCount() > 0;
            c.close();
            if(!found)
                this.getWritableDatabase().execSQL("INSERT INTO user_history (date, history_json) VALUES (" + Long.toString(date) + ", '{}')");

        } catch (SQLiteException e ){
            e.printStackTrace();
        }
    }
    public void updateHistoryEntry(long date, String history_json){
        try {
            this.getWritableDatabase().execSQL("UPDATE user_history SET history_json = '" + history_json +"' WHERE date = " + Long.toString(date) );
        } catch (SQLiteException e){
            e.printStackTrace();
        }
    }
    public JSONObject makeJsonFromUserDb(){
        JSONObject output = new JSONObject();
        try{

            JSONArray user_kanji = new JSONArray();
            JSONObject row;
            Cursor c = this.getReadableDatabase().rawQuery("SELECT _id, due_date, needs_correct, pending, history, stage_id, interval_id FROM user_kanji", null);
            c.moveToFirst();
            final int id = c.getColumnIndex(DatabaseQuery.COL_USER_KANJI_ID);
            final int due_date = c.getColumnIndex("due_date");
            final int needs_correct = c.getColumnIndex(DatabaseQuery.COL_USER_KANJI_NEEDS_CORRECT);
            final int pending = c.getColumnIndex(DatabaseQuery.COL_USER_KANJI_PENDING);
            final int history = c.getColumnIndex(DatabaseQuery.COL_USER_KANJI_HISTORY);
            final int stage = c.getColumnIndex(DatabaseQuery.COL_USER_KANJI_STAGE);
            final int interval = c.getColumnIndex(DatabaseQuery.COL_USER_KANJI_INTERVAL);
            Calendar date = Calendar.getInstance();
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");

            while(!c.isAfterLast()){
                row = new JSONObject();
                row.put(DatabaseQuery.COL_USER_KANJI_ID, c.getInt(id));
                //date.setTimeInMillis(c.getLong(due_date));
                //row.put(DatabaseQuery.COL_USER_KANJI_DUE_DATE, format.format(date.getTime()));
                row.put(DatabaseQuery.COL_USER_KANJI_DUE_DATE, c.getLong(due_date));
                row.put(DatabaseQuery.COL_USER_KANJI_NEEDS_CORRECT, c.getInt(needs_correct));
                row.put(DatabaseQuery.COL_USER_KANJI_PENDING, c.getInt(pending));
                row.put(DatabaseQuery.COL_USER_KANJI_HISTORY, c.getString(history));
                row.put(DatabaseQuery.COL_USER_KANJI_INTERVAL, c.getInt(interval));
                row.put(DatabaseQuery.COL_USER_KANJI_STAGE, c.getInt(stage));
                user_kanji.put(row);
                c.moveToNext();
            }
            c.close();

            JSONArray user_history = new JSONArray();
            c = this.getReadableDatabase().rawQuery("SELECT date, history_json FROM user_history", null);
            c.moveToFirst();
            while(!c.isAfterLast()){
                row = new JSONObject();
                row.put("date", c.getLong(0));
                row.put("history_json", c.getString(1));
                user_history.put(row);
                c.moveToNext();
            }
            c.close();

            output.put("user_kanji", user_kanji);
            output.put("user_history", user_history);

        } catch (Exception e){
            e.printStackTrace();
        }
        return output;
    }
    public boolean restoreUserDBfromJSON(JSONObject input){
        try{
            JSONArray user_history = input.getJSONArray("user_history");
            JSONArray user_kanji = input.getJSONArray("user_kanji");
            final int history_size = user_history.length();
            final int kanji_size = user_kanji.length();

            clearUserData();
            JSONObject row;
            for (int i = 0; i < history_size; i ++){
                row = user_history.getJSONObject(i);
                this.getWritableDatabase().execSQL("INSERT INTO user_history (date, history_json) VALUES (?,?)",
                        new String[] {Long.toString(row.getLong("date")),row.getString("history_json")});
            }
            for(int i = 0; i < kanji_size; i ++){
                row = user_kanji.getJSONObject(i);
                String[] args = new String[]{
                        Integer.toString(row.getInt("_id")),
                        Long.toString(row.getLong("due_date")),
                        Integer.toString(row.getInt("needs_correct")),
                        Integer.toString(row.getInt("pending")),
                        row.getString("history"),
                        Integer.toString(row.getInt("stage_id")),
                        Integer.toString(row.getInt("interval_id"))
                                };

                this.getWritableDatabase().execSQL("INSERT INTO user_kanji (_id, due_date, needs_correct, pending, history, stage_id, interval_id) VALUES (?,?,?,?,?,?,?)", args);

            }
            return true;
        } catch (Exception e ){
            e.printStackTrace();
            return false;
        }
    }

}
