package com.brian.kanjielementtests;

import android.content.Context;
import android.database.Cursor;
import android.os.AsyncTask;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ProgressBar;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;

/**
 * Created by BRIAN on 2/28/2015.
 */
public abstract class EndlessSimpleCursorAdapter extends SimpleCursorAdapter implements AbsListView.OnScrollListener{
    private String[] mFrom;
    private int[] mTo;
    private int mLayout;
    private Context mContext;

    private Cursor cursor;
    private int cursorCount = 0;
    private boolean mDataValid;


    private int fromCol[];

    private static final int VIEW_TYPE_LOADING = 2;
    private static final int VIEW_TYPE_ACTIVITY = 1;
    private static final int VIEW_TYPE_BLANK = 0;
    private static final int SERVER_LIST_RESET = -1;

    private int visibleThreshold;
    private int limit;
    private int currentOffset = 0;
    private int serverListSize = -1;
    private boolean isLoading;

    //public EndlessScrollListener onScrollListener;


    private static class ViewHolder{
        TextView[] textViews;
        ProgressBar progressBar;
        ParseJSONTask task;
        /*TextView kanjiText;
        TextView rdngKunText;
        TextView rdngOnText;
        TextView meaningText;*/
    }

    public EndlessSimpleCursorAdapter(Context context, int layout, Cursor c, String[] from, int[] to, int threshold, int listSize){
        super(context, layout, c, from, to, SimpleCursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER);
        mContext = context;
        mLayout = layout;
        isLoading = c == null;
        cursor = c;
        mDataValid = c != null;
        currentOffset = 0;
        visibleThreshold = threshold;
        limit = listSize;
        mFrom = from;
        mTo = to;

        if (mDataValid) {
            cursorCount = c.getCount();
            final int length = from.length;
            fromCol = new int[length];
            for(int i = 0; i < length; i ++){
                fromCol[i] = cursor.getColumnIndex(from[i]);
            }
        }
        else loadCursor(currentOffset, limit);

    }

    public void setServerListSize(int size){
        serverListSize = size;
    }

    @Override
    public int getCount(){
        if (!mDataValid) return 0;
        if (currentOffset > 0) return cursorCount +3;
        return cursorCount + 2;
    }

    @Override
    public boolean isEnabled(int position){
        return getItemViewType(position) == VIEW_TYPE_ACTIVITY;
    }

    @Override
    public int getItemViewType(int position){
        if (position == 0) return VIEW_TYPE_BLANK;
        else if (position > cursorCount || position == 1 && currentOffset > 0) return VIEW_TYPE_LOADING;
        else return VIEW_TYPE_ACTIVITY;
    }

    @Override
    public void changeCursor(Cursor cursor){
        //setServerListSize(SERVER_LIST_RESET);
        super.changeCursor(cursor);
    }

    public void nextCursor(Cursor cursor, int offset){
        currentOffset = offset;
        super.changeCursor(cursor);
    }

    @Override
    public Cursor swapCursor(Cursor cursor){
        isLoading = cursor == null;
        mDataValid = cursor != null;
        this.cursor = cursor;
        if (mDataValid) {
            cursorCount = cursor.getCount();
            final int length = mFrom.length;
            fromCol = new int[length];
            for(int i = 0; i < length; i ++){
                fromCol[i] = cursor.getColumnIndexOrThrow(mFrom[i]);
            }
        }
        return super.swapCursor(cursor);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent){
        if (!mDataValid){
            throw new IllegalStateException("this should only be called when the cursor is valid");
        }
        View v;
        if (convertView == null) v = newView(mContext, parent);
        else v = convertView;
        switch(getItemViewType(position)){
            case VIEW_TYPE_BLANK:
                bindBlankView(v, mContext);
                break;
            case VIEW_TYPE_LOADING:
                bindProgressView(v, mContext);
                break;
            case VIEW_TYPE_ACTIVITY:
            default:
                cursor.moveToPosition(position - 1);
                bindView(v, mContext, cursor);
                break;
        }
        return v;
    }
    public void bindBlankView(View v, Context context){
        ViewHolder viewHolder = (ViewHolder) v.getTag();
        final int length = viewHolder.textViews.length;
        viewHolder.progressBar.setVisibility(View.GONE);
        for (int i = 0; i < length; i ++){
            viewHolder.textViews[i].setVisibility(View.INVISIBLE);
        }
    }
    public void bindProgressView(View v, Context context){
        ViewHolder viewHolder = (ViewHolder) v.getTag();
        final int length = viewHolder.textViews.length;
        viewHolder.progressBar.setVisibility(View.VISIBLE);
        for (int i = 0; i < length; i ++){
            viewHolder.textViews[i].setVisibility(View.GONE);
        }
    }
    @Override
    public void bindView(View v, Context context, Cursor cursor){
        ViewHolder viewHolder = (ViewHolder) v.getTag();
        final int length = viewHolder.textViews.length;
        viewHolder.progressBar.setVisibility(View.GONE);
        for(int i = 0; i < length; i ++){
            TextView textView = viewHolder.textViews[i];
            textView.setVisibility(View.VISIBLE);
            textView.setText("");
        }
        viewHolder.textViews[0].setText(cursor.getString(fromCol[0]));
        String rdng = cursor.getString(fromCol[1]);
        String meaning = cursor.getString(fromCol[2]);
        if (viewHolder.task != null) {
            viewHolder.task.cancel(true);
        }
        viewHolder.task = new ParseJSONTask(viewHolder);
        viewHolder.task.execute(rdng,meaning);
    }

    private final class ParseJSONTask extends AsyncTask<String, Void, String[]> {
        ViewHolder viewHolder;
        public ParseJSONTask(ViewHolder viewHolder){
            this.viewHolder = viewHolder;
        }
        protected String[] doInBackground(String... strings){
            String[] stringList = new String[3];
            stringList[0] = ParseJSON.rdngToString(strings[0], ParseJSON.RDNG_ON);
            stringList[1] = ParseJSON.rdngToString(strings[0], ParseJSON.RDNG_KUN);
            stringList[2] = ParseJSON.meaningToString(strings[1]);
            return stringList;
        }
        protected void onPostExecute(String[] result){
            final int length = result.length;
            for( int i = 0; i < length; i ++){
                viewHolder.textViews[i+1].setText(result[i]);
            }
        }
    }

    public View newView(Context context, ViewGroup parent){
        final LayoutInflater inflater = LayoutInflater.from(context);
        final int length = mTo.length;
        View v = inflater.inflate(mLayout, parent, false);
        ViewHolder viewHolder = new ViewHolder();
        viewHolder.textViews = new TextView[length];
        viewHolder.progressBar = (ProgressBar) v.findViewById(R.id.progressBar);
        for (int i = 0; i < length; i ++){
            viewHolder.textViews[i] = (TextView) v.findViewById(mTo[i]);
        }
        v.setTag(viewHolder);
        return v;
    }

    @Override
    public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount){
        //Check if loading data, if not then check upper and lower bounds
        //Log.i("EndlessCursor", "Server list size: " + serverListSize);
        //Log.i("EndlessCursor", "CurrentOffset: " + currentOffset);
        if(!isLoading){
            //get previous data set
            //Check if first item is in threshold AND if currentOffset > 0 - beginning of list
            if(firstVisibleItem <= visibleThreshold && currentOffset > 0) {
                currentOffset = currentOffset - limit/2;
                if (currentOffset < 0){
                    currentOffset = 0;
                }
                loadCursor(currentOffset, limit);
                isLoading = true;
            }
            // get next data set
            //Check if last item is in threshold && currentOffset < serverListSize



            if((firstVisibleItem + visibleItemCount) >= (totalItemCount - visibleThreshold) && currentOffset+limit < serverListSize ) {
                Log.i("EndlessCursor", "firstItem: " + firstVisibleItem );
                Log.i("EndlessCursor", "visibleItemCount: " + visibleItemCount);
                Log.i("EndlessCursor", "totalItemCount: " + totalItemCount);
                Log.i("EndlessCursor", "visibleThreshold: " + visibleThreshold);
                currentOffset = currentOffset + limit/2;
                loadCursor(currentOffset, limit);
                isLoading = true;
            }
        }
    }
    @Override
    public void onScrollStateChanged(AbsListView View, int scrollState){}

    public abstract void loadCursor(int offset, int limit);

    /*
    public abstract static class EndlessScrollListener implements AbsListView.OnScrollListener{


        private boolean isLoading;
        private int currentOffset;


        public EndlessScrollListener(int visibleThreshold, int listSize){
            this.visibleThreshold = visibleThreshold;
            this.limit = listSize;
            this.currentOffset = 0;
        }





        public abstract boolean getLoadingState();
        public abstract int getCurrentOffset();

    }*/
}
