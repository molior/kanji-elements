package com.brian.kanjielementtests;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.text.style.ReplacementSpan;

import java.util.Arrays;

/**
 * Created by Brian on 1/28/2015.
 */
public class FuriganaSpan extends ReplacementSpan {

    CharSequence mText;
    private float mFontSize = 0.0f;
    //Font of furigana must be at least 4.0f
    static float MIN_FONT_SIZE = 9.0f;
    //Font of furigana must be 0.7 or less of text it is placed over
    static float MAX_FONT_RATIO = 0.5f;
    //Font change step size
    static float STEP_SIZE = 0.1f;
    //Font use standard font ratio of one half text size
    static float FONT_RATIO = 0.5f;

    private boolean visible;
    private boolean alwaysVisible;
    private boolean isUnderlined;
    private boolean isShown;
    private char mReplacement;
    private CharSequence savedText;

    private Object drawLock = new Object();

    public FuriganaSpan(CharSequence text, Boolean isVisible){
        super();
        savedText = text;
        mText = text;
        visible = isVisible;
        alwaysVisible = false;
        this.isUnderlined = true;
    }
    public FuriganaSpan(CharSequence text, Boolean isVisible, Boolean isUnderlined){
        super();
        savedText = text;
        mText = text;
        visible = isVisible;
        alwaysVisible = false;
        this.isUnderlined = isUnderlined;
    }
    @Override
    public int getSize (Paint paint, CharSequence text, int start, int end, Paint.FontMetricsInt fm){
        //if (mFontSize == 0.0f) mFontSize = calculateFontSize(paint, text, start, end);
        mFontSize = paint.getTextSize() * FONT_RATIO;
        //mFontSize = paint.getTextSize();
        int actualEnd = end;
        final char replacement = getReplacement();
        for (int i = end - 1; i >= start; i --){
            final char c = text.charAt(i);
            if (Character.UnicodeBlock.of(c) == Character.UnicodeBlock.CJK_UNIFIED_IDEOGRAPHS ||
                    c == replacement)
            {
                actualEnd = i + 1;
                break;
            }
        }

        final int width =(int) paint.measureText(text,start,actualEnd);
        final Paint mPaint = new Paint(paint);
        mPaint.setTextSize(mFontSize);
        final int mWidth = (int) mPaint.measureText(mText, 0, mText.length());
        if (fm != null) {
            Paint.FontMetricsInt mFm = new Paint.FontMetricsInt();
            mPaint.getFontMetricsInt(mFm);
            paint.getFontMetricsInt(fm);
            fm.ascent = fm.ascent + mFm.ascent;
            fm.top = fm.ascent + mFm.ascent;

        }

        return Math.max(width,mWidth) + (int) paint.measureText(text,actualEnd, end);
    }
    @Override
    public void draw(Canvas canvas, CharSequence text, int start, int end, float x, int top, int y, int bottom, Paint paint) {
        //draw the frame with custom Paint
        mFontSize = paint.getTextSize()*FONT_RATIO;
        //if (mFontSize == 0.0f) mFontSize = calculateFontSize(paint, text, start, end);
        //find last kanji in text

        int actualEnd = end;
        final char replacement = getReplacement();
        for (int i = end - 1; i >= start; i --){
            final char c = text.charAt(i);
            if (Character.UnicodeBlock.of(c) == Character.UnicodeBlock.CJK_UNIFIED_IDEOGRAPHS ||
                    c == replacement)
            {
                actualEnd = i + 1;
                break;
            }
        }
        final float width = paint.measureText(text,start,actualEnd);
        final float extraWidth = paint.measureText(text,actualEnd, end);
        final CharSequence actualText = text.subSequence(start,actualEnd);
        final Paint mPaint = new Paint(paint);
        mPaint.setTextSize(mFontSize);
        final float mWidth = mPaint.measureText(mText, 0, mText.length());
        final float mY = y + paint.getFontMetrics().ascent;
        final float mActualSpace = (width >= mWidth) ? (width /(float)mText.length()) : mWidth/(float)mText.length();
        //final float mX = (width >= mWidth) ? x + (width / 2f - mWidth / 2f) : x;
        final float actualSpace = (mWidth > width) ? (mWidth/(float)actualText.length()) : width/(float)actualText.length();
        /*if (width >= mWidth) mX += width / 2 - mWidth / 2;
        else {
            actualSpace = (mWidth - width)/(actualText.length());
        }*/
        //Draw underline
        //TODO add checks for kanji in string then center over kanji(or fix in data)
        //TODO Fix issue with over lap with next or previous word (rounding errors?)
        final Paint linePaint = new Paint();
        synchronized (drawLock) {
            linePaint.setColor(paint.getColor());
            linePaint.setStrokeWidth(3f);
            linePaint.setStrokeCap(Paint.Cap.SQUARE);
            if(isUnderlined)
                canvas.drawLine(x+2, y + (paint.getFontMetrics().descent), x + Math.max(mWidth, width) + extraWidth - 4, y + (paint.getFontMetrics().descent), linePaint);

            final int count = actualText.length();
            for (int i = 0; i < count; i++) {
                final float textWidth = paint.measureText(actualText, i, i + 1);
                final float halfFreeSpace = (actualSpace - textWidth) / 2f;
                canvas.drawText(actualText, i, i + 1, x + i * (actualSpace) + halfFreeSpace, y, paint);
            }
            //canvas.drawText(text, start, end, xCopy, y, paint);
            if (visible) {
                final int length = mText.length();
                for (int i = 0; i < length; i++) {
                    final float textWidth = mPaint.measureText(mText, i, i + 1);
                    final float halfFreeSpace = (mActualSpace - textWidth) / 2f;
                    canvas.drawText(mText, i, i + 1, x + i * (mActualSpace) + halfFreeSpace, mY, mPaint);
                }
            }
            if (actualEnd < end){
                canvas.drawText(text,actualEnd,end,x + Math.max(mWidth, width), y, paint);
            }
        }
    }
    /*
    private float calculateFontSize(Paint paint, CharSequence text, int start, int end){
        float fontSize = MAX_FONT_RATIO*paint.getTextSize();
        final Paint mPaint = new Paint(paint);
        final float width = paint.measureText(text, start, end);
        mPaint.setTextSize(fontSize);
        while(mPaint.measureText(mText, 0, mText.length()) > width){
            fontSize -= paint.getTextSize()*STEP_SIZE;
            mPaint.setTextSize(fontSize);
        }
        if (fontSize < MIN_FONT_SIZE) fontSize = MIN_FONT_SIZE;
        return fontSize;
    }*/
    /*
    private float calculateMaxWidth(Paint paint, CharSequence text){
        float width = paint.measureText(text,0,1);
        final int count = text.length();
        for (int i = 0; i< count; i++){
            width = Math.max(paint.measureText(text, i, i + 1), width);
        }
        return width;
    }*/

    public void flipVisibility(){
        visible = (visible) ? false : true;
        if (alwaysVisible){
            visible = true;
        }
    }
    public void setVisibility(boolean isVisible){
        visible = isVisible;
        if (alwaysVisible){
            visible = true;
        }
    }
    public void setAlwaysVisible(boolean isAlwaysVisible){
        alwaysVisible = isAlwaysVisible;
        if (alwaysVisible) visible = true;
    }
    public void setReplacement(char replacement){
        mReplacement = replacement;
    }
    public char getReplacement(){
        return mReplacement;
    }
    public void replaceFurigana(){
        mText = createString(mReplacement,mText.length());
    }
    public void restoreFurigana(){
        mText = savedText;
    }
    public boolean getVisibility(){ return visible;}
    private static String createString(char character, int length) {
        char[] chars = new char[length];
        Arrays.fill(chars, character);
        return new String(chars);
    }
}
