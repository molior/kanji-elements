package com.brian.kanjielementtests;

import android.app.Activity;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.app.Fragment;
import android.os.IBinder;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.nio.charset.Charset;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link LookupBar.OnFragmentInteractionListener} interface
 * to handle interaction events.
 */
public class LookupBar extends Fragment implements View.OnClickListener, View.OnFocusChangeListener{

    private OnFragmentInteractionListener mListener;

    public LookupBar() {
        // Required empty public constructor
    }

    private LinearLayout baseLayout;
    private ImageButton modeButton;
    private FrameLayout focusHolder;
    private TextView textField;

    private View.OnKeyListener onSoftKeyboardDonePress =  new View.OnKeyListener(){
        @Override
        public boolean onKey(View view, int i, KeyEvent keyEvent) {
            if(keyEvent.getKeyCode() == KeyEvent.KEYCODE_ENTER){
                focusHolder.requestFocus();
            }
            return false;
        }
    };
    private TextWatcher textWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            //Determine type of text (kanji / reading / meaning)
        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {

        }

        @Override
        public void afterTextChanged(Editable editable) {
             mListener.updateDictionaryCursor(editable);

        }
    };


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        baseLayout = (LinearLayout) inflater.inflate(R.layout.lookup_field, container, false);
        textField = (TextView) baseLayout.findViewById(R.id.lookupTextField);
        textField.setOnClickListener(this);
        textField.setOnFocusChangeListener(this);
        textField.setOnKeyListener(onSoftKeyboardDonePress);
        textField.addTextChangedListener(textWatcher);
        modeButton = (ImageButton) baseLayout.findViewById(R.id.lookupModeButton);
        modeButton.setOnClickListener(this);
        focusHolder = (FrameLayout) baseLayout.findViewById(R.id.lookupFocusHolder);
        return baseLayout;
    }

    @Override
    public void onClick(View view) {
        final int id = view.getId();
        switch(id){
            case R.id.lookupTextField:
                mListener.addDictionaryFragment();
                break;
        }
    }

    @Override
    public void onFocusChange(View view, boolean b) {
        final int id = view.getId();

        switch(id){
            case R.id.lookupTextField:
                if (b)
                    mListener.addDictionaryFragment();
                else {
                    InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                }
                break;
        }


    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {

        public void addDictionaryFragment();
        public void updateDictionaryCursor(CharSequence searchQuery);
    }

}
