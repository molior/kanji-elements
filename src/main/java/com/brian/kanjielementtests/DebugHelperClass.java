package com.brian.kanjielementtests;

import android.app.Activity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.PopupWindow;
import android.widget.Spinner;

/**
 * Created by Brian on 3/18/2015.
 */
public class DebugHelperClass implements View.OnClickListener,
        AdapterView.OnItemSelectedListener {
    MyActivity mActivity;
    ArrayAdapter<Integer> intervalAdapter;
    public DebugHelperClass (MyActivity activity){
        mActivity = activity;
    }
    public void setIntervalAdapter(ArrayAdapter<Integer> arrayAdapter){
        intervalAdapter = arrayAdapter;
    }
    @Override
    public void onClick(View view) {
        final int id = view.getId();
        switch(id){
            case R.id.debugPopup_buttonOk:
                mActivity.closePopup(true);
                break;
            case R.id.debugPopup_buttonCancel:
                mActivity.closePopup(false);
                break;
        }

    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int pos, long itemId) {
        final int id = adapterView.getId();
        switch(id){
            case R.id.spinnerStage:
                intervalAdapter.clear();
                int stageId = (Integer) adapterView.getItemAtPosition(pos);
                UserSettings.Stage stage = UserSettings.getInstance().getStage(stageId);
                final int length = stage.intervals.length;
                for (int i = 0; i < length; i ++){
                    intervalAdapter.add(stage.intervals[i]);
                }
                mActivity.cardSettingsStage(stageId);
                if (stageId == 0) mActivity.cardSettingsInterval(0);
                break;
            case R.id.spinnerInterval:
                mActivity.cardSettingsInterval(pos);
                break;
            case R.id.spinnerHistory:
                mActivity.cardSettingsHistory((String) adapterView.getItemAtPosition(pos));
                break;
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }
}

