package com.brian.kanjielementtests;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.DashPathEffect;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PathEffect;
import android.graphics.PorterDuff;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Brian on 1/30/2015.
 */
public class DrawingCanvas extends View {

    private List<Path> mPathList = new ArrayList<Path>();
    private Paint mPaint;
    private Canvas mCanvas;
    private Bitmap mBitmap;
    private Paint mBitmapPaint;
    private Path mPath;
    private Object redrawLock = new Object();

    public DrawingCanvas(Context context, AttributeSet attr){
        super(context, attr);

        mPaint = new Paint();
        mPaint.setColor(getResources().getColor(R.color.draw_canvas_stroke));
        mPaint.setStyle(Paint.Style.STROKE);
        mPaint.setStrokeCap(Paint.Cap.ROUND);
        mPaint.setStrokeJoin(Paint.Join.ROUND);
        mPaint.setStrokeWidth(8f);
        mPaint.setAntiAlias(true);
        mBitmapPaint = new Paint();
        mBitmapPaint.setDither(true);
        mPath = new Path();
    }
    private void createBackground(){
        mBitmap = Bitmap.createBitmap(getHeight(), getWidth(), Bitmap.Config.ARGB_8888);
        mCanvas = new Canvas(mBitmap);
        final Paint paint = new Paint(mPaint);
        paint.setColor(getResources().getColor(R.color.kanji_background_lines));
        paint.setStrokeWidth(4f);
        final PathEffect pathEffect = new DashPathEffect(new float[]{10f, 5f},0);
        paint.setPathEffect(pathEffect);
        mCanvas.drawColor(Color.WHITE);
        mCanvas.drawLine(getWidth()/2, 0, getWidth()/2, getHeight(), paint);
        mCanvas.drawLine(0,getHeight()/2, getWidth(),getHeight()/2, paint);
    }
    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh){
        super.onSizeChanged(w, h, oldw, oldh);
        createBackground();
    }

    @Override
    protected void onDraw(Canvas canvas){
        if (mBitmap == null){createBackground();}
        //mCanvas.drawPath(mPath,mPaint);
        canvas.drawBitmap(mBitmap,0,0, mBitmapPaint);

        canvas.drawPath(mPath,mPaint);

    }

    private float mX, mY;
    private static final float TOUCH_TOLERANCE = 4;

    private void touch_start(float x, float y) {

        mPath.reset();
        mPath.moveTo(x, y);
        mX = x;
        mY = y;
    }
    private void touch_move(float x, float y) {
        float dx = Math.abs(x - mX);
        float dy = Math.abs(y - mY);
        if (dx >= TOUCH_TOLERANCE || dy >= TOUCH_TOLERANCE) {
            mPath.quadTo(mX, mY, (x + mX)/2, (y + mY)/2);
            mX = x;
            mY = y;
        }
    }
    private void touch_up() {
        mPath.lineTo(mX, mY);
        // commit the path to our offscreen
        mCanvas.drawPath(mPath, mPaint);
        // kill this so we don't double draw
        final Path path = new Path(mPath);
        mPathList.add(path);
        mPath.reset();
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        float x = event.getX();
        float y = event.getY();

        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                touch_start(x, y);
                postInvalidate();
                break;
            case MotionEvent.ACTION_MOVE:
                touch_move(x, y);
                postInvalidate();
                break;
            case MotionEvent.ACTION_UP:
                touch_up();
                postInvalidate();
                break;
        }
        return true;
    }

    public void undoLast(){

        final int count = mPathList.size();
        if (count == 0) return;
        mPathList.remove(count-1);
        synchronized (redrawLock){
            redrawPathListLocked();
        }
        invalidate();
    }
    private void redrawPathListLocked(){
        createBackground();
        final int count = mPathList.size();
        for (int i = 0; i < count; i ++){
            final Path path = mPathList.get(i);
            mCanvas.drawPath(path,mPaint);

        }
    }
    public void clearCanvas(){
        mPathList.clear();

        //if (getWidth() > 0 && getHeight() > 0) {
        createBackground();
        invalidate();
        //}
        Log.i("DrawingCanvas", "canvas cleared.");
    }
    public Bitmap getBitmap(){
        final Paint paint = new Paint(mPaint);
        paint.setColor(getResources().getColor(R.color.kanji_background_lines));
        paint.setStrokeWidth(4f);
        mCanvas.drawRect(0,0,getWidth(),getHeight(),paint);
        mCanvas.drawPath(mPath, mPaint);
        return mBitmap;
    }
    public List<Path> getPaths(){
        return mPathList;
    }

}
