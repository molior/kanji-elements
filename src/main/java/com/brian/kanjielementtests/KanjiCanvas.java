package com.brian.kanjielementtests;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.animation.TimeInterpolator;
import android.animation.ValueAnimator;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.DashPathEffect;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PathEffect;
import android.graphics.PorterDuff;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;
//import android.support.annotation.NonNull;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.LinearInterpolator;
import android.view.animation.OvershootInterpolator;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Brian on 1/14/2015.
 */
public class KanjiCanvas extends View {

    public enum AnimationState {
        ANIMATE_STROKE, STEP_STROKE, ANIMATION_RUNNING
    }

    private static float KANJI_BASE_SIZE = 109.0f;
    private static float STROKE_WIDTH = 10f;
    private float mBaseSize;

    private List<KanjiPath> kanjiPaths;// = new ArrayList<KanjiPath>();
    private List<Animator> animations;// = new ArrayList<ObjectAnimator>();
    private final Object drawLock = new Object();

    private float canvasScale;

    private int emptyAnimationCount;

    private long timeStamp;
    private static long delayAfterLastAnim = 500;

    private Canvas mCanvas;
    private Bitmap mBitmap;
    private Bitmap mStrokeBitmap;
    private Canvas mStrokeCanvas;
    private Paint mPaint;


    private long mDuration = 800;
    private long mDelayBuffer = 200;
    private long mStepDuration = 100;
    private long mStepDelay = 0;







    public KanjiCanvas(Context context, AttributeSet attr){
        super(context, attr);
        kanjiPaths = new ArrayList<KanjiPath>();
        animations = new ArrayList<Animator>();
        mPaint = new Paint(Paint.DITHER_FLAG);
        emptyAnimationCount = 0;
        canvasScale = 1.0f;
        mBaseSize = KANJI_BASE_SIZE;

    }
    public void setKanjiPaths(List<KanjiPath> newKanjiPaths, float baseSize){
        mBaseSize = baseSize;
        setKanjiPaths(newKanjiPaths);
    }
    public void setKanjiPaths(List<KanjiPath> newKanjiPaths){
        kanjiPaths.clear();
        emptyAnimationCount = 0;
        final int count = newKanjiPaths.size();

        //mCanvas.drawColor(Color.WHITE, PorterDuff.Mode.CLEAR);

        if (mStrokeCanvas != null){
            mStrokeCanvas.drawColor(Color.WHITE, PorterDuff.Mode.CLEAR);
            canvasScale =  Math.min(getHeight(), getWidth()) / mBaseSize;
        }

        for (int i = 0; i < count; i ++){

            KanjiPath kanjiPath = new KanjiPath(newKanjiPaths.get(i));
            kanjiPaths.add(kanjiPath);
            kanjiPath.setCallback(this);
            if (mStrokeCanvas != null) {
                kanjiPath.scale(canvasScale);
                kanjiPath.setStrokeWidth(STROKE_WIDTH);
                kanjiPath.drawBackground(mStrokeCanvas);
            }
        }
        //forces view to redraw itself
        invalidate();
    }
    private void createBackground(){

    }

    private void setupCanvas(){

        final int count = kanjiPaths.size();

        canvasScale =  Math.min(getHeight(), getWidth()) / mBaseSize;

        mStrokeBitmap = Bitmap.createBitmap(getHeight(), getWidth(), Bitmap.Config.ARGB_8888);
        mStrokeCanvas = new Canvas(mStrokeBitmap);

        mBitmap = Bitmap.createBitmap(getHeight(), getWidth(), Bitmap.Config.ARGB_8888);
        mCanvas = new Canvas(mBitmap);
        final Paint paint = new Paint();
        paint.setColor(getResources().getColor(R.color.kanji_background_lines));
        paint.setStrokeWidth(4f);
        paint.setAntiAlias(true);
        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeJoin(Paint.Join.ROUND);
        final PathEffect pathEffect = new DashPathEffect(new float[]{10f, 5f},0);
        mCanvas.drawColor(Color.WHITE);
        mCanvas.drawRect(0,0,getWidth(), getHeight(), paint);
        paint.setPathEffect(pathEffect);
        mCanvas.drawLine(getWidth()/2, 0, getWidth()/2, getHeight(), paint);
        mCanvas.drawLine(0,getHeight()/2, getWidth(),getHeight()/2, paint);
        //mCanvas.drawLine(KANJI_BASE_SIZE*canvasScale/2, 0, KANJI_BASE_SIZE*canvasScale/2, KANJI_BASE_SIZE*canvasScale, paint);
        //mCanvas.drawLine(0,KANJI_BASE_SIZE*canvasScale/2, KANJI_BASE_SIZE*canvasScale,KANJI_BASE_SIZE*canvasScale/2, paint);


        for (int i = 0; i < count; i ++){
            KanjiPath kanjiPath = kanjiPaths.get(i);
            kanjiPath.scale(canvasScale);
            kanjiPath.setStrokeWidth(STROKE_WIDTH);
            kanjiPath.drawBackground(mStrokeCanvas);
        }

    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh){
        super.onSizeChanged(w, h, oldw, oldh);
        setupCanvas();

    }

    @Override
    protected void onDraw(Canvas canvas) {

        final int count = kanjiPaths.size();
        if (mBitmap == null){
            setupCanvas();
        }
        canvas.drawBitmap(mBitmap, 0, 0, mPaint);
        if (animations.size() > 0){
            canvas.drawBitmap(mStrokeBitmap, 0, 0, mPaint);
        }

        synchronized (drawLock) {
            for (int i = 0; i < count; i++) {
                KanjiPath kanjiPath = kanjiPaths.get(i);
                kanjiPath.draw(canvas);

            }
        }
    }
    @Override
    public void invalidateDrawable(Drawable dr){
        invalidate();
    }

    public int getKanjiPathSize(){
        return kanjiPaths.size();
    }
    public void syncToKanjiCanvas(KanjiCanvas secondCanvas){
        resetAnimation();
        secondCanvas.resetAnimation();
        final int kanjiPathSize = kanjiPaths.size();
        final int secondPathSize = secondCanvas.getKanjiPathSize();
        if (kanjiPathSize > secondPathSize) {
            secondCanvas.setEmptyAnimationCount(kanjiPathSize - secondPathSize);
            emptyAnimationCount = 0;
        }
        else {
            emptyAnimationCount = secondPathSize - kanjiPathSize;
            secondCanvas.setEmptyAnimationCount(0);
        }
    }
    public void resetAnimation(){
        final int animationSize = animations.size();
        for (int i = 0; i < animationSize; i ++){
            Animator animator = animations.get(i);
            animator.removeAllListeners();
            animator.end();
        }
        animations.clear();
    }
    public void removeSyncToKanjiCanvas(){
        emptyAnimationCount = 0;
    }
    public void setEmptyAnimationCount(int count){
        emptyAnimationCount = count;
    }
    public void onClick(){
        final int animationSize = animations.size();
        if (animationSize > 0){
            Animator animator = animations.get(0);
            if(animator.isStarted()){
                animator.removeAllListeners();
                animator.end();
                animations.remove(animator);
            }
            else{
                animator.setDuration(mStepDuration);
                animator.setStartDelay(mStepDelay);
                animator.addListener(new Animator.AnimatorListener() {
                    @Override
                    public void onAnimationStart(Animator animator) {

                    }

                    @Override
                    public void onAnimationEnd(Animator animator) {
                        animations.remove(animator);
                        timeStamp = System.currentTimeMillis() + delayAfterLastAnim;
                    }

                    @Override
                    public void onAnimationCancel(Animator animator) {

                    }

                    @Override
                    public void onAnimationRepeat(Animator animator) {

                    }
                });

                animator.start();
            }
        }
        else if (timeStamp < System.currentTimeMillis()) {
            final int count = kanjiPaths.size();
            for (int i = 0; i < count; i++) {

                KanjiPath kanjiPath = kanjiPaths.get(i);
                if (kanjiPath.animator != null) kanjiPath.animator.cancel();
                kanjiPath.setPhase(0.0f);
                kanjiPath.animator = ObjectAnimator.ofFloat(kanjiPath, "phase", 0.0f, 1.0f);
                kanjiPath.animator.setDuration(mDuration);
                kanjiPath.animator.setStartDelay(mDelayBuffer);
                kanjiPath.animator.setInterpolator(new AccelerateDecelerateInterpolator());
                animations.add(kanjiPath.animator);

                //kanjiPath.animator.start();

            }
            for (int q = 0; q < emptyAnimationCount; q ++){
                ValueAnimator animator = ObjectAnimator.ofFloat(0f, 1f);
                animator.setStartDelay(mDelayBuffer);
                animator.setDuration(mDuration);
                animations.add(animator);
            }
            if (count > 0) {
                final ObjectAnimator firstAnimator = kanjiPaths.get(0).animator;
                firstAnimator.addListener(new Animator.AnimatorListener() {
                    @Override
                    public void onAnimationStart(Animator animator) {
                    }

                    @Override
                    public void onAnimationEnd(Animator finishedAnimator) {
                        animations.remove(finishedAnimator);
                        if (animations.size() > 0) {
                            final Animator nextAnimator = animations.get(0);
                            nextAnimator.addListener(this);
                            nextAnimator.start();
                        }
                    }

                    @Override
                    public void onAnimationCancel(Animator animator) {
                    }

                    @Override
                    public void onAnimationRepeat(Animator animator) {
                    }
                });
                firstAnimator.start();
            }
        }
        invalidate();
    }


    /*
    public void drawKanji(long duration, long delay){
        animateKanji = ObjectAnimator.ofFloat(this, "phase", 0.0F, 1.0f);
        animateKanji.setDuration(duration);
        animateKanji.setStartDelay(delay);
        animateKanji.start();
        animateKanji.addListener(animatorListener);

        }
*/


}
