package com.brian.kanjielementtests;

import android.content.Context;
import android.database.Cursor;
import android.os.AsyncTask;
import android.util.Log;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by Brian on 1/19/2015.
 */
public class KanjiCursorAdapter extends SimpleCursorAdapter {

    private static final String KANJI_TXT = "txt";
    private static final String KANJI_RDNG = "rdng";
    private static final String KANJI_MEANING = "meaning";

    private static class ViewHolder{
        TextView[] textViews;
        ProgressBar progressBar;
        /*TextView kanjiText;
        TextView rdngKunText;
        TextView rdngOnText;
        TextView meaningText;*/
        ParseJSONTask task;
    }
    private ArrayList<ParseJSONTask> taskQueue;
    private boolean isQueueRunning;
    private Context context;
    private boolean mDataValid;
    private int layout;
    private int[] to;
    private String[] from;
    private SparseArray<String[]> buffer;

    public KanjiCursorAdapter (Context context, int layout, Cursor c, String[] from, int[] to){

        super(context, layout, c, from, to, SimpleCursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER);
        taskQueue = new ArrayList<ParseJSONTask>();
        buffer = new SparseArray<String[]>();
        mDataValid = c != null;
        this.context = context;
        this.layout = layout;
        this.from = from;
        this.to = to;

    }
    @Override
    public Cursor swapCursor(Cursor newCursor){
        mDataValid = newCursor != null;
        return super.swapCursor(newCursor);
    }
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (!mDataValid) {
            throw new IllegalStateException("this should only be called when the cursor is valid");
        }
        /*if (!mCursor.moveToPosition(position)) {
            throw new IllegalStateException("couldn't move cursor to position " + position);
        }*/
        Cursor cursor = getCursor();
        View v;
        if (convertView == null) {
            v = newView(context, cursor, parent);
        } else {
            v = convertView;
        }
        bindView(v, context, cursor, position);
        return v;
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent){

        final LayoutInflater inflater = LayoutInflater.from(context);
        View v = inflater.inflate(layout, parent, false);
        ViewHolder viewHolder = new ViewHolder();
        viewHolder.textViews = new TextView[to.length];
        viewHolder.progressBar = (ProgressBar) v.findViewById(R.id.progressBar);
        for (int i = 0; i < to.length; i ++){
            viewHolder.textViews[i] = (TextView) v.findViewById(to[i]);
        }
        v.setTag(viewHolder);
        return v;
    }
    @Override
    public long getItemId(int position){
        return 0;
    }

    @Override
    public void bindView(View v, Context context, Cursor c){
    }
    public void bindView(View v, Context context, Cursor c,int position){

        final ViewHolder viewHolder = (ViewHolder) v.getTag();
        final String[] strings = buffer.get(position);
        if (strings != null){
            viewHolder.progressBar.setVisibility(View.GONE);
            for (TextView textView: viewHolder.textViews){
                textView.setVisibility(View.VISIBLE);
            }
            final int length = strings.length;
            for (int i = 0 ; i< length; i ++){
                viewHolder.textViews[i].setText(strings[i]);
            }
        }
        else {
            viewHolder.progressBar.setVisibility(View.VISIBLE);
            for (TextView textView : viewHolder.textViews) {
                textView.setVisibility(View.GONE);
            }
            if (viewHolder.task != null) {
                viewHolder.task.cancel(true);
            }
            viewHolder.task = new ParseJSONTask(viewHolder, c, position, from);
            viewHolder.task.execute();
        }
        //taskQueue.add(viewHolder.task);
        //if(!isQueueRunning) processQueue();

        /*
        int kanjiCol = c.getColumnIndex("txt");
        int rdngCol = c.getColumnIndex("rdng");
        int meaningCol = c.getColumnIndex("meaning");

        final String kanji = c.getString(kanjiCol);
        final String rdngJSON = c.getString(rdngCol);
        final String meaningJSON = c.getString(meaningCol);
        */


        //String rdngOn = ParseJSON.rdngToString(c.getString(rdngCol), ParseJSON.RDNG_ON);
        //String rdngKun = ParseJSON.rdngToString(c.getString(rdngCol), ParseJSON.RDNG_KUN);
        //String meaning = ParseJSON.meaningToString(c.getString(meaningCol));

        /*
        if (kanji != null) {
            viewHolder.kanjiText.setText(kanji);
        }
        */
    }
/*
    private void setViews(View v, Cursor c){
                ViewHolder viewHolder = (ViewHolder) v.getTag();

                if (viewHolder.task != null){
                    viewHolder.task.cancel(true);

                }

                int kanjiCol = c.getColumnIndex("txt");
                int rdngCol = c.getColumnIndex("rdng");
                int meaningCol = c.getColumnIndex("meaning");

                final String kanji = c.getString(kanjiCol);
                final String rdngJSON = c.getString(rdngCol);
                final String meaningJSON = c.getString(meaningCol);

                viewHolder.task = new ParseJSONTask(viewHolder).execute(rdngJSON, meaningJSON);

                //String rdngOn = ParseJSON.rdngToString(c.getString(rdngCol), ParseJSON.RDNG_ON);
                //String rdngKun = ParseJSON.rdngToString(c.getString(rdngCol), ParseJSON.RDNG_KUN);
                //String meaning = ParseJSON.meaningToString(c.getString(meaningCol));


                if (kanji != null) {
                    viewHolder.kanjiText.setText(kanji);
                }

    }
    */
    private final class ParseJSONTask extends AsyncTask<Void, Void, String[]>{

        ViewHolder mView;
        Cursor mCursor;
        int mPosition;
        String[] mFrom;

        public ParseJSONTask(ViewHolder view, Cursor cursor, int position, String[] from){
            mView = view;
            mCursor = cursor;
            mPosition = position;
            mFrom = from;
        }

        protected String[] doInBackground(Void... voids){
            final int length = to.length;
            final String[] list = new String[length];
            //Log.i("ParseJSONTask", "Getting data from position " + mPosition);
            final int fromLength = mFrom.length;
            final String[] holder = new String[fromLength];
            synchronized (this) {
                if (!mCursor.moveToPosition(mPosition)) {
                    throw new IllegalStateException("couldn't move cursor to position " + mPosition);
                }
                for (int i = 0; i < fromLength; i++) {
                    final int col = mCursor.getColumnIndex(mFrom[i]);
                    holder[i] = mCursor.getString(col);
                }
            }
            list[0] = (holder[0]);
            list[1] = (ParseJSON.rdngToString(holder[1], ParseJSON.RDNG_ON));
            list[2] = (ParseJSON.rdngToString(holder[1], ParseJSON.RDNG_KUN));
            list[3] = (ParseJSON.meaningToString(holder[2]));
            /*
                for (String string : mFrom) {
                    final int col = mCursor.getColumnIndex(string);
                    //Log.i("ParseJSONTask", "txt: " + mCursor.getString(col));
                    if (string.equals(KANJI_TXT)) {
                        list[0] = (mCursor.getString(col));
                    }
                    if (string.equals(KANJI_RDNG)) {
                        final String rdngJSON = mCursor.getString(col);
                        list[1] = (ParseJSON.rdngToString(rdngJSON, ParseJSON.RDNG_ON));
                        list[2] = (ParseJSON.rdngToString(rdngJSON, ParseJSON.RDNG_KUN));
                    }
                    if (string.equals(KANJI_MEANING)) {
                        final String meaningJSON = mCursor.getString(col);
                        list[3] = (ParseJSON.meaningToString(meaningJSON));
                    }
                }*/

            return list;
        }
        protected void onPostExecute(String[] result){
            buffer.put(mPosition, result);
            /*
            for (int i = 0; i < mView.textViews.length; i++){
                //Log.i("ParseJSONTask", "result: " + result.get(i));
                mView.textViews[i].setText(result[i]);
            }*/
            notifyDataSetChanged();
            //processQueue();
        }

    }
    private void processQueue(){
        int size = taskQueue.size();
        if (size > 0){
            isQueueRunning = true;
            final ParseJSONTask task = taskQueue.get(0);
            final AsyncTask.Status status = task.getStatus();
            if (status == AsyncTask.Status.PENDING)
                task.execute();
            if (status == AsyncTask.Status.FINISHED){
                taskQueue.remove(0);
                processQueue();
            }
        }
        else{
            isQueueRunning = false;
        }
    }


}
