package com.brian.kanjielementtests;

import android.app.Activity;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.app.Fragment;
import android.util.Log;
import android.util.SparseArray;
import android.util.SparseIntArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.utils.ValueFormatter;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link HomeFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 */
public class HomeFragment extends Fragment implements View.OnClickListener{

    private OnFragmentInteractionListener mListener;

    public HomeFragment() {
        // Required empty public constructor
    }
    private RelativeLayout baseLayout;
    private ScheduleView scheduleView;
    private TotalsView totalsView;

    private LineChart historyChart;
    private LineData lineData;
    private ArrayList<LineDataSet> lineDataSets;
    private ArrayList<String> xLabels;
    private boolean waitingForHistoryData;

    private UserSettings userSettings;
    private LinearLayout totalsValueLayout;
    private Button startButton;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        userSettings = UserSettings.getInstance();
        baseLayout = (RelativeLayout) inflater.inflate(R.layout.fragment_home, container, false);
        scheduleView = (ScheduleView) baseLayout.findViewById(R.id.scheduleCanvas);
        setScheduleViewData(scheduleView);

        totalsView = (TotalsView) baseLayout.findViewById(R.id.totalsView);
        totalsValueLayout = (LinearLayout) baseLayout.findViewById(R.id.totalsValueContainer);
        setTotalsViewData(totalsView);
        setTotalsValueLayout(totalsView, totalsValueLayout);
        historyChart = (LineChart) baseLayout.findViewById(R.id.historyChart);
        setHistoryChartData(historyChart);

        startButton = (Button) baseLayout.findViewById(R.id.homeStartButton);
        startButton.setOnClickListener(this);
        //xAxis.setValues(xLabels);
        return baseLayout;

    }

    @Override
    public void onClick(View view) {
        final int id = view.getId();
        switch(id){
            case R.id.homeStartButton:
                Log.i("HomeFragment", "Start button clicked");
                mListener.onStudyStart();
                break;
        }
    }

    private void setTotalsViewData(TotalsView totalsView){
        totalsView.resetView(3020);
        for(int i = 0; i < 3020; i ++){
            totalsView.addItem(userSettings.getStage(userSettings.randomInt(6,0)));
        }
        totalsView.invalidate();

    }
    public TotalsView getTotalsView(){
        return totalsView;
    }
    public void setTotalsView(TotalsView totalsView){
        if (this.totalsView != totalsView) this.totalsView = totalsView;
        this.totalsView.invalidate();
        setTotalsValueLayout(this.totalsView, totalsValueLayout);
    }
    private void setTotalsValueLayout(TotalsView totalsView, LinearLayout totalsValueLayout){
        TextView sum = (TextView) totalsValueLayout.findViewById(R.id.sumText);
        TextView remaining = (TextView) totalsValueLayout.findViewById(R.id.remainingText);
        TextView finished = (TextView) totalsValueLayout.findViewById(R.id.finishedText);
        //TODO remove hard coded stages for totals
        TextView value1 = (TextView) totalsValueLayout.findViewById(R.id.valueText1);
        TextView value2 = (TextView) totalsValueLayout.findViewById(R.id.valueText2);
        TextView value3 = (TextView) totalsValueLayout.findViewById(R.id.valueText3);
        TextView value4 = (TextView) totalsValueLayout.findViewById(R.id.valueText4);

        int val1, val2, val3, val4, sumInt, remainingInt, finishedInt;
        //TODO make getSum return an array so it can be summed in one pass
        val1 = totalsView.getSumForStage(userSettings.getStage(1));
        val2 = totalsView.getSumForStage(userSettings.getStage(2));
        val3 = totalsView.getSumForStage(userSettings.getStage(3));
        val4 = totalsView.getSumForStage(userSettings.getStage(4));
        finishedInt = totalsView.getSumForStage(userSettings.getStage(5));
        sumInt = val1 + val2 + val3 + val4 +finishedInt;
        remainingInt=totalsView.getSumForStage(userSettings.getStage(0));
        sum.setText(Integer.toString(sumInt));
        remaining.setText(Integer.toString(remainingInt));
        finished.setText(Integer.toString(finishedInt));
        value1.setText(Integer.toString(val1));
        value2.setText(Integer.toString(val2));
        value3.setText(Integer.toString(val3));
        value4.setText(Integer.toString(val4));
    }
    private void setHistoryChartData(LineChart historyChart){
        xLabels = new ArrayList<String>();
        for (int i = 14; i >=0; i --){
            if(i != 0)
                xLabels.add("-" + Integer.toString(i));
            else
                xLabels.add("TODAY");
        }
        ArrayList<LineDataSet> dataSets = new ArrayList<LineDataSet>();
        List<Integer> integers = new ArrayList<Integer>();
        final int numOfLines = userSettings.getStageCount() - 1; //remove final stage
        for(int i = 0; i < numOfLines; i++){
            final ArrayList<Entry> entries = new ArrayList<Entry>();
            for(int q = 0; q <= 14; q++){
                final int length = integers.size();
                if (q == length) {
                    integers.add(userSettings.randomInt(10, 1));
                }
                else{
                    integers.set(q,integers.get(q) + userSettings.randomInt(10, 1));
                }

                Entry entry = new Entry(integers.get(q),q);
                entries.add(entry);

            }
            final LineDataSet dataSet = new LineDataSet(entries,Integer.toString(i));
            dataSet.setDrawCircles(false);
            final int color = userSettings.getStage(i).backgroundColor;
            dataSet.setColor(color);
            dataSet.setDrawFilled(true);
            dataSet.setFillColor(color);
            dataSet.setFillAlpha(0xFF);
            dataSets.add(0, dataSet);
        }
        LineData lineData = new LineData(xLabels,dataSets);
        historyChart.setData(lineData);
        setHistoryChartStyle(historyChart);

    }
    public void setHistoryData(List<MyActivity.HistoryEntry> historyEntryList){
        List<MyActivity.HistoryEntry> reversed = new ArrayList<MyActivity.HistoryEntry>();
        reversed.addAll(historyEntryList);
        Collections.reverse(reversed);

        lineDataSets = new ArrayList<LineDataSet>();
        List<Integer> integers = new ArrayList<Integer>();
        final int numOfLines = userSettings.getStageCount() - 1; //remove final stage
        for(int i = 0; i < numOfLines; i++){
            final ArrayList<Entry> entries = new ArrayList<Entry>();
            for(int q = 0; q <= 14; q++){
                final int length = integers.size();
                if (q == length) {
                    integers.add(reversed.get(q).data.get(i,0));
                }
                else{
                    integers.set(q,integers.get(q) + reversed.get(q).data.get(i,0));
                }
                Entry entry = new Entry(integers.get(q),q);
                entries.add(entry);

            }
            LineDataSet dataSet = new LineDataSet(entries,Integer.toString(i));
            dataSet.setDrawCircles(false);
            final int color = userSettings.getStage(i).backgroundColor;
            dataSet.setColor(color);
            dataSet.setDrawFilled(true);
            dataSet.setFillColor(color);
            dataSet.setFillAlpha(0xFF);
            lineDataSets.add(0, dataSet);
        }
        lineData = new LineData(xLabels, lineDataSets);
        historyChart.setData(lineData);
    }
    private void setHistoryChartStyle(LineChart historyChart){

        historyChart.setDescription("");
        historyChart.setMaxVisibleValueCount(0);
        historyChart.setTouchEnabled(false);

        YAxis yAxisLeft = historyChart.getAxisLeft();
        yAxisLeft.setEnabled(false);
        //yAxisLeft.setDrawLabels(false);
        //yAxisLeft.setDrawAxisLine(true);
        //yAxisLeft.setAxisLineColor(0xFFFFFFFF);

        YAxis yAxis = historyChart.getAxisRight();
        yAxis.setDrawAxisLine(true);
        yAxis.setAxisLineWidth(2f);
        yAxis.setGridColor(Color.LTGRAY);
        yAxis.setAxisLineColor(Color.BLACK);
        yAxis.setValueFormatter(new ValueFormatter() {
            @Override
            public String getFormattedValue(float value) {
                return Integer.toString((int) value);
            }
        });


        XAxis xAxis = historyChart.getXAxis();
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        //xAxis.setGridColor(Color.LTGRAY);
        xAxis.setAxisLineWidth(2f);
        xAxis.setAxisLineColor(Color.BLACK);
        xAxis.setAvoidFirstLastClipping(true);
        xAxis.setDrawAxisLine(true);
        xAxis.setDrawGridLines(false);


        Legend legend = historyChart.getLegend();
        legend.setEnabled(false);


        //historyChart.setLogEnabled(true);
        /*
        yAxisLeft.setValueFormatter(new ValueFormatter() {
            @Override
            public String getFormattedValue(float value) {
                return "";
            }
        });*/
        //historyChart.offsetTopAndBottom(0);
    }

    private void setScheduleViewData(ScheduleView scheduleView){
        //Test data
        List<ScheduleView.Day> days = new ArrayList<ScheduleView.Day>();
        for(int i = 0; i < 5; i ++) {
            ScheduleView.Day day = new ScheduleView.Day();
            day.stageDataList = new ArrayList<ScheduleView.Day.StageData>();
            day.dayValue = i;
            switch(i){
                case 0:
                    day.title = "TODAY";
                    break;
                case 1:
                    day.title = "+1";
                    break;
                case 2:
                    day.title ="+3";
                    break;
                case 3:
                    day.title = "+7";
                    break;
                case 4:
                    day.title = "+14";
                    break;
            }
            for (int q = 0; q < 4; q ++) {
                ScheduleView.Day.StageData stageData = new ScheduleView.Day.StageData();
                stageData.id = q;
                stageData.count = 10;
                day.stageDataList.add(stageData);
            }

            days.add(day);
        }
        scheduleView.setDays(days);
    }
    public void setScheduleData(List<Map.Entry<Long,Integer>> scheduleList){
        int pos = 0;
        List<ScheduleView.Day> days = new ArrayList<ScheduleView.Day>();
        Calendar date = Calendar.getInstance();
        date.setTimeInMillis(System.currentTimeMillis());
        date.set(Calendar.HOUR_OF_DAY, 0);
        date.set(Calendar.MINUTE, 0);
        date.set(Calendar.SECOND, 0);
        date.set(Calendar.MILLISECOND, 0);
        final int stageCount = UserSettings.getInstance().getStageCount();
        final int listLength = scheduleList.size();
        long timeStamp;
        for(int i = 0; i < 5; i ++) {
            ScheduleView.Day day = new ScheduleView.Day();
            day.stageDataList = new ArrayList<ScheduleView.Day.StageData>();
            day.dayValue = i;
            switch(i){
                case 0:
                    day.title = "TODAY";
                    date.add(Calendar.DAY_OF_YEAR, 1);
                    break;
                case 1:
                    day.title = "+1";
                    date.add(Calendar.DAY_OF_YEAR, 1);
                    break;
                case 2:
                    day.title ="+3";
                    date.add(Calendar.DAY_OF_YEAR, 2);
                    break;
                case 3:
                    day.title = "+7";
                    date.add(Calendar.DAY_OF_YEAR, 4);
                    break;
                case 4:
                    day.title = "+14";
                    date.add(Calendar.DAY_OF_YEAR,7);
                    break;
            }
            timeStamp = date.getTimeInMillis();
            //Log.i("HomeFragment", "timeStamp: " + timeStamp);
            List<ScheduleView.Day.StageData> stageDataList = new ArrayList<ScheduleView.Day.StageData>();
            for (int q = 0; q < stageCount; q++)
                stageDataList.add(new ScheduleView.Day.StageData());
            while(pos < listLength && scheduleList.get(pos).getKey() < timeStamp ){
                final int stage = scheduleList.get(pos).getValue();
                ScheduleView.Day.StageData stageData = stageDataList.get(stage);
                stageData.id = stage;
                stageData.count += 1;
                pos ++;
            }

            //List<ScheduleView.Day.StageData> trimmedList = new ArrayList<ScheduleView.Day.StageData>();
            for (int q = 0; q < stageCount; q ++){
                ScheduleView.Day.StageData stageData = stageDataList.get(q);
                if (stageData.count > 0) day.stageDataList.add(stageData);
            }
            /*
            for (int q = 0; q < 4; q ++) {

                ScheduleView.Day.StageData stageData = new ScheduleView.Day.StageData();
                stageData.id = q;
                stageData.count = 10;
                day.stageDataList.add(stageData);
            }*/
            //.addAll(trimmedList);
            days.add(day);
        }
        scheduleView.setDays(days);
    }


    @Override
    public void onResume() {
        super.onResume();
        mListener.requestHistoryData();
        mListener.requestScheduleData();
        mListener.requestTotalsData();

    }

    @Override
    public void onViewCreated(View view, Bundle savedInstance){
        super.onViewCreated(view, savedInstance);
       // scheduleView.redrawBitmap();
       // totalsView.setLayoutParams(params);
       // totalsView.invalidate();

    }

    @Override
    public void onStart(){
        super.onStart();

    }



    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        public void onStudyStart();
        public void requestHistoryData();
        public void requestScheduleData();
        public void requestTotalsData();
    }

}
