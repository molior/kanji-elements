package com.brian.kanjielementtests;

import android.util.Log;

/**
 * Created by Brian on 3/13/2015.
 */
public class CharDetector {

    private final static int IDEOGRAPHIC_SPACE = 0x3000;

    public static boolean isAsian(char c){
        return c > IDEOGRAPHIC_SPACE;
    }
    public static boolean isFullWidth(char c){
        return isAsian(c) || c == IDEOGRAPHIC_SPACE;
    }
    public static boolean isKanji(char c){
        Log.i("CharDetector", "character: " + ((int) c) + " " + 0x4E00 );
        return (0x4E00 <= c ) && (c <= 0x9FFF);
    }
    public static boolean isHiragana(char c){
        return (0x3041 <= c) && (c <= 0x309F);
    }
    public static boolean isFullWidthKatana(char c){
        return (0x30A0 <= c) && (c <= 0x30FF);
    }
    public static boolean isHalfWidthKatana(char c){
        return (0xFF65 <= c) && (c <= 0xFF9F);
    }
    public static boolean isFullDigit(char c){
        return (0xFF10 <= c) && (c <= 0xFF19);
    }
    public static boolean isFullAlpha(char c){
        return (((0xFF21 <= c) && (c <= 0xFF3A) )||((0xFF41 <= c) && (c <= 0xFF5A)) );
    }
}
