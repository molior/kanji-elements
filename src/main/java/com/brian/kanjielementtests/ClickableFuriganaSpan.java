package com.brian.kanjielementtests;

import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.ClickableSpan;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

/**
 * Created by Brian on 1/29/2015.
 */
public class ClickableFuriganaSpan extends ClickableSpan{
    private FuriganaSpan mSpan;
    public ClickableFuriganaSpan(FuriganaSpan span){
        super();
        mSpan = span;
    }
    @Override
    public void onClick(View view){
        TextView textView = (TextView) view;
        SpannableString stringSpan = (SpannableString) textView.getText();
        mSpan.flipVisibility();
        textView.setText(stringSpan);
        final int start = stringSpan.getSpanStart(this);
        final int end = stringSpan.getSpanEnd(this);
        String string = stringSpan.toString();
    }
}
