package com.brian.kanjielementtests;

import android.app.AlertDialog;
import android.app.FragmentTransaction;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Parcelable;
import android.preference.PreferenceManager;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.util.Log;
import android.util.SparseIntArray;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Map;


public class MyActivity extends FragmentActivity implements
        LoaderManager.LoaderCallbacks<Cursor>,
        DictionaryListFragment.OnFragmentInteractionListener,
        LookupBar.OnFragmentInteractionListener,
        CardFragment.OnFragmentInteractionListener,
        HomeFragment.OnFragmentInteractionListener,
        View.OnClickListener,
        PrefsFragment.OnPrefsInteraction {

    private static enum KanjiCursorState{
        DICTIONARY, TEST
    }


    public static class HistoryEntry{
        long date;
        SparseIntArray data;

        public HistoryEntry(long date, String json){
            this.date = date;
            data = new SparseIntArray();
            setDataFromJson(json);
        }
        public HistoryEntry(long date){
            this.date = date;
            data = new SparseIntArray();
        }

        public void setDataFromJson(String json){
            try{
                data = new SparseIntArray();
                JSONObject jsonObject = new JSONObject(json);
                UserSettings userSettings = UserSettings.getInstance();
                final int stageCount = userSettings.getStageCount();
                for (int i = 0; i < stageCount; i ++){
                    if (jsonObject.has(Integer.toString(i))) data.put(i,jsonObject.getInt(Integer.toString(i)));
                    else data.put(i,0);
                }

            } catch (JSONException e){
                e.printStackTrace();
            }
        }
        public String makeJsonFromData(){
            String output = "{}";
            try{
                JSONObject json = new JSONObject();
                UserSettings userSettings = UserSettings.getInstance();
                final int stageCount = userSettings.getStageCount();
                for(int i = 0; i < stageCount; i ++){
                    int value = data.get(i, 0);
                    json.put(Integer.toString(i), value);
                }
                output = json.toString();

            } catch (JSONException e){
                e.printStackTrace();
            }
            return output;
        }
    }
    private List<HistoryEntry> historyList;

    private List<Map.Entry<Long, Integer>> scheduleList;
    private Cursor kanjiCursor;
    private KanjiCursorState kanjiCursorState;
    private int kanjiCursorPosition;
    private int studyListPosition;
    private int kanjiCursorCount;

    private Cursor testCursor;



    private KanjiDatabase kanjiDB;
    private UserDatabase userDB;

    private DictionaryListFragment dictionaryList;
    private long mKanjiCount;
    private CardFragment cardFragment;
    private HomeFragment homeFragment;
    private LookupBar lookupBar;
    private PrefsFragment prefsFragment;

    private List<Integer> dueKanjiArray;
    private List<Integer> newKanjiArray;
    private List<Integer> repeatKanjiArray;
    private List<Integer> studyArray;
    private List<Integer> undoArray;
    private List<Bundle> undoDBArray;
    private int undoLockPosition;
    private int progressTotal;
    private int progressValue;

    private Parcelable dictionaryListState;

    private final int CURSOR_LOADER = 0;
    private final int CURSOR_LOADER_BUNDLE = 1;
    private final int ARRAY_LOADER = 2;
    private static final String TAG_KANJI_LIST_CLICK = "MyActivity.onKanjiListClick";

    public static class KanjiData{
        public boolean newKanji;
        public Bundle bundle;
        public ArrayList<KanjiHelper.ExampleWordData> wordData;
        public ArrayList<KanjiHelper.ExampleNameData> nameData;
        public CharSequence rdngOn;
        public CharSequence rdngKun;
        public CharSequence rdngNanori;
        public CharSequence meaning;

        public LinearLayout kanjiDetails;
        public List<KanjiPath> kanjiPaths;
        public LinearLayout kanjiExtraDetails;
        public RelativeLayout kanjiCompareLayout;
        public KanjiCanvas kanjiCanvas;
        public KanjiCanvas userKanjiCanvas;
        public TextView userStrokeText;
        public TextView kanjiStrokeText;

        public AsyncTask task;
        public void set(KanjiData kanjiData){
            bundle = kanjiData.bundle;
            wordData = kanjiData.wordData;
            nameData = kanjiData.nameData;
            rdngOn = kanjiData.rdngOn;
            rdngKun = kanjiData.rdngKun;
            rdngNanori = kanjiData.rdngNanori;
            meaning = kanjiData.meaning;

            kanjiDetails = kanjiData.kanjiDetails;
            kanjiPaths = kanjiData.kanjiPaths;
            kanjiExtraDetails = kanjiData.kanjiExtraDetails;
            kanjiCompareLayout = kanjiData.kanjiCompareLayout;
            kanjiCanvas = kanjiData.kanjiCanvas;
            userKanjiCanvas = kanjiData.userKanjiCanvas;
            userStrokeText = kanjiData.userStrokeText;
            kanjiStrokeText = kanjiData.kanjiStrokeText;
        }
        public boolean isEmpty(){
            return bundle == null;
        }
        public void cancel(){
            if (task != null) task.cancel(true);
        }
    }
    private CardFragment.Mode cardFragmentMode;
    private Boolean waitingForKanjiData;
    private KanjiData currentKanjiData;
    private KanjiData nextKanjiData;
    private KanjiData previousKanjiData;


    private static String SAVE_DICTIONARY_LIST = "dictionary_list";
    private static String SAVE_KANJI_CURSOR_POSITION = "kanji_cursor_position";
    private static String SAVE_STUDY_LIST_POSITION = "study_list_position";
    private static String SAVE_DATE = "date";
    private static String SAVE_DUE_KANJI_LIST = "due_kanji_list";
    private static String SAVE_NEW_KANJI_LIST = "new_kanji_list";
    private static String SAVE_REPEAT_KANJI_LIST = "repeat_kanji_list";
    private static String SAVE_STUDY_LIST = "study_list";
    private static String SAVE_UNDO_LOCK_POS = "undo_lock_pos";
    private static String SAVE_PROGRESS_TOTAL = "progress_total";
    private static String SAVE_PROGRESS_VALUE = "progress_value";

    private PopupWindow debugCardSettings;
    private View myActivityLayout;
    private Bundle cardSettings;
    private Bundle newCardSettings;

    private String previousSearchQuery;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //PreferenceManager.setDefaultValues(this, R.xml.preferences, false);
        //PreferenceManager.setDefaultValues(this, R.xml.preferences,false);

        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);

        UserSettings userSettings = UserSettings.getInstance();
        Map<String, ?> map = sharedPreferences.getAll();
        /*for (Map.Entry<String, ?> entry: map.entrySet()){
            Log.i("MyActivity", entry.getKey() + " - "  + entry.getValue());
        }*/
        userSettings.setUserSkipInitialStage(sharedPreferences.getBoolean("study_stage_skip",getResources().getBoolean(R.bool.default_skip_initial_stage)));


        userSettings.setUserSessionLimit(Integer.parseInt(sharedPreferences.getString("cards_per_session", Integer.toString(getResources().getInteger(R.integer.default_session_limit)))));
        userSettings.setUserNewPerSessionLimit(Integer.parseInt(sharedPreferences.getString("new_cards_per_session", Integer.toString(getResources().getInteger(R.integer.default_new_per_session_limit)))));

        requestWindowFeature(Window.FEATURE_PROGRESS);
        setContentView(R.layout.activity_my);
        myActivityLayout = findViewById(R.id.activityMy);
        dictionaryList = new DictionaryListFragment();
        homeFragment = new HomeFragment();
        lookupBar = new LookupBar();
        prefsFragment = new PrefsFragment();
        cardSettings = new Bundle();
        newCardSettings = new Bundle();
        undoArray = new ArrayList<Integer>();
        undoDBArray = new ArrayList<Bundle>();

        //DEBUG CODE
        newCardSettings.putInt(UserSettings.KEY_STAGE, 0);
        //intervalId
        newCardSettings.putInt(UserSettings.KEY_INTERVAL,0);
        newCardSettings.putString(UserSettings.KEY_HISTORY,"[]");




        cardFragmentMode = CardFragment.Mode.DICTIONARY;
        previousSearchQuery = "";
        initDebugCardSettingsPopup();

        kanjiDB = new KanjiDatabase(this);
        userDB = new UserDatabase(this, kanjiDB.getReadableDatabase());
        kanjiCursor = null;
        kanjiCursorPosition = -1;
        kanjiCursorCount = 0;
        nextKanjiData = new KanjiData();
        currentKanjiData = new KanjiData();
        previousKanjiData = new KanjiData();
        testCursor = null;
        dictionaryListState = null;
        cardFragment = new CardFragment();
        undoLockPosition = -1;
        progressTotal = 0;
        progressValue = 0;

        //getSupportLoaderManager().initLoader(CURSOR_LOADER, null, this);

        long date = 0;


        //Log.i("MyActivity", "Time Long: " +  System.currentTimeMillis());
        Calendar previousDate = Calendar.getInstance();
        previousDate.setTimeInMillis(date);
        Calendar todayDate = Calendar.getInstance();
        todayDate.setTimeInMillis(System.currentTimeMillis());
        todayDate.set(Calendar.HOUR_OF_DAY, 0);
        todayDate.set(Calendar.MILLISECOND, 0);
        todayDate.set(Calendar.MINUTE, 0);
        todayDate.set(Calendar.SECOND, 0);

        if (previousDate.get(Calendar.YEAR) != todayDate.get(Calendar.YEAR) || previousDate.get(Calendar.DAY_OF_YEAR) != todayDate.get(Calendar.DAY_OF_YEAR)){

            /*
            *
            *  If date is not today reset study arrays and repeat arrays
            *  Make new user_history entry
            *
            */
            repeatKanjiArray = new ArrayList<Integer>();
            studyArray = new ArrayList<Integer>();
            progressTotal = 0;
            progressValue = 0;
            new LoadSessionKanji(studyArray).execute();
        }

        /*
         *    Get data for home fragment.
         *      User history array
         *      Schedule List
         *      Totals
         */
        historyList = DatabaseQuery.getSessionHistoryList(userDB.getReadableDatabase(), todayDate.getTimeInMillis());
        Log.i("MyActivity", "todayDate: " + todayDate.getTimeInMillis() + " compare: " + historyList.get(0).date);
        Log.i("MyActivity", "historyList Size: " + historyList.size());
        userDB.insertHistoryEntry(historyList.get(0).date);
        /*
        if (historyList.get(0).date != todayDate.getTimeInMillis()){
            userDB.insertHistoryEntry(todayDate.getTimeInMillis());
            historyList.add(0, new HistoryEntry(todayDate.getTimeInMillis()));
            //historyList.remove(historyList.size()-1);
        }*/
        todayDate.add(Calendar.DAY_OF_YEAR, 15);
        scheduleList = DatabaseQuery.getScheduleList(userDB.getReadableDatabase(), todayDate.getTimeInMillis());

        android.app.FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.add(R.id.linearLayoutMy,lookupBar);
        transaction.add(R.id.linearLayoutMy, homeFragment);
        //transaction.add(R.id.activityMy, dictionaryList);
        transaction.commit();


        //new GetDatabaseSizeTask().execute();

        //loadKanjiData(0, 50);
        //Test Data
        /*
        List<DictionaryListFragment.KanjiData> list1 = new ArrayList<DictionaryListFragment.KanjiData>();
        for(int i = 0; i < 50; i ++){
            long id = i+1;
            String string = Integer.toString(i+1);
            list1.add(new DictionaryListFragment.KanjiData(id,string,string,string,string));
        }*/




        /*
        listViewPosition = 0;


        //kanjiCursor = kanjiDB.getKanjis();
        String[] from = {"txt","rdng", "meaning"};
        int[] to = {R.id.kanjiText, R.id.rdngOnText, R.id.rdngKunText, R.id.meaningText};
        mAdapter = new KanjiCursorAdapter(this,R.layout.item_kanjilist, null, from, to);
        //mArrayAdapter = new KanjiArrayAdapter(this);
        //final ListAdapter adapter = new KanjiCursorAdapter(this, R.layout.item_kanjilist, kanjiCursor, from, to);

        listView = (ListView) findViewById(R.id.list);
        listView.setAdapter(mAdapter);
        listViewState = listView.onSaveInstanceState();
        */
        //getSupportLoaderManager().initLoader(ARRAY_LOADER, null, this);
        //getSupportLoaderManager().initLoader(CURSOR_LOADER,null,this);
        /*listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {

                Intent intent = new Intent(MyActivity.this, CardActivity.class);
                Bundle bundle = new Bundle();
                Cursor cursor = mAdapter.getCursor();
                if (cursor != null) {
                    final int kanjiCol = cursor.getColumnIndex("txt");
                    final int rdngCol = cursor.getColumnIndex("rdng");
                    final int meaningCol = cursor.getColumnIndex("meaning");
                    final int exampleCol = cursor.getColumnIndex("example");
                    final int svgCol = cursor.getColumnIndex("svg");
                    final int strokeCol = cursor.getColumnIndex("stroke_count");
                    final int heisigCol = cursor.getColumnIndex("heisig");
                    synchronized (this) {
                        cursor.moveToPosition(position);
                        bundle.putString(UserSettings.KEY_KANJI, cursor.getString(kanjiCol));
                        bundle.putInt(UserSettings.KEY_STROKE, cursor.getInt(strokeCol));
                        bundle.putInt(UserSettings.KEY_HEISIG, cursor.getInt(heisigCol));
                        bundle.putString(UserSettings.KEY_RDNG, cursor.getString(rdngCol));
                        bundle.putString(UserSettings.KEY_MEANING,cursor.getString(meaningCol));
                        bundle.putString(UserSettings.KEY_EXAMPLE,cursor.getString(exampleCol));
                        bundle.putString(UserSettings.KEY_SVG,cursor.getString(svgCol));
                    }
                }

                Log.i(TAG_KANJI_LIST_CLICK, "kanji id: "+ id);
                bundle.putInt("mode",CardActivity.Mode.DICTIONARY.ordinal());
                intent.putExtras(bundle);
                startActivity(intent);
                listViewPosition = listView.getFirstVisiblePosition();
                listViewState = listView.onSaveInstanceState();

            }
        });*/


    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        long date;
        if (savedInstanceState != null) {
            dictionaryListState = savedInstanceState.getParcelable(SAVE_DICTIONARY_LIST);
            kanjiCursorPosition = savedInstanceState.getInt(SAVE_KANJI_CURSOR_POSITION);
            date = savedInstanceState.getLong(SAVE_DATE);
            //dueKanjiArray = savedInstanceState.getIntegerArrayList(SAVE_DUE_KANJI_LIST);
            //newKanjiArray = savedInstanceState.getIntegerArrayList(SAVE_NEW_KANJI_LIST);
            repeatKanjiArray = savedInstanceState.getIntegerArrayList(SAVE_REPEAT_KANJI_LIST);
            studyArray = savedInstanceState.getIntegerArrayList(SAVE_STUDY_LIST);
            undoLockPosition = savedInstanceState.getInt(SAVE_UNDO_LOCK_POS);
            progressTotal = savedInstanceState.getInt(SAVE_PROGRESS_TOTAL);
            progressValue = savedInstanceState.getInt(SAVE_PROGRESS_VALUE);
            studyListPosition = savedInstanceState.getInt(SAVE_STUDY_LIST_POSITION);
        }
        super.onRestoreInstanceState(savedInstanceState);
    }

    public void initDebugCardSettingsPopup(){
        LinearLayout popupView = (LinearLayout) getLayoutInflater().inflate(R.layout.debug_popup_card_settings, new LinearLayout(this) , false);
        debugCardSettings = new PopupWindow(popupView, 400, 600, true);
        //debugCardSettings.setFocusable(true);
        DebugHelperClass debugHelper = new DebugHelperClass(this);
        (popupView.findViewById(R.id.debugPopup_buttonOk)).setOnClickListener(debugHelper);
        (popupView.findViewById(R.id.debugPopup_buttonCancel)).setOnClickListener(debugHelper);
        List<Integer> list = new ArrayList<Integer>();
        final int length = UserSettings.getInstance().getStageCount();
        for (int i = 0; i < length; i ++){
            list.add(i);
        }
        List<Integer> list2 = new ArrayList<Integer>();
        UserSettings.Stage stage = UserSettings.getInstance().getStage(0);
        final int itvLength = stage.intervals.length;
        for (int i = 0; i < itvLength; i ++){
            list2.add(stage.intervals[i]);
        }
        List<String> historyList = new ArrayList<String>();
        historyList.add("[]");
        historyList.add("[0]");
        historyList.add("[1]");
        historyList.add("[0,1]");
        historyList.add("[1,1]");
        historyList.add("[0,0]");

        ArrayAdapter<Integer> arrayAdapter = new ArrayAdapter<Integer>(this, android.R.layout.simple_spinner_item,list );
        ArrayAdapter<Integer> arrayAdapter1 = new ArrayAdapter<Integer>(this, android.R.layout.simple_spinner_item,list2);
        ArrayAdapter<String> arrayAdapter2 = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, historyList);
        ((Spinner)popupView.findViewById(R.id.spinnerStage)).setAdapter(arrayAdapter);
        ((Spinner)popupView.findViewById(R.id.spinnerStage)).setOnItemSelectedListener(debugHelper);
        ((Spinner)popupView.findViewById(R.id.spinnerInterval)).setAdapter(arrayAdapter1);
        ((Spinner)popupView.findViewById(R.id.spinnerInterval)).setOnItemSelectedListener(debugHelper);
        ((Spinner)popupView.findViewById(R.id.spinnerHistory)).setAdapter(arrayAdapter2);
        ((Spinner)popupView.findViewById(R.id.spinnerHistory)).setOnItemSelectedListener(debugHelper);
        debugHelper.setIntervalAdapter(arrayAdapter1);
    }
    @Override
    public void onClick(View view) {

    }

    @Override
    public void onDestroy(){
        super.onDestroy();
        if (userDB != null){
            userDB.close();

        }
        if (kanjiDB != null){
            kanjiDB.close();
        }

        //kanjiDB.close();

    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putParcelable(SAVE_DICTIONARY_LIST,dictionaryListState);
        outState.putInt(SAVE_KANJI_CURSOR_POSITION, kanjiCursorPosition);
        outState.putIntegerArrayList(SAVE_STUDY_LIST, (ArrayList<Integer>) studyArray);
        outState.putIntegerArrayList(SAVE_REPEAT_KANJI_LIST, (ArrayList<Integer>) repeatKanjiArray);
        outState.putInt(SAVE_UNDO_LOCK_POS, undoLockPosition);
        outState.putLong(SAVE_DATE, System.currentTimeMillis());
        outState.putInt(SAVE_PROGRESS_VALUE, progressValue);
        outState.putInt(SAVE_PROGRESS_TOTAL,progressTotal);
        outState.putInt(SAVE_STUDY_LIST_POSITION,studyListPosition);
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onPause(){
        super.onPause();
        if (kanjiDB != null)
            kanjiDB.close();
        getSupportLoaderManager().destroyLoader(CURSOR_LOADER);
        if (userDB != null)
            userDB.close();
        clearKanjiData();
        //save cursor state

    }
    private void clearKanjiData(){
        currentKanjiData.cancel();
        currentKanjiData = new KanjiData();
        nextKanjiData.cancel();
        previousKanjiData.cancel();
        nextKanjiData = new KanjiData();
        previousKanjiData = new KanjiData();

    }
    @Override
    public void onStop(){
        super.onStop();
    }

    @Override
    public void onResume(){
        super.onResume();
        //Log.i("MyActivity", "default stage json: " + UserSettings.getInstance().stagesToJSON());
        kanjiDB = new KanjiDatabase(this);
        userDB = new UserDatabase(this, kanjiDB.getReadableDatabase());
        //Reload state
        //reload cursors
        Bundle args = null;
        if(previousSearchQuery != null) {
            args = new Bundle();
            args.putString(KanjiDatabase.SEARCH_QUERY, previousSearchQuery);

        }
        getSupportLoaderManager().initLoader(CURSOR_LOADER, args, this);
        //Reload state
    }

    @Override
    public void onBackPressed() {
        if (getFragmentManager().getBackStackEntryCount() == 0) {
            super.onBackPressed();
        } else {
            /*getFragmentManager().beginTransaction()
                    .remove(cardFragment)
                    .commit();*/
            getFragmentManager().popBackStack();
        }
    }

    public void closePopup(boolean comfirmSettings){
        if (comfirmSettings)
            cardSettings = newCardSettings;
        Toast.makeText(getBaseContext(),cardSettings.toString(), Toast.LENGTH_LONG).show();
        debugCardSettings.dismiss();
    }
    public void cardSettingsStage(int stageId) {
        newCardSettings.putInt(UserSettings.KEY_STAGE, stageId);
    }
    public void cardSettingsInterval(int intervalId){
        newCardSettings.putInt(UserSettings.KEY_INTERVAL, intervalId);
    }
    public void cardSettingsHistory(String history){
        newCardSettings.putString(UserSettings.KEY_HISTORY,history);
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.my, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        switch (id){
            case R.id.next_card_settings:
                debugCardSettings.showAtLocation(myActivityLayout, Gravity.CENTER_VERTICAL, 0,0);
                return true;
            case R.id.action_settings:
                //get active fragments
                //lookup bar
                //card fragment
                //home fragment
                //dictionaryList
                if (prefsFragment.isVisible()) return true;
                FragmentTransaction transaction = getFragmentManager().beginTransaction();
                if(lookupBar.isVisible()) transaction.remove(lookupBar);
                if(cardFragment.isVisible()) transaction.remove(cardFragment);
                if(homeFragment.isVisible()) transaction.remove(homeFragment);
                if(dictionaryList.isVisible()) transaction.remove(dictionaryList);
                transaction.add(R.id.activityMy, prefsFragment);
                transaction.addToBackStack(null);
                transaction.commit();
                return true;
            case R.id.study_array:
                final int size = studyArray.size();
                CharSequence[] array = new String[size];
                for (int i = 0; i < size; i ++){
                    array[i] = studyArray.get(i).toString();
                }
                AlertDialog alertDialog = new AlertDialog.Builder(this)
                        .setItems(array, null)
                        .setTitle("Study Array\nPosition: " + Integer.toString(studyListPosition) +
                                "\nSize: " + Integer.toString(size))
                        .setPositiveButton("OK", null)
                        .show();
                return true;
            case R.id.repeat_array:
                final int repeatSize = repeatKanjiArray.size();
                CharSequence[] repeatText = new String[repeatSize];
                for (int i = 0; i < repeatSize; i ++){
                    repeatText[i] = repeatKanjiArray.get(i).toString();
                }
                AlertDialog alertDialog1 = new AlertDialog.Builder(this)
                        .setItems(repeatText, null)
                        .setTitle("Repeat Array\n" + "Position: " + Integer.toString(studyListPosition) +
                                "\nSize: " + Integer.toString(repeatSize))
                        .setPositiveButton("OK", null)
                        .show();
                return true;
            case R.id.kanji_data:
                String currentKanji = currentKanjiData.isEmpty() ? "empty": currentKanjiData.bundle.getInt(UserSettings.KEY_ID) + " " + currentKanjiData.bundle.getString(UserSettings.KEY_KANJI);
                String nextKanji = nextKanjiData.isEmpty() ? "empty": nextKanjiData.bundle.getInt(UserSettings.KEY_ID) + " " + nextKanjiData.bundle.getString(UserSettings.KEY_KANJI);
                String prevKanji = previousKanjiData.isEmpty() ? "empty": previousKanjiData.bundle.getInt(UserSettings.KEY_ID) + " " + previousKanjiData.bundle.getString(UserSettings.KEY_KANJI);
                String cardWaiting = cardFragment.isWaitingForData() ? "Yes" : "No";
                AlertDialog alertDialog2 = new AlertDialog.Builder(this)
                        .setMessage("Previous Data: " + prevKanji +
                                "\nCurrent Data: " + currentKanji +
                                "\nNext Data: " + nextKanji +
                                "\nCard Fragment Waiting: " + cardWaiting)
                        .setTitle("Kanji Data")
                        .setPositiveButton("OK", null)
                        .show();
                return true;
            }


        return super.onOptionsItemSelected(item);
    }
    public void setKanjiCursorPosition(int pos){
        kanjiCursorPosition = pos;
    }
    public Integer[] getKanjiCursorPosition() {
        return getKanjiCursorPosition(0);
    }
    public Integer[] getKanjiCursorPosition(int posDelta){
        switch (cardFragmentMode){
            case DICTIONARY:
                return new Integer[]{kanjiCursorPosition+posDelta};
            case TEST_BACK:
            case TEST_FRONT:
                if (studyListPosition+posDelta >= studyArray.size() && repeatKanjiArray.size() > 0) {
                        studyArray.addAll(repeatKanjiArray);
                        undoArray.add(-1);
                        repeatKanjiArray.clear();
                    }
                if (studyListPosition+posDelta < studyArray.size() && studyListPosition+posDelta >= 0) {
                    //Log.i("MyActivity.getKanjiCursorPosition", " studyArray id:" + studyArray.get(studyListPosition + posDelta));
                    return new Integer[]{-1, studyArray.get(studyListPosition + posDelta)};
                }

        }
        return new Integer[]{-2};
    }
    public void moveToPreviousCursorPosition(){
        moveToPreviousCursorPosition(CardFragment.TestModeState.OFF);
    }
    public void moveToPreviousCursorPosition(CardFragment.TestModeState state){
        switch (state){
            case OFF:
                kanjiCursorPosition --;
                break;
            default:
                final int undoArraySize = undoArray.size();
                if (undoArraySize > 0){
                    int undo = undoArray.remove(undoArraySize - 1);
                    if (undo == -1) {
                        while(studyArray.size() > studyListPosition)
                            repeatKanjiArray.add(studyArray.remove(studyListPosition));
                        undo = undoArray.remove(undoArraySize -2);
                    }
                    if (undo >= 0) {
                        //Log.i("MyActivity","repeatArray: " + repeatKanjiArray.toString());
                        repeatKanjiArray.remove(undo);
                    }
                    updateKanji(undoDBArray.remove(undoDBArray.size() -1), null);
                    studyListPosition --;
                }
                break;
        }



    }
    public void undoMoveToNextCursorPosition(Integer id){
        int index = repeatKanjiArray.indexOf(id);
        if (index < 0){
            index = studyArray.lastIndexOf(id);
            if (index > kanjiCursorPosition)
                studyArray.remove(index);
        }
        else
            repeatKanjiArray.remove(index);
    }
    public int moveToNextKanjiCursorPosition(){
        return moveToNextKanjiCursorPosition(false);
    }
    public int moveToNextKanjiCursorPosition(boolean repeatCurrent){
        final int randomPosition = UserSettings.getInstance().randomInt(repeatKanjiArray.size(), 0);
        if (repeatCurrent){
            Integer id = studyArray.get(studyListPosition);
            repeatKanjiArray.add(randomPosition,id);
        }
        switch(cardFragmentMode){
            case DICTIONARY:
                kanjiCursorPosition ++;
                break;
            default:
                studyListPosition ++;
                break;
        }
        return randomPosition;
    }
    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        if (id == CURSOR_LOADER)
            return new KanjiDatabase.KanjiCursorLoader(this, this.kanjiDB, args);
        if (id == CURSOR_LOADER_BUNDLE)
            return new KanjiDatabase.KanjiCursorLoader(this, this.kanjiDB, args);
        else
            return new KanjiDatabase.KanjiArrayLoader(this,this.kanjiDB, args);

    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        final int id = loader.getId();
        if (id == CURSOR_LOADER){
            kanjiCursor = data;
            kanjiCursorCount = kanjiCursor.getCount();
            if(dictionaryList.isWaitingForData()){
                loadKanjiCursorToDictionary();
                //dictionaryList.setData(kanjiCursor);
            }
            if(cardFragment.isWaitingForData()){
                Log.i("MyActivity", "Load card from CursorLoader");
                loadCardFragment();
            }
           // listView.onRestoreInstanceState(listViewState);
            //listView.setSelection(listViewPosition);
        }/*
        if (loader.getClass() == KanjiDatabase.KanjiArrayLoader.class){
            dictionaryList.setList(((KanjiDatabase.KanjiArrayLoader) loader).list);
            data.close();
           // mArrayAdapter.addAll(((KanjiDatabase.KanjiArrayLoader) loader).list);
          //  kanjiDB.close();
          //  data.close();
        }*/
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        if (loader.getId() == CURSOR_LOADER) {
            dictionaryList.removeData();
            kanjiCursor = null;
        }
    }

    private void loadCardFragment(){
        //final long start = System.nanoTime();
        //Log.i("MyActivity", "kanjiCursorPosition: " + getKanjiCursorPosition());
       // Log.i("MyActivity", "kanjiCursorState: " + (kanjiCursor != null));
        //Log.i("MyActivity", "currentKanjiData: " + (currentKanjiData.isEmpty()));
        //Log.i("MyActivity", "loadCardFragment start: " + System.nanoTime());
        if (currentKanjiData.isEmpty()) {
            //nextKanjiData.clear();

            nextKanjiData.cancel();
            previousKanjiData.cancel();
            //previousKanjiData.clear();
            Integer[] pos = getKanjiCursorPosition();
            int pos2 = pos.length == 2? pos[1]: -1;
            Log.i("MyActivity", "kanjiCursorPosition: " + pos[0] + " " + pos2);
            if(pos[0] != -2 && (currentKanjiData.task == null || currentKanjiData.task.getStatus() != AsyncTask.Status.RUNNING)) {
                Log.i("MyActivity", "Create new currentKanji task");
                currentKanjiData.task = new GetKanjiData(currentKanjiData).execute(pos);
            }
        }
        else{
            nextKanjiData.cancel();
            previousKanjiData.cancel();

            cardFragment.reloadFragment(currentKanjiData);

            if(nextKanjiData.isEmpty()) nextKanjiData.task = new GetKanjiData(nextKanjiData).execute(getKanjiCursorPosition(1));

            if (cardFragmentMode == CardFragment.Mode.DICTIONARY)
                if(previousKanjiData.isEmpty()) previousKanjiData.task = new GetKanjiData(previousKanjiData).execute(getKanjiCursorPosition(-1));
            //cardFragment = CardActivity.newInstance(currentKanjiData.bundle);
            //getFragmentManager().beginTransaction()
            //                    .add(R.id.activityMy, cardFragment)
            //                    .commit();
        }

        //Log.i("MyActivity", "loadCardFragment time: " + (System.nanoTime() - start));
    }

    public void setPreviousState(Parcelable state){
        dictionaryListState = state;
    }

    public Parcelable getDictionaryListPreviousState(){
        return dictionaryListState;
    }
    public void loadKanjiCursorToDictionary(){
        if(kanjiCursor != null && !kanjiCursor.isClosed()) dictionaryList.setData(kanjiCursor);
        else {
            getSupportLoaderManager().initLoader(CURSOR_LOADER, null, this);
            //setDictionaryListNow = true;
        }
    }


    public void onItemSelected(int id, int position){
        setKanjiCursorPosition(position);
        currentKanjiData.cancel();
        currentKanjiData = new KanjiData();
        nextKanjiData.cancel();
        nextKanjiData = new KanjiData();
        previousKanjiData.cancel();
        previousKanjiData = new KanjiData();

        android.app.FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.remove(lookupBar);
        transaction.remove(dictionaryList);
        transaction.add(R.id.activityMy,cardFragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }
    public LinearLayout getKanjiDetails(){
        return currentKanjiData.kanjiDetails;
    }
    public List<KanjiPath> getKanjiPaths(){
        return currentKanjiData.kanjiPaths;
    }
    public void onStudyComplete(){

        AlertDialog alert = new AlertDialog.Builder(this)
                                .setTitle("Study Session Complete.")
                                .setPositiveButton(getString(R.string.dialog_yes), new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        getFragmentManager().popBackStack();

                                        resetHome();
                                    }
                                }).show();
    }
    public void requestTotalsData(){
        TotalsView totalsView = homeFragment.getTotalsView();
        DatabaseQuery.fillTotalsView(userDB.getReadableDatabase(), totalsView);
        homeFragment.setTotalsView(totalsView);
    }
    public void requestHistoryData(){
        homeFragment.setHistoryData(historyList);
    }
    public void requestScheduleData(){
        Calendar today = Calendar.getInstance();
        today.setTimeInMillis(System.currentTimeMillis());
        today.set(Calendar.SECOND, 0);
        today.set(Calendar.HOUR_OF_DAY, 0);
        today.set(Calendar.MINUTE,0);
        today.set(Calendar.MILLISECOND,0);
        today.add(Calendar.DAY_OF_YEAR, 15);
        scheduleList = DatabaseQuery.getScheduleList(userDB.getReadableDatabase(), today.getTimeInMillis());

        //Add new cards to be studied to schedule.
        long timeStamp = System.currentTimeMillis();
        HistoryEntry historyEntry = historyList.get(0);
        final int newCardsStudiedCount = historyEntry.data.get(0,0);
        final int newCardsRemaining = Math.max(UserSettings.getInstance().getUserNewPerSessionLimit() - newCardsStudiedCount, 0);
        for(int i = 0; i < newCardsRemaining; i ++){
            Map.Entry<Long, Integer> entry = new AbstractMap.SimpleEntry<Long, Integer>(timeStamp, 0);
            scheduleList.add(0,entry);
        }
        homeFragment.setScheduleData(scheduleList);
    }
    public void onStudyStart(){
        clearKanjiData();
        undoArray.clear();
        undoDBArray.clear();
        cardFragmentMode = CardFragment.Mode.TEST_FRONT;

        if (studyArray.size() == 0){
            repeatKanjiArray.clear();
            new LoadSessionKanji(studyArray).execute();
        }
        //Log.i("MyActivity.onStudyStart", "studyArray size: " + studyArray.size());
        getFragmentManager().beginTransaction()
                .remove(lookupBar)
                .remove(homeFragment)
                .add(R.id.activityMy , cardFragment)
                .addToBackStack(null)
                .commit();
        //loadCardFragment();
    }
    public void nextKanji(CardFragment.TestModeState state){
        nextKanjiData.cancel();
        previousKanjiData.cancel();
        previousKanjiData = currentKanjiData;
        currentKanjiData = nextKanjiData;
        nextKanjiData = new KanjiData();
        Bundle last;
        final int undoSize = undoDBArray.size();
        switch(state){
            case OFF:
                moveToNextKanjiCursorPosition();
                Log.i("MyActivity", "Load Card from nextKanji OFF.");
                loadCardFragment();
                break;
            case FALSE:
                undoArray.clear();
                if (undoSize > 0) {
                    last = undoDBArray.get(undoSize - 1);
                    undoDBArray.clear();
                    undoDBArray.add(last);
                }
            case STUDY:
                undoArray.add(moveToNextKanjiCursorPosition(true));
                Log.i("MyActivity", "Load Card from nextKanji FALSE/STUDY.");
                loadCardFragment();
                break;
            case TRUE:
                undoArray.clear();
                if (undoSize > 0) {
                    last = undoDBArray.get(undoSize - 1);
                    undoDBArray.clear();
                    undoDBArray.add(last);
                }
                undoArray.add(-2);
                moveToNextKanjiCursorPosition(false);
                if(!isNextAvailable())
                    onStudyComplete();
                else {
                    Log.i("MyActivity", "Load Card from nextKanji True.");
                    loadCardFragment();
                }
                break;
        }
    }
    public void previousKanji(CardFragment.TestModeState state){
        nextKanjiData.cancel();
        previousKanjiData.cancel();
        nextKanjiData = currentKanjiData;
        currentKanjiData = previousKanjiData;
        previousKanjiData = new KanjiData();
        moveToPreviousCursorPosition(state);
        Log.i("MyActivity", "Load Card from previousKanji.");
        loadCardFragment();
    }
    public void cardFragmentRequestData(){
        Log.i("MyActivity", "Load Card from cardFragmentRequest.");
        loadCardFragment();
    }
    public void undoToPreviousKanji(){}
    public void insertKanjiUser(Bundle values){

    }

    public void insertKanji(Bundle values){
        userDB.insertKanji(values);
    }
    public void updateHistory(int stage, boolean undo){
        HistoryEntry entry = historyList.get(0);
        int newValue = undo ? Math.max(entry.data.get(stage, 0) - 1,0) :  entry.data.get(stage, 0) + 1  ;
        entry.data.put(stage, newValue);
        userDB.updateHistoryEntry(entry.date, entry.makeJsonFromData());
    }
    public void updateKanji(Bundle newValues, Bundle oldValues){
        if (oldValues != null) undoDBArray.add(oldValues);
        if (newValues.containsKey(UserSettings.KEY_USER_HISTORY)){
           updateHistory(newValues.getInt(UserSettings.KEY_USER_HISTORY), oldValues == null);
        }
        userDB.updateKanji(newValues);
        //Toast.makeText(getBaseContext(),updateStatement + setStatement + whereStatement, Toast.LENGTH_LONG).show();
    }
    public boolean isNextAvailable(){
        switch(cardFragmentMode){
            case DICTIONARY:
                return kanjiCursor != null && kanjiCursorPosition < kanjiCursorCount - 1;
            default:
                return (studyListPosition < studyArray.size() || repeatKanjiArray.size() > 0);
        }
    }
    public boolean isPreviousAvailable(CardFragment.TestModeState state){
        switch(state){
            case OFF:
                return kanjiCursor != null && kanjiCursorPosition > 0;
            case STUDY:
            default:
                return undoArray.size() > 0;
                //return previousKanjiData!= null && !previousKanjiData.isEmpty();
        }
    }
    public boolean isUndoAvailable(){
        return false;
    }

    public void addDictionaryFragment(){

        if(homeFragment.isAdded()) {
            getFragmentManager().beginTransaction()
                    .remove(homeFragment)
                    .add(R.id.linearLayoutMy, dictionaryList)
                    .addToBackStack(null)
                    .commit();
            cardFragmentMode = CardFragment.Mode.DICTIONARY;
        }
    }


    @Override
    public void updateDictionaryCursor(CharSequence searchQuery) {
        if (!previousSearchQuery.equals(searchQuery.toString())) {
            previousSearchQuery = searchQuery.toString();
            if (dictionaryList.isAdded()) dictionaryList.notifyDataChange();
            Bundle args = new Bundle();
            args.putString(KanjiDatabase.SEARCH_QUERY, searchQuery.toString());
            kanjiCursor = null;
            getSupportLoaderManager().restartLoader(CURSOR_LOADER, args, this);
        }
    }

    public void backupUserDbToFile(){
        JSONObject jsonArray = userDB.makeJsonFromUserDb();
        //File file = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS), "kanjiElements");

        //String filename = "kanjielements.txt";
        try {
            File path = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);
            File file = new File(path,getString(R.string.backup_file));
            //Log.i("MyActivity", "Backup file: " + file.getAbsolutePath());
            file.createNewFile();
            FileOutputStream stream = new FileOutputStream(file);
            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(stream);
            outputStreamWriter.write(jsonArray.toString(3));
            outputStreamWriter.close();
        }
        catch (Exception e){
            Log.e("MyActivity", "Backup file write failed: " +e.toString() );

        }
    }
    public boolean restoreUserDbFromFile(){
        try{
            File path = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);
            File file = new File(path,getString(R.string.backup_file));
            FileInputStream inStream = new FileInputStream(file);
            BufferedReader myReader = new BufferedReader(new InputStreamReader(inStream));
            StringBuilder inputString = new StringBuilder();

            String line;
            while((line = myReader.readLine()) != null){
                inputString.append(line);
            }
            JSONObject input = new JSONObject(inputString.toString());
            if (userDB.restoreUserDBfromJSON(input)){
                resetHome();
                return true;
            }
            else
                return false;

        } catch (Exception e){
            Log.e("MyActivity", "Backup file failed to load: " + e.toString());
            return false;
        }
    }
    private class LoadSessionKanji extends AsyncTask<Integer,Void,List<Integer>>{
        private List<Integer> mList;

        public LoadSessionKanji(List<Integer> list){
            mList = list;
        }
        @Override
        protected List<Integer> doInBackground(Integer... integers) {
            //Randomize dueList here
            UserSettings userSettings = UserSettings.getInstance();

            int sessionLimit = userSettings.getUserSessionLimit();
            int newKanjiLimit = userSettings.getUserNewPerSessionLimit();
//            Cursor cursor = DatabaseQuery.getNeedsCorrectKanji(userDB.getReadableDatabase(), sessionLimit);
//            List<Integer> needsCorrectList = new ArrayList<Integer>();
//            if (cursor != null){
//                final int idCol = cursor.getColumnIndex(DatabaseQuery.COL_USER_KANJI_ID);
//                cursor.moveToFirst();
//                while(!cursor.isAfterLast()){
//                    needsCorrectList.add(userSettings.randomInt(needsCorrectList.size(), 0), cursor.getInt(idCol));
//                    cursor.moveToNext();
//                }
//                cursor.close();
//            }

            Cursor cursor = DatabaseQuery.getDueKanji(userDB.getReadableDatabase(),sessionLimit);
            List<Integer> list = new ArrayList<Integer>();
            List<Integer> needsCorrectList = new ArrayList<Integer>();
            if (cursor != null) {
                final int idCol = cursor.getColumnIndex(DatabaseQuery.COL_USER_KANJI_ID);
                final int needsCorrectCol = cursor.getColumnIndex(DatabaseQuery.COL_USER_KANJI_NEEDS_CORRECT);
                cursor.moveToFirst();
                while (!cursor.isAfterLast()) {
                    if (cursor.getInt(needsCorrectCol) > 0)
                        needsCorrectList.add(userSettings.randomInt(list.size(), 0), cursor.getInt(idCol));
                    else
                        list.add(userSettings.randomInt(list.size(), 0), cursor.getInt(idCol));
                    cursor.moveToNext();
                }
                cursor.close();
            }
            list.addAll(0,needsCorrectList);
            cursor = DatabaseQuery.getNewKanji(userDB.getReadableDatabase(), newKanjiLimit);
            if (cursor != null) {
                final int idCol = cursor.getColumnIndex(DatabaseQuery.COL_KANJI_ID);
                cursor.moveToFirst();
                while (!cursor.isAfterLast()) {
                    list.add(cursor.getInt(idCol));
                    cursor.moveToNext();
                }
                cursor.close();
            }
            //Log.i("MyActivity", "New Kanji Query Size: " + cursor.getCount());
            return list;
        }

        @Override
        protected void onPostExecute(List<Integer> integers) {
            if (integers != null) {
                mList.addAll(integers);
                studyListPosition = 0;
                progressTotal = mList.size();
                progressValue = 1;
                if (cardFragment.isWaitingForData()) {
                    Log.i("MyActivity", "Load Card from Session Loader");
                    loadCardFragment();
                }

            }
        }
    }

    private class GetKanjiCountTask extends AsyncTask<Void, Void, Long> {
        protected Long doInBackground(Void... voids) {
            return kanjiDB.getKanjiCount();
        }

        protected void onPostExecute(Long result) {
            mKanjiCount = result;
            dictionaryList.updateKanjiCount(result);
        }
    }

    private final class GetKanjiData extends AsyncTask<Integer, Void, KanjiData>{

        private KanjiData pointer;
        public GetKanjiData(KanjiData pointer){
            this.pointer = pointer;
        }
        @Override
        protected KanjiData doInBackground(Integer... integers) {
            int id = 0;

            if (integers.length == 1) {
                synchronized (this) {
                    if (kanjiCursor!= null && !kanjiCursor.isClosed() && kanjiCursor.moveToPosition(integers[0])) {
                        final int idCol = kanjiCursor.getColumnIndex("_id");
                        id = kanjiCursor.getInt(idCol);
                    }
                }
            }
            else if (integers.length == 2)
            {
                id = integers[1];
            }
            if(id > 0){


                //LayoutInflater inflater = getLayoutInflater();
                final KanjiData kanjiData = new KanjiData();


                kanjiData.bundle = DatabaseQuery.getKanjiById(userDB.getReadableDatabase(), id);

                switch(cardFragmentMode){
                    case DICTIONARY:
                        kanjiData.bundle.putInt("mode", CardFragment.Mode.DICTIONARY.ordinal());
                        break;
                    default:
                        kanjiData.bundle.putInt("mode", CardFragment.Mode.TEST_FRONT.ordinal());
                        // currentKanjiData.bundle.putInt(UserSettings.KEY_STAGE, newCardSettings.getInt(UserSettings.KEY_STAGE));
                        // currentKanjiData.bundle.putInt(UserSettings.KEY_INTERVAL, newCardSettings.getInt(UserSettings.KEY_INTERVAL));
                        // currentKanjiData.bundle.putString(UserSettings.KEY_HISTORY, newCardSettings.getString(UserSettings.KEY_HISTORY));
                        break;
                }

                //rdng Text
                final String rdng = kanjiData.bundle.getString(UserSettings.KEY_RDNG);
                if (rdng != null) {
                    kanjiData.rdngOn = ParseJSON.makeTextFromJSON(rdng, LayoutTag.Type.RDNG_ON);
                    //Log.i("MyActivity","kanjiData.rdngOn:  "+ kanjiData.rdngOn);
                    kanjiData.rdngKun = ParseJSON.makeTextFromJSON(rdng, LayoutTag.Type.RDNG_KUN);
                    kanjiData.rdngNanori = ParseJSON.makeTextFromJSON(rdng, LayoutTag.Type.RDNG_NANORI);
                }
                //meaning Text
                final String meaning = kanjiData.bundle.getString(UserSettings.KEY_MEANING);
                if (meaning != null)
                    kanjiData.meaning = ParseJSON.makeTextFromJSON(meaning, LayoutTag.Type.MEANING);

                //examples
                final String exampleJSON =  kanjiData.bundle.getString(UserSettings.KEY_EXAMPLE);
                if (exampleJSON != null){
                    final String replacement = getString(R.string.kanji_replacement);
                    final String kanji = kanjiData.bundle.getString(UserSettings.KEY_KANJI);
                    kanjiData.wordData = ParseJSON.makeExampleWordData(exampleJSON,kanji,replacement);
                    kanjiData.nameData = ParseJSON.makeExampleNameData(exampleJSON, kanji, replacement);
                }

                //kanjiData.kanjiDetails = (LinearLayout) inflater.inflate(R.layout.card_container, new LinearLayout(getBaseContext()),false);
                //kanjiData.kanjiCanvas = (KanjiCanvas) kanjiData.kanjiDetails.findViewById(R.id.kanjiConatiner_KanjiCanvas);
               // kanjiData.userKanjiCanvas = (KanjiCanvas) kanjiData.kanjiDetails.findViewById(R.id.kanjiContainer_userKanjiCanvas);
                //kanjiData.kanjiCompareLayout = (RelativeLayout) kanjiData.kanjiDetails.findViewById(R.id.kanjiContainer_compareLayout);
                //kanjiData.kanjiExtraDetails = (LinearLayout) kanjiData.kanjiDetails.findViewById(R.id.kanjiContainer_extraDetail);
               // kanjiData.kanjiStrokeText = (TextView) kanjiData.kanjiDetails.findViewById(R.id.compareLayout_kanjiStrokeText);
                //kanjiData.userStrokeText = (TextView) kanjiData.kanjiDetails.findViewById(R.id.compareLayout_userStrokeText);

               // final String kanji = kanjiData.bundle.getString(UserSettings.KEY_KANJI);
                //if (kanji != null) {
                //    Typeface handWriting = Typeface.createFromAsset(getAssets(), "fonts/851fontzatsu-reduced.ttf");
                //    Typeface brushCursive = Typeface.createFromAsset(getAssets(), "fonts/KouzanBrushFontGyousyo-reduced.ttf");
                    //TextView handWritingView = (TextView) kanjiData.kanjiDetails.findViewById(R.id.styleLayout_handwriting);
                    //TextView brushView = (TextView) kanjiData.kanjiDetails.findViewById(R.id.styelLayout_shoji);
                    //TextView gothicView = (TextView) kanjiData.kanjiDetails.findViewById(R.id.styleLayout_print);
                   // handWritingView.setTypeface(handWriting);
                   // handWritingView.setText(kanji);
                    //brushView.setTypeface(brushCursive);
                   // brushView.setText(kanji);
                    //gothicView.setText(kanji);

               // }
                //final int heisigId = kanjiData.bundle.getInt(UserSettings.KEY_HEISIG);
               // if (heisigId != 0) {
                    //TextView heisigText = (TextView) kanjiData.kanjiDetails.findViewById(R.id.extraText_heisig);
                    //heisigText.setText(Integer.toString(heisigId));

               // }
               // final int strokeCount = kanjiData.bundle.getInt(UserSettings.KEY_STROKE);
               // if (strokeCount != 0) {
                    //TextView strokeText = (TextView) kanjiData.kanjiDetails.findViewById(R.id.extraText_stroke);
               //     strokeText.setText(Integer.toString(strokeCount));
               // }
                final String svgJSON = kanjiData.bundle.getString(UserSettings.KEY_SVG);
                if (svgJSON != null)
                    kanjiData.kanjiPaths = ParseJSON.svgToKanjiPath(svgJSON);

                   //kanjiData.kanjiPaths = ParseJSON.svgToKanjiPath(kanjiData.bundle.getString(UserSettings.KEY_SVG));

                //    kanjiData.kanjiCanvas.setKanjiPaths(kanjiData.kanjiPaths);
               // }
                //LinearLayout kanjiLayout = (LinearLayout) kanjiData.kanjiDetails.findViewById(R.id.cardKanjiContainer);
               // kanjiLayout.setTag(new LayoutTag(LayoutTag.Type.KANJI));
               // kanjiLayout.setOnClickListener(new View.OnClickListener() {
               //     private KanjiCanvas kanjiCanvas = kanjiData.kanjiCanvas;
              //      @Override
               //     public void onClick(View view) {
               //         kanjiCanvas.onClick();
                /*if (userKanjiCanvas.getVisibility() == View.VISIBLE){
                    userKanjiCanvas.onClick();
                }*/
               //     }
               // });
                //if(kanjiDetails != null) cardContainer.removeView(kanjiDetails);
                //kanjiDetails = mListener.getKanjiDetails();
                //cardContainer.addView(kanjiDetails);

        /*

                    ParseJSON.rdngToLayout(rdngJSON, cardContainer);
                if (meaningJSON != null)
                    ParseJSON.meaningToLayout(meaningJSON, cardContainer);
                if (exampleJSON != null)
                    ParseJSON.examplesToLayout(exampleJSON, kanji, cardContainer);   */
                //final String rdngJSON = kanjiData.bundle.getString(UserSettings.KEY_RDNG);
                //if (rdngJSON != null)
                //    ParseJSON.rdngToLayout(rdngJSON,kanjiData.kanjiDetails);
                //final String meaningJSON = kanjiData.bundle.getString(UserSettings.KEY_MEANING);
                //if (meaningJSON != null)
                //    ParseJSON.meaningToLayout(meaningJSON,kanjiData.kanjiDetails);
                //final String exampleJSON = kanjiData.bundle.getString(UserSettings.KEY_EXAMPLE);
                //if (exampleJSON != null)
                //    ParseJSON.examplesToLayout( exampleJSON,
                //        kanjiData.bundle.getString(UserSettings.KEY_KANJI),kanjiData.kanjiDetails);
                final String historyJSON = kanjiData.bundle.getString(UserSettings.KEY_HISTORY);
                if (historyJSON == null)
                    kanjiData.bundle.putString(UserSettings.KEY_HISTORY, "[]");
                return kanjiData;
            }
            else
                return null;
        }

        @Override
        protected void onPostExecute(KanjiData kanjiData) {
            if(kanjiData != null){
                pointer.set(kanjiData);

                if(cardFragment.isWaitingForData()){
                    //waitingForKanjiData = false;
                    //currentKanjiData = pointer;
                    Log.i("MyActivity", "Load card from GetKanjiData.");
                    loadCardFragment();
                }
                if(pointer == nextKanjiData){
                    cardFragment.setNextCard(pointer);
                    //Toast.makeText(getBaseContext(), "Setting Next Card - " + pointer.bundle.getString(UserSettings.KEY_KANJI), Toast.LENGTH_LONG).show();
                }
                else if(pointer == previousKanjiData){
                    cardFragment.setPrevCard(pointer);
                    //Toast.makeText(getBaseContext(), "Setting Previous Card - " + pointer.bundle.getString(UserSettings.KEY_KANJI), Toast.LENGTH_LONG).show();
                }
            }
        }
    }
     /*
    public void loadKanjiData(int offset, int limit){
        Log.i("MyActivity", "making list: " + offset + ", " + limit);
        Bundle args = new Bundle();
        args.putInt("offset", offset);
        args.putInt("limit", limit);

        getSupportLoaderManager().destroyLoader(ARRAY_LOADER);
        getSupportLoaderManager().initLoader(ARRAY_LOADER, args, this);

        if(mKanjiCount == 0) new GetKanjiCountTask().execute();
    }
    public void loadCursor(int offset, int limit){
        Bundle args = new Bundle();
        args.putInt("offset", offset);
        args.putInt("limit", limit);
        getSupportLoaderManager().initLoader(CURSOR_LOADER_BUNDLE, args, this);
    }*/

    @Override
    public void clearUserData() {
        userDB.clearUserData();
        resetHome();
        //reset currentkanjidata and update cardfragment
    }
    private void resetHome(){
        studyArray.clear();
        repeatKanjiArray.clear();
        studyListPosition = 0;
        kanjiCursorPosition = 0;
        undoLockPosition = -1;
        studyListPosition = 0;
        progressTotal = 0;
        progressValue = 0;
        clearKanjiData();
        Calendar date = Calendar.getInstance();
        date.setTimeInMillis(System.currentTimeMillis());
        date.set(Calendar.HOUR_OF_DAY, 0);
        date.set(Calendar.MINUTE, 0);
        date.set(Calendar.SECOND, 0);
        date.set(Calendar.MILLISECOND, 0);
        historyList.clear();

        historyList = DatabaseQuery.getSessionHistoryList(userDB.getReadableDatabase(), date.getTimeInMillis());
        HistoryEntry entry = historyList.get(0);
        Log.i("MyActivity", "Entry Date: " + Long.toString(entry.date) + " " +date.getTimeInMillis());
        userDB.insertHistoryEntry(entry.date);

        undoLockPosition = -1;
        studyListPosition = 0;
        progressTotal = 0;
        progressValue = 0;
    }
    public void setBackgroundOfActivityContainer(int color){
        myActivityLayout.setBackgroundColor(color);
    }
    public void setVisibilityOfContainer(int visibility){
        LinearLayout layout = (LinearLayout) myActivityLayout.findViewById(R.id.linearLayoutMy);
        layout.setVisibility(visibility);

    }
}
