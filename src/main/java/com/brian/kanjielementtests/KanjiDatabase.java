package com.brian.kanjielementtests;


import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteQueryBuilder;
import android.os.Bundle;
import android.util.Log;


import com.readystatesoftware.sqliteasset.SQLiteAssetHelper;

import java.util.ArrayList;

/**
 * Created by BRIAN on 1/18/2015.
 */
public class KanjiDatabase extends SQLiteAssetHelper{

    private static final String DATABASE_NAME = "kanjiele.sqlite";
    private static final int DATABASE_VERSION = 2;

    public static final String SEARCH_QUERY = "search_query";

    public KanjiDatabase(Context context){

        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        setForcedUpgrade(2);
    }

    private static final String GET_KANJI_TAG = "KanjiDatabase.getKanjibyID";
    /*
    public Cursor getKanjibyID(int kanjiID){
        Cursor c = null;
        try {
            SQLiteDatabase db = getReadableDatabase();
            SQLiteQueryBuilder qb = new SQLiteQueryBuilder();


            String[] sqlSelect = {"_id, txt, heisig, stroke_count, rdng, meaning, example, svg"};
            String sqlTables = "kanji";
            //String orderBy = "heisig";
            String whereBy = "_id = " + kanjiID;


            qb.setTables(sqlTables);
            c = qb.query(db, sqlSelect, whereBy, null, null, null, null);


            Log.i(GET_KANJI_TAG, "loading id: " + kanjiID);
            Log.i(GET_KANJI_TAG, "query string: " + qb.buildQuery(sqlSelect, whereBy,null,null,null,null));
            c.moveToFirst();
            Bundle bundle = new Bundle();
            bundle.putInt(UserSettings.KEY_ID, c.getInt(0));
            bundle.putString(UserSettings.KEY_KANJI, c.getString(1));
            bundle.putInt(UserSettings.KEY_HEISIG, c.getInt(2));
            bundle.putInt(UserSettings.KEY_STROKE, c.getInt(3));
            bundle.putString(UserSettings.KEY_RDNG, c.getString(4));
            bundle.putString(UserSettings.KEY_MEANING, c.getString(5));
            bundle.putString(UserSettings.KEY_EXAMPLE, c.getString(6));
            bundle.putString(UserSettings.KEY_SVG, c.getString(7));

        } catch (SQLiteAssetException e){
            e.printStackTrace();
        }
        return c;
    }*/
    public Bundle getKanjibyID(int kanjiID){
        Cursor c = null;
        Bundle bundle = new Bundle();
        try {
            SQLiteDatabase db = getReadableDatabase();
            SQLiteQueryBuilder qb = new SQLiteQueryBuilder();


            String[] sqlSelect = {"_id, txt, heisig, stroke_count, rdng, meaning, example, svg"};
            String sqlTables = "kanji";
            //String orderBy = "heisig";
            String whereBy = "_id = " + kanjiID;


            qb.setTables(sqlTables);
            c = qb.query(db, sqlSelect, whereBy, null, null, null, null);


            Log.i(GET_KANJI_TAG, "loading id: " + kanjiID);
            Log.i(GET_KANJI_TAG, "query string: " + qb.buildQuery(sqlSelect, whereBy,null,null,null,null));
            c.moveToFirst();
            bundle.putInt(UserSettings.KEY_ID, c.getInt(0));
            bundle.putString(UserSettings.KEY_KANJI, c.getString(1));
            bundle.putInt(UserSettings.KEY_HEISIG, c.getInt(2));
            bundle.putInt(UserSettings.KEY_STROKE, c.getInt(3));
            bundle.putString(UserSettings.KEY_RDNG, c.getString(4));
            bundle.putString(UserSettings.KEY_MEANING, c.getString(5));
            bundle.putString(UserSettings.KEY_EXAMPLE, c.getString(6));
            bundle.putString(UserSettings.KEY_SVG, c.getString(7));
            //stageId
            bundle.putInt(UserSettings.KEY_STAGE, 0);
            //intervalId
            bundle.putInt(UserSettings.KEY_INTERVAL,0);
            //pending
            bundle.putInt(UserSettings.KEY_PENDING, 0);
            //needsCorrect
            bundle.putInt(UserSettings.KEY_NEEDS_CORRECT, 0);
            //historyJSON
            bundle.putString(UserSettings.KEY_HISTORY, "[]");

        } catch (SQLiteAssetException e){
            e.printStackTrace();
        }
        return bundle;
    }
    public long getKanjiCount(){
        long count = 0;
        try {
            SQLiteDatabase db = getReadableDatabase();
            count = DatabaseUtils.queryNumEntries(db, "kanji");
        } catch (SQLiteAssetException e){
            e.printStackTrace();
        }
        return count;
    }

    public static final class KanjiCursorLoader extends SimpleCursorLoader {
        KanjiDatabase mHelper;
        int mOffset;
        int mLimit;
        String searchQuery;
        String where;
        String[] selectionArgs;

        public KanjiCursorLoader(Context context, KanjiDatabase helper){
            super(context);
            mHelper = helper;
            mLimit = 0;
            searchQuery = "";
            where = null;
            selectionArgs = null;
        }

        public KanjiCursorLoader(Context context, KanjiDatabase helper, Bundle args){
            super(context);
            mHelper = helper;
            if (args != null) {
                searchQuery = args.getString(SEARCH_QUERY);
                if (!searchQuery.isEmpty()){
                    final char c = searchQuery.charAt(0);
                    Log.i("KanjiCursorLoader", "Character to test: " + c);


                    if (Character.UnicodeBlock.of(c) == Character.UnicodeBlock.HIRAGANA
                            || Character.UnicodeBlock.of(c) == Character.UnicodeBlock.KATAKANA){
                        Log.i("KanjiCursorLoader", "Character is hiragana/katakana");
                        final int length = searchQuery.length();
                        String rebuiltQuery = "";
                        for ( int i = 0; i < length; i ++){
                            rebuiltQuery += "\\u" + Integer.toHexString(searchQuery.charAt(i));
                        }
                        searchQuery = "%" + rebuiltQuery + "%";
                        where = "rdng LIKE ?";

                    }
                    else if (Character.UnicodeBlock.of(c) == Character.UnicodeBlock.CJK_UNIFIED_IDEOGRAPHS) {
                        Log.i("KanjiCursorLoader", "Character is kanji");
                        searchQuery = searchQuery.substring(0,1);
                        where = "txt = ?";
                    }
                    else if (Character.isLetter(c)) {
                        Log.i("KanjiCursorLoader", "Character is letter");
                        searchQuery = "%" + searchQuery + "%";
                        where = "meaning LIKE ?";
                    }
                    else{
                        searchQuery = "";
                        where = null;
                    }
                    selectionArgs = new String[] {searchQuery};
                }
                else {
                    where = null;
                    selectionArgs = null;
                }
                Log.i("KanjiCursorLoader", "search query: " + searchQuery);

                //mOffset = args.getInt("offset");
                //mLimit = args.getInt("limit");
            }
            else{
                mLimit = 0;
            }
        }

        @Override
        public Cursor loadInBackground() {
            Cursor cursor = null;
            try{
                SQLiteDatabase db = mHelper.getReadableDatabase();

                SQLiteQueryBuilder qb = new SQLiteQueryBuilder();
                String [] sqlSelect = {"_id, txt, rdng, meaning"};
                String sqlTables = "kanji";
                String orderBy = "heisig";
                String limit = "";
                if (mLimit > 0){
                    limit = Integer.toString(mOffset)+","+Integer.toString(mLimit);
                }

                Log.i("KanjiCursorLoader", "Query: "+ qb.buildQuery(sqlSelect, where, null,null,orderBy,limit));
                //String limit = "100";
                qb.setTables(sqlTables);
                cursor = qb.query(db,sqlSelect, where,selectionArgs,null,null,orderBy, limit);

                cursor.moveToFirst();

            } catch( SQLiteException e){
                e.printStackTrace();
            }
            return cursor;
        }
    }


    public static final class KanjiArrayLoader extends SimpleCursorLoader {
        KanjiDatabase mHelper;
        //public ArrayList<Kanji> list;
        public ArrayList<DictionaryListFragment.KanjiData> list;
        private String limit;
        public KanjiArrayLoader(Context context, KanjiDatabase helper, Bundle args) {
            super(context);
            mHelper = helper;
            limit = "LIMIT " + Integer.toString(args.getInt("limit")) + " OFFSET " + Integer.toString(args.getInt("offset"));
            Log.i("KanjiLoader", "LIMIT: " + limit);
        }

        @Override
        public Cursor loadInBackground() {
            Cursor cursor = null;
            list = new ArrayList<DictionaryListFragment.KanjiData>();
            try {
                SQLiteDatabase db = mHelper.getReadableDatabase();
                SQLiteQueryBuilder qb = new SQLiteQueryBuilder();
                String[] sqlSelect = {"_id, txt, rdng, meaning"};
                String sqlTables = "kanji";
                String orderBy = "heisig";
                //String limit = "200";
                qb.setTables(sqlTables);
                String rawQuery = "SELECT _id, txt, rdng, meaning FROM kanji ORDER BY heisig ASC " + limit;
                Log.i("KanjiLoader", "SQL: " + rawQuery);
                //cursor = db.query();
                //db.query(sqlTables, sqlSelect, , String[] selectionArgs, String groupBy, String having, String orderBy, limit)
               // cursor = qb.query(db, sqlSelect, null, null, null, null, orderBy, limit);
                cursor = db.rawQuery(rawQuery, null);
                cursor.moveToFirst();

                int idCol = cursor.getColumnIndex("_id");
                int txtCol = cursor.getColumnIndex("txt");
                int rdngCol = cursor.getColumnIndex("rdng");
                int meaningCol = cursor.getColumnIndex("meaning");

                int kanjId = cursor.getInt(idCol);
                String txt = cursor.getString(txtCol);
                String rdngJSON = cursor.getString(rdngCol);
                String meaningJSON = cursor.getString(meaningCol);


                list.add(new DictionaryListFragment.KanjiData(kanjId, txt, ParseJSON.rdngToString(rdngJSON, ParseJSON.RDNG_ON),
                        ParseJSON.rdngToString(rdngJSON, ParseJSON.RDNG_KUN), ParseJSON.meaningToString(meaningJSON)));
                while(cursor.moveToNext()){
                    kanjId = cursor.getInt(idCol);
                    txt = cursor.getString(txtCol);
                    rdngJSON = cursor.getString(rdngCol);
                    meaningJSON = cursor.getString(meaningCol);
                    list.add(new DictionaryListFragment.KanjiData(kanjId, txt, ParseJSON.rdngToString(rdngJSON, ParseJSON.RDNG_ON),
                            ParseJSON.rdngToString(rdngJSON, ParseJSON.RDNG_KUN), ParseJSON.meaningToString(meaningJSON)));
                }


            } catch (SQLiteException e) {
                e.printStackTrace();
            }
            return cursor;
        }
    }
}
