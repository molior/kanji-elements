package com.brian.kanjielementtests;

import android.text.SpannableString;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Brian on 1/28/2015.
 */
public class LayoutTag {
    public enum Type{
        KANJI, EXAMPLE, RDNG_ALL, RDNG_ON, RDNG_KUN, RDNG_NANORI, MEANING
    }
    private Map<Type,CharSequence> map = new HashMap<Type, CharSequence>();
    private Type[] mTypes;
    private CharSequence mDefaultString;
    private Helper mHelper;
    public LayoutTag(Type... types){
        mTypes = types;
    }

    public LayoutTag(Type[] types, CharSequence defaultString){
        mTypes = types;
        mDefaultString = defaultString;
    }
    public LayoutTag(Helper helper,Type... types){
        mTypes = types;
        mHelper = helper;
    }
    public void addReplacementString(Type type, CharSequence replacement){
        final boolean isFound = this.contains(type);
        if (!isFound) return;
        map.put(type,replacement);
    }
    public void toggleLayout(View view, boolean toggle, LayoutTag.Type... types){
        final boolean isFound = this.contains(types);
        if(!isFound) return;
        if(mHelper == null) {
            if (toggle) view.setVisibility(View.GONE);
            else view.setVisibility(View.VISIBLE);
        }
        else{
            final TextView textView = (TextView) view.findViewById(R.id.detailList);
            if (toggle)  mHelper.toggleView(textView,types);
            else mHelper.unToggleView(textView,types);
        }
        /*
        if(!map.keySet().contains(type)){
            if (view.getVisibility() == View.GONE) view.setVisibility(View.VISIBLE);
            else view.setVisibility(View.GONE);
            return;
        }
        final TextView textView = (TextView) view.findViewById(R.id.detailList);
        if (textView != null)
        {
            final String mapString = map.get(type).toString();
            final String textViewString = textView.getText().toString();
            if (!mapString.equals(textViewString))
                textView.setText(map.get(type));
            else{
                textView.setText(mDefaultString);
            }
        }
        */
    }

    /*
    public SpannableString exchangeReplacement(Type type, SpannableString string){
        SpannableString out = mReplacement;
        mReplacement = string;
        return out;
    }*/

    public boolean contains(Type... types){
        for( int i = 0; i < types.length; i ++){
            for (int q = 0; q < mTypes.length; q ++){
                if (types[i] == mTypes[q]) return true;
            }
        }
        return false;
    }
    public interface Helper{
        public abstract void toggleView(View v, Type... types);
        public abstract void unToggleView(View v, Type... types);

    }
}

