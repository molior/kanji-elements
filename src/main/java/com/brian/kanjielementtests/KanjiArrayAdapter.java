package com.brian.kanjielementtests;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ProgressBar;
import android.widget.TextView;


import java.util.ArrayList;
import java.util.Collection;

/**
 * Created by Brian on 2/16/2015.
 */
public class KanjiArrayAdapter extends BaseAdapter {
    ArrayList<Kanji> mList;
    Context mContext;

    private static class ViewHolder{
        TextView kanjiText;
        TextView rdngKunText;
        TextView rdngOnText;
        TextView meaningText;
        ProgressBar progressBar;
    }


    public KanjiArrayAdapter(Context context){
        mList = new ArrayList<Kanji>();
        mContext = context;
    }

    @Override
    public long getItemId(int position){
        return position;
        //return mList.get(position).mId;
    }

    @Override
    public Kanji getItem(int position){
        return mList.get(position);
    }

    @Override
    public int getCount(){
        final int size = mList.size();
        if (size == 0){
            return 1;
        }
        return size;
    }

    public void addAll(Collection<Kanji> list){
        mList.addAll(list);
        notifyDataSetChanged();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent){
        if (mList.size() == 0){
            return newView(parent);
        }
        View view;
        if (convertView == null){
            view = newView(parent);
        }
        else{
            view = convertView;
        }
        bindView(position, view);
        return view;
    }

    private View newView(ViewGroup parent){

        final LayoutInflater inflater = LayoutInflater.from(mContext);
        View v = inflater.inflate(R.layout.item_kanjilist, parent, false);
        ViewHolder viewHolder = new ViewHolder();
        viewHolder.kanjiText = (TextView) v.findViewById(R.id.kanjiText);
        viewHolder.rdngKunText = (TextView) v.findViewById(R.id.rdngKunText);
        viewHolder.rdngOnText = (TextView) v.findViewById(R.id.rdngOnText);
        viewHolder.meaningText = (TextView) v.findViewById(R.id.meaningText);
        viewHolder.progressBar = (ProgressBar) v.findViewById(R.id.progressBar);
        v.setTag(viewHolder);
        return v;
    }

    private void bindView(int position, View view){
        ViewHolder viewHolder = (ViewHolder) view.getTag();
        final Kanji kanji = mList.get(position);
        viewHolder.progressBar.setVisibility(View.GONE);
        viewHolder.kanjiText.setVisibility(View.VISIBLE);
        viewHolder.rdngOnText.setVisibility(View.VISIBLE);
        viewHolder.rdngKunText.setVisibility(View.VISIBLE);
        viewHolder.rdngOnText.setVisibility(View.VISIBLE);
        viewHolder.kanjiText.setText(kanji.mTxt);
        viewHolder.rdngOnText.setText(kanji.mRdngOn);
        viewHolder.rdngKunText.setText(kanji.mRdngKun);
        viewHolder.meaningText.setText(kanji.mMeaning);
    }
}
