package com.brian.kanjielementtests;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Brian on 3/9/2015.
 */
public class ScheduleView extends View {

    public static class Day{
        public static class StageData{
            public int id;
            public int count;
        }
        public String title;
        public int dayValue;
        public List<StageData> stageDataList;
        public Day(){
            super();
        }
    }

    //private static int topGap = 8;
    private final int lineTop_line_width;
    private final int line_vertical_width;
    private final int line_horizontal_width;

    private final int stage_textSize;
    private final int label_textSize;
    private final int lineTop_textSize;
    private final int stage_textAscent;
    private final int stage_textDescent;
    private final int label_textAscent;
    private final int label_textDescent;
    private final int lineTop_textAscent;
    private final int lineTop_textDescent;

    private final int leftPadding;
    private final int rightPadding;
    private final int topPadding;
    private final int bottomPadding;
    private final int stage_boxSize;
    private final int lineTop_boxWidth;


    private final int triangleHeight;
    private final int triangleWidth;

    private static int number_of_divisions = 5;


    private Paint verticalLinePaint;
    private Paint horizontalLinePaint;
    private List<Paint> stageBoxes;
    private Paint lineTop_text;
    private Paint lineTop_box;
    private Paint stageText;
    private Paint labelText;
    private Paint horizontalTriangle;

    //private Canvas mCanvas;
    private Bitmap mBitmap;
    private Paint mBitmapPaint;

    private Path triangle;
    private Paint trianglePaint;

    private List<Day> days;
    private boolean dataValid;

    public ScheduleView(Context context, AttributeSet attrs){
        super(context,attrs);
        dataValid = false;
        verticalLinePaint= new Paint();
        line_vertical_width = context.getResources().getDimensionPixelSize(R.dimen.schedule_vertical_width);
        verticalLinePaint.setStrokeWidth(line_vertical_width);
        verticalLinePaint.setColor(0xFF000000);
        verticalLinePaint.setAntiAlias(true);
        verticalLinePaint.setStyle(Paint.Style.STROKE);
        verticalLinePaint.setStrokeJoin(Paint.Join.MITER);

        horizontalLinePaint = new Paint();
        line_horizontal_width = context.getResources().getDimensionPixelSize(R.dimen.schedule_horizontal_width);
        horizontalLinePaint.setStrokeWidth(line_horizontal_width);
        horizontalLinePaint.setColor(0xFF000000);
        horizontalLinePaint.setAntiAlias(true);
        horizontalLinePaint.setStyle(Paint.Style.STROKE);
        horizontalLinePaint.setStrokeJoin(Paint.Join.MITER);

        leftPadding = context.getResources().getDimensionPixelSize(R.dimen.schedule_left_padding);
        rightPadding = context.getResources().getDimensionPixelSize(R.dimen.schedule_right_padding);
        topPadding = context.getResources().getDimensionPixelSize(R.dimen.schedule_top_padding);
        bottomPadding = context.getResources().getDimensionPixelSize(R.dimen.schedule_bottom_padding);

        lineTop_text = new Paint();

        lineTop_text.setTextSize(context.getResources().getDimensionPixelSize(R.dimen.schedule_sum_text));
        lineTop_text.setTypeface(Typeface.DEFAULT_BOLD);
        lineTop_text.setColor(0xFF000000);
        lineTop_text.setAntiAlias(true);
        lineTop_textAscent = Math.abs(lineTop_text.getFontMetricsInt().ascent);
        lineTop_textDescent = Math.abs(lineTop_text.getFontMetricsInt().descent);
        lineTop_textSize = lineTop_textAscent;

        lineTop_box = new Paint();
        lineTop_box.setColor(0xFF000000);
        lineTop_box.setAntiAlias(true);
        lineTop_box.setStyle(Paint.Style.STROKE);
        lineTop_box.setStrokeCap(Paint.Cap.SQUARE);
        lineTop_box.setStrokeJoin(Paint.Join.MITER);
        lineTop_line_width = context.getResources().getDimensionPixelSize(R.dimen.schedule_line_top_line_width);
        lineTop_box.setStrokeWidth(lineTop_line_width);
        lineTop_boxWidth = context.getResources().getDimensionPixelSize(R.dimen.schedule_line_top_length);

        labelText = new Paint();
        labelText.setColor(0xFF000000);

        labelText.setTextSize(context.getResources().getDimensionPixelSize(R.dimen.schedule_label_text));
        labelText.setAntiAlias(true);
        label_textAscent = Math.abs(labelText.getFontMetricsInt().ascent);
        label_textDescent = Math.abs(labelText.getFontMetricsInt().descent);
        label_textSize = label_textAscent+label_textDescent;

        stageText = new Paint();
        stageText.setColor(Color.BLACK);

        stageText.setTextSize(context.getResources().getDimensionPixelSize(R.dimen.schedule_stage_text));
        stageText.setAntiAlias(true);
        stage_textAscent = Math.abs(stageText.getFontMetricsInt().ascent);
        stage_textDescent = Math.abs(stageText.getFontMetricsInt().descent);
        stage_textSize = stage_textAscent + stage_textDescent/2;
        stage_boxSize = context.getResources().getDimensionPixelSize(R.dimen.schedule_stage_box_size);

        UserSettings userSettings = UserSettings.getInstance();
        final int length = userSettings.getStageCount() - 1; //remove one for final stage not included in schedule
        stageBoxes = new ArrayList<Paint>();
        for (int i = 0; i < length; i ++){
            Paint paint = new Paint();
            paint.setStyle(Paint.Style.FILL);
            UserSettings.Stage stage = userSettings.getStage(i);
            paint.setColor(stage.backgroundColor);

            paint.setAntiAlias(true);
            stageBoxes.add(paint);
        }
        triangle = new Path();


        triangleHeight = context.getResources().getDimensionPixelSize(R.dimen.schedule_triangle_height);
        triangleWidth = context.getResources().getDimensionPixelSize(R.dimen.schedule_triangle_width);
        triangle.moveTo(0,-1*triangleHeight);
        triangle.lineTo(triangleWidth, 0);
        triangle.lineTo(0,triangleHeight);
        triangle.close();
        trianglePaint = new Paint();
        trianglePaint.setAntiAlias(true);
        trianglePaint.setColor(Color.BLACK);
        trianglePaint.setStyle(Paint.Style.FILL);


        dataValid = false;
        mBitmapPaint = new Paint(Paint.DITHER_FLAG);

    }
    public void setDays(List<Day> days) {
        this.days = days;
        if (days != null && days.size() > 0){
            dataValid = true;
            ViewGroup.LayoutParams params = getLayoutParams();
            params.height = calculateHeight();
            //Log.i("HomeFragment", "scheduleCanvas height: " + params.height);
            setLayoutParams(params);
            invalidate();
        }



        //mBitmapPaint.setFilterBitmap(false);
    }
    private void createBitmap(){

        final int verticalLineMaxHeight = calculateMinimumHeight();
        //setMinimumHeight(calculateHeight());

        mBitmap = Bitmap.createBitmap(getWidth(), getHeight(), Bitmap.Config.ARGB_8888);
        Canvas mCanvas = new Canvas(mBitmap);

        //Draw horizontal Line
        final int horizontalLineLength = mBitmap.getWidth() - (leftPadding + rightPadding + triangleWidth);
        mCanvas.save();
        mCanvas.translate(leftPadding, topPadding + verticalLineMaxHeight);
        mCanvas.drawLine(0,0, horizontalLineLength, 0, horizontalLinePaint);
        mCanvas.restore();
        mCanvas.save();
        mCanvas.translate(leftPadding + horizontalLineLength,topPadding + verticalLineMaxHeight);
        mCanvas.drawPath(triangle, trianglePaint);
        mCanvas.restore();
        final int length = days.size() < 5? days.size() : number_of_divisions;
        final int widthBetweenVertical = horizontalLineLength/length;
        for (int i = 0; i < length; i ++){
            Day day = days.get(i);
            mCanvas.save();
            mCanvas.translate(leftPadding + line_vertical_width/2f + i*widthBetweenVertical,  topPadding + verticalLineMaxHeight);
            drawVerticalLine(day,mCanvas);
            mCanvas.restore();
        }
    }
    @Override
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if (!dataValid)
            return;
        if(mBitmap == null)
            createBitmap();
        //canvas.drawColor(Color.WHITE);
        //Log.i("ScheduleView", "canvas width, height: " + canvas.getWidth() + ", " + canvas.getHeight());
        canvas.drawBitmap(mBitmap, 0,0 , mBitmapPaint);


    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        createBitmap();
    }


    private int calculateMinimumHeight(){
        if (!dataValid) return 0;
        final int length = days.size();
        int maxHeight = 0;
       // final int largeFontHeight = Math.abs(lineTop_text.getFontMetricsInt().top) + Math.abs(lineTop_text.getFontMetricsInt().bottom);
       // final int stageFontHeight = Math.abs(stageText.getFontMetricsInt().top) + Math.abs(stageText.getFontMetricsInt().bottom);
        for (int i =0; i< length; i ++){
            int heightTest = days.get(i).stageDataList.size() * stage_textSize + stage_textSize/2;
            if(i == 0)
                heightTest += lineTop_textSize;
            if (heightTest > maxHeight)
                maxHeight = heightTest;
        }
        Log.i("ScheduleCanvas", "maxHeight: " + maxHeight);
        return maxHeight;
    }
    public int calculateHeight(){
        //final int labelTextHeight = label_textSize;
        return calculateMinimumHeight() + topPadding +  label_textSize + bottomPadding;
    }
    public void drawVerticalLine(Day day, Canvas canvas){
        final int length = day.stageDataList.size();
       // final int stageTextHeight =  Math.abs(stageText.getFontMetricsInt().top) + Math.abs(stageText.getFontMetricsInt().bottom);
        //final int largeFontHeight = Math.abs(lineTop_text.getFontMetricsInt().top) + Math.abs(lineTop_text.getFontMetricsInt().bottom);
        final int verticalLineHeight = day.dayValue == 0 ?
                length*stage_textSize+stage_textSize/2+lineTop_textSize/2:length*stage_textSize+stage_textSize/2;
        final int stageBoxMargin = (stage_textSize - stage_boxSize)/2;
        canvas.drawLine(0,0,0,-1*verticalLineHeight,verticalLinePaint);

        //Draw labels
        canvas.drawText(day.title, 0, label_textAscent+line_horizontal_width/2, labelText);
        int sum =0;
        //canvas.translate(0, -1*stageTextHeight);
        canvas.translate(0,-1*stage_textSize/2);
        for(int i = 0; i < length; i ++){
            //if(i==0) canvas.translate(0,-1*stageTextHeight);
            //draw box

            Day.StageData stageData = day.stageDataList.get(i);
            canvas.drawRect(stageBoxMargin, -1*stageBoxMargin - stage_boxSize, stageBoxMargin + stage_boxSize,  -1*stageBoxMargin, stageBoxes.get(stageData.id));
            canvas.drawText(Integer.toString(stageData.count),2*stageBoxMargin+stage_boxSize,-1*(stage_textSize/2+((stage_textDescent-stage_textAscent)/2)), stageText);
            if(day.dayValue == 0) sum += stageData.count;
            canvas.translate(0,-1*stage_textSize);
        }
        if(day.dayValue == 0){
            //canvas.translate(0,-1*stage_textSize);
            canvas.drawLine(line_vertical_width/2f, -1 * lineTop_textSize / 2, stage_boxSize+stageBoxMargin - lineTop_box.getStrokeWidth()/2, -1 * lineTop_textSize / 2, lineTop_box);
            canvas.drawText(Integer.toString(sum), stage_boxSize + 2*stageBoxMargin, -1*(lineTop_textSize/2+(lineTop_textDescent-lineTop_textAscent)/2), lineTop_text);
        }
    }
}
