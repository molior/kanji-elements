package com.brian.kanjielementtests;

import android.app.Application;

/**
 * Created by Brian on 2/25/2015.
 */
public class App extends Application {
    private static App instance;
    public static App getInstance() { return instance; }

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
    }
}
