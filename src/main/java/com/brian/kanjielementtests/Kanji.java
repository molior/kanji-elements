package com.brian.kanjielementtests;

/**
 * Created by Brian on 1/20/2015.
 */
public final class Kanji {
    public int mId;
    public String mTxt;
    public String mRdngOn;
    public String mRdngKun;
    public String mMeaning;

    public Kanji(int id, String txt, String rdngOn, String rdngKun, String meaning){
        mId = id;
        mTxt = txt;
        mRdngOn = rdngOn;
        mRdngKun = rdngKun;
        mMeaning = meaning;
    }
}
