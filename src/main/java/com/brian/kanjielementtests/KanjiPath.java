package com.brian.kanjielementtests;

import android.animation.ObjectAnimator;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ColorFilter;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PathMeasure;
import android.graphics.PixelFormat;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;

import org.apache.http.impl.io.ContentLengthOutputStream;

/**
 * Created by Brian on 1/14/2015.
 */

public class KanjiPath extends Drawable {



    private final Path mPath;
    private final Paint mPaint;
    private final Path renderPath;



    private final PathMeasure measure;
    private float mPhase;
    private final float[] endPoint = new float[2];
    private final float[] endTan = new float[2];

    private float radius;
    private final Paint cPaint;

    private final Paint bPaint;

    public ObjectAnimator animator;
    private float length;


    public KanjiPath(Path path, Paint paint){
        mPhase = 1.0f;
        mPath = new Path(path);
        mPaint = new Paint(paint);
        measure = new PathMeasure(mPath, false);
        length = measure.getLength();

        //drawing marker
        radius = mPaint.getStrokeWidth()*1.5f;
        cPaint = new Paint();
        cPaint.setColor(Color.argb(150,0,0,255));
        cPaint.setAntiAlias(true);

        //background paint
        bPaint = new Paint(mPaint);
        bPaint.setColor(Color.argb(255,200,200,200));

        measure.getPosTan(0, endPoint, endTan);
        renderPath = new Path(mPath);
        renderPath.rLineTo(0.0f,0.0f);
    }
    public KanjiPath (KanjiPath kanjiPath){
        //Path path = new Path(kanjiPath.getPath());
        //Paint paint = new Paint(kanjiPath.getPaint());
        mPhase = 1.0f;
        mPath = new Path(kanjiPath.getPath());
        mPaint = new Paint(kanjiPath.getPaint());
        measure = new PathMeasure(mPath, false);
        length = measure.getLength();

        //drawing marker
        radius = mPaint.getStrokeWidth()*1.5f;
        cPaint = new Paint();
        cPaint.setColor(Color.argb(150,0,0,255));
        cPaint.setAntiAlias(true);

        //background paint
        bPaint = new Paint(mPaint);
        bPaint.setColor(Color.argb(255,200,200,200));

        measure.getPosTan(0, endPoint, endTan);
        renderPath = new Path(mPath);
        renderPath.rLineTo(0.0f,0.0f);

    }

    @Override
    public int getOpacity(){
        return PixelFormat.TRANSLUCENT;
    }
    @Override
    public void setAlpha(int alpha){
        mPaint.setAlpha(alpha);
    }
    @Override
    public void draw(Canvas canvas){
        canvas.drawPath(renderPath, mPaint);
        if (animator != null && animator.isRunning()){
            canvas.drawCircle(endPoint[0],endPoint[1], radius, cPaint);
        }
    }
    public void drawBackground(Canvas canvas){
        canvas.drawPath(mPath,bPaint);
    }
    @Override
    public void setColorFilter(ColorFilter cf){
        mPaint.setColorFilter(cf);
    }

    public void setPhase(float phase){
        mPhase = phase;
        renderPath.reset();
        measure.getSegment(0.0f, mPhase * length, renderPath, true);
        measure.getPosTan(mPhase * length, endPoint, endTan);
        renderPath.rLineTo(0.0f,0.0f);
        invalidateSelf();

    }
    public float getPhase (){
        return mPhase;
    }

    public void computeBounds(RectF rectf){
        boolean exact = true;
        mPath.computeBounds(rectf,exact);
    }
    public void scale(float scale){
        Matrix matrix = new Matrix();
        matrix.setScale(scale, scale);
        mPath.transform(matrix);
        renderPath.transform(matrix);
        measure.setPath(mPath, false);
        length = measure.getLength();
        bPaint.setStrokeWidth(scale*bPaint.getStrokeWidth());
        mPaint.setStrokeWidth(scale*mPaint.getStrokeWidth());
        radius = mPaint.getStrokeWidth()*1.5f;
    }
    public void setStrokeWidth(float width){
        mPaint.setStrokeWidth(width);
        bPaint.setStrokeWidth(width);
        radius = width * 1.5f;
    }
    public Path getPath(){
        return mPath;
    }
    public Paint getPaint(){ return mPaint; }
}
