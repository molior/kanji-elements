package com.brian.kanjielementtests;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.SpannedString;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.Button;
import android.widget.GridLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


public class CardFragment extends Fragment implements View.OnClickListener{

    private DrawingCanvas drawingCanvas;
    private static final int SCREEN_WIDTH = 800;

    //private Cursor cursor;
    //private KanjiDatabase db;


    private LayoutTag.Type[] excludedTags;

    private SetDetailsTask currentTask;
    private SetDetailsTask nextTask;
    private SetDetailsTask previousTask;

    private static int SLIDE_DURATION = 500;
    private String kanji;
    private int kanjiId;
    private int strokeCount;
    private int heisigId;

    private boolean isNew;

    private int stageId;
    private int intervalId;

    private String rdngJSON;
    private String meaningJSON;
    private String exampleJSON;
    private String svgJSON;
    private String historyJSON;

    private int pending;
    private int needsCorrect;
    private boolean isTestMode;
    private TestModeState testModeState;



    private List<Integer> historyList;

    //unused variables
    //private RelativeLayout userStrokeInfo;
    //private LinearLayout kanjiInfo;
    //private LinearLayout kanjiDetails;
    //private LinearLayout kanjiLayout;
    //private KanjiCanvas userKanjiCanvas;
    //private KanjiCanvas kanjiCanvas;
    
    //private LinearLayout cardContainer;

    //container in scroll view
    public RelativeLayout containerInScroll;
    private RelativeLayout canvasButtonLayout;   
    private List<LinearLayout> userKanjiLayoutList;
    
    
    //Detail Containers
    private DetailContainer curDetailContainer;
    private DetailContainer nextDetailContainer;
    private DetailContainer prevDetailContainer;

   //private TextView kanjiStrokeText;
    //private TextView userStrokeText;
   // private TextView handWritingView;
   // private TextView brushView;
   // private TextView gothicView;
   // private TextView heisigText;
   // private TextView strokeText;

    private Button buttonCanvasButton;
    private Button buttonLeft;
    private Button buttonMiddle;
    private Button buttonRight;
    private LinearLayout buttonBar;

    private MenuItem modeDictionary;
    private MenuItem modeTestFront;
    private MenuItem modeTestBack;

    private ScrollView scrollView;
    private ProgressBar progressBar;

    private UserSettings userSettings;
    private UserSettings.Stage stage;
    //private List<KanjiPath> kanjiPaths;

    private LayoutInflater mInflater;
    private RelativeLayout baseLayout;

    private boolean waitingForData;

    private static class DetailContainer{
        
        //valid boolean
        private boolean valid;

        //flip between userStroke and kanjiStroke available
        public boolean canFlipInfoToUser;
        
        public List<KanjiPath> kanjiPaths;
        //container
        public LinearLayout container;

        
        //kanji Layouts
        public LinearLayout kanjiLayout;
        public KanjiCanvas kanjiCanvas;
        
        //kanji - User Stroke
        public RelativeLayout userStrokeInfo;
        public TextView kanjiStrokeText;
        public TextView userStrokeText;
        public KanjiCanvas userKanjiCanvas;
        
        //kanji - Kanji info
        public LinearLayout kanjiInfo;
        public TextView heisigText;
        public TextView strokeText;
        public TextView handWritingText;
        public TextView brushText;
        public TextView gothicText;
        
        //Meaning Text view
        public LinearLayout meaningLayout;
        public TextView meaningText;

        //Reading Layouts
        public LinearLayout rdngAllLayout;
        public LinearLayout rdngOnLayout;
        public LinearLayout rdngKunLayout;
        public LinearLayout rdngNanoriLayout;

        //Reading Text views
        public TextView rdngOnText;
        public TextView rdngKunText;
        public TextView rdngNanoriText;
        //Examples
        public LinearLayout exampleLayout;
        public ArrayList<ExampleDetail> exampleWords;
        public GridLayout exampleNameGridLayout;
        public ArrayList<ExampleName> exampleNames;
        
        public Bundle data;
        public DetailContainer(LayoutInflater inflater){
            valid = false;            
            Context context = inflater.getContext();
            int containerID;
            //Container - Plus kanji
            container =  (LinearLayout) inflater.inflate(R.layout.card_container, new LinearLayout(context),false);

            //Reading Layout
            rdngAllLayout = detailLayoutMaker(inflater, context, LayoutTag.Type.RDNG_ALL);
            container.addView(rdngAllLayout);
            detailLayoutContentMaker(inflater,context,rdngAllLayout, LayoutTag.Type.RDNG_ON);
            detailLayoutContentMaker(inflater,context,rdngAllLayout, LayoutTag.Type.RDNG_KUN);
            detailLayoutContentMaker(inflater,context,rdngAllLayout, LayoutTag.Type.RDNG_NANORI);

            //Meaning Layout
            meaningLayout = detailLayoutMaker(inflater,context, LayoutTag.Type.MEANING);
            container.addView(meaningLayout);
            detailLayoutContentMaker(inflater,context, meaningLayout , LayoutTag.Type.MEANING);

            //Example Layout
            exampleLayout = detailLayoutMaker(inflater, context, LayoutTag.Type.EXAMPLE);
            container.addView(exampleLayout);
            detailLayoutContentMaker(inflater, context, exampleLayout, LayoutTag.Type.EXAMPLE);
            
            //KanjiLayout
            userStrokeInfo = (RelativeLayout) container.findViewById(R.id.kanjiContainer_compareLayout);
            kanjiInfo = (LinearLayout) container.findViewById(R.id.kanjiContainer_extraDetail);
            userKanjiCanvas =  (KanjiCanvas) container.findViewById(R.id.kanjiContainer_userKanjiCanvas);
            kanjiCanvas = (KanjiCanvas) container.findViewById(R.id.kanjiConatiner_KanjiCanvas);

            kanjiStrokeText =(TextView) container.findViewById(R.id.compareLayout_kanjiStrokeText);
            userStrokeText = (TextView) container.findViewById(R.id.compareLayout_userStrokeText);

            Typeface handWriting = Typeface.createFromAsset(context.getAssets(), "fonts/851fontzatsu-reduced.ttf");
            Typeface brushCursive = Typeface.createFromAsset(context.getAssets(), "fonts/KouzanBrushFontGyousyo-reduced.ttf");
            handWritingText = (TextView) container.findViewById(R.id.styleLayout_handwriting);
            brushText = (TextView) container.findViewById(R.id.styelLayout_shoji);
            gothicText = (TextView) container.findViewById(R.id.styleLayout_print);
            handWritingText.setTypeface(handWriting);
            brushText.setTypeface(brushCursive);

            heisigText = (TextView) container.findViewById(R.id.extraText_heisig);
            strokeText = (TextView) container.findViewById(R.id.extraText_stroke);

            kanjiLayout = (LinearLayout) container.findViewById(R.id.cardKanjiContainer);
            kanjiLayout.setTag(new LayoutTag(LayoutTag.Type.KANJI));

            //Onclick listeners
            kanjiCanvas.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    kanjiCanvas.onClick();
                if (userKanjiCanvas.getVisibility() == View.VISIBLE){
                    userKanjiCanvas.onClick();
                }
                }
            });
            kanjiInfo.setOnClickListener( new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(canFlipInfoToUser){
                        kanjiInfo.setVisibility(View.GONE);
                        userStrokeInfo.setVisibility(View.VISIBLE);
                    }
                }
            });
            View.OnClickListener listener = new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    userStrokeInfo.setVisibility(View.GONE);
                    kanjiInfo.setVisibility(View.VISIBLE);
                }
            };

            userStrokeInfo.setOnClickListener(listener);
            userKanjiCanvas.setOnClickListener(listener);
        }
        private LinearLayout detailLayoutMaker(LayoutInflater inflater, Context context, LayoutTag.Type type){
            int containerID;
            LayoutTag tag;
            switch (type)
            {
                case RDNG_ALL:
                    tag = new LayoutTag(type);
                    containerID = context.getResources().getInteger(R.integer.rdng_all);
                    break;
                case MEANING:
                    tag = new LayoutTag(type);
                    containerID = context.getResources().getInteger(R.integer.meaning);
                    break;
                case EXAMPLE:
                default:
                    tag = new LayoutTag(type);
                    containerID = context.getResources().getInteger(R.integer.example);
                    break;
            }
            LinearLayout container = (LinearLayout) inflater.inflate(R.layout.kanji_detailcontainer, new LinearLayout(context), false);
            container.setId(containerID);
            container.setTag(tag);
            return container;
        }
        private void detailLayoutContentMaker(LayoutInflater inflater, Context context, LinearLayout detailLayout, LayoutTag.Type type ){
            String title;
            LayoutTag tag;
            ArrayList<LinearLayout> linearLayouts = new ArrayList<LinearLayout>();
            LinearLayout layout;
            LinearLayout details;
            TextView textView;

            layout = (LinearLayout) inflater.inflate(R.layout.kanji_detailtitle, new LinearLayout(context),false);
            textView = (TextView) layout.findViewById(R.id.detailTitle);
            tag = new LayoutTag(type);
            layout.setTag(tag);
            detailLayout.addView(layout);

            switch (type){
                case RDNG_ON:
                    textView.setText( context.getString(R.string.rdngOn));
                    details = (LinearLayout) inflater.inflate(R.layout.kanji_detail, new LinearLayout(context), false);
                    details.setTag(tag);
                    detailLayout.addView(details);
                    rdngOnLayout = details;
                    rdngOnText = (TextView) details.findViewById(R.id.detailList);
                    break;
                case RDNG_KUN:
                    textView.setText( context.getString(R.string.rdngKun));
                    details = (LinearLayout) inflater.inflate(R.layout.kanji_detail, new LinearLayout(context), false);
                    details.setTag(tag);
                    detailLayout.addView(details);
                    rdngKunLayout = details;
                    rdngKunText = (TextView) details.findViewById(R.id.detailList);

                    break;
                case RDNG_NANORI:
                    textView.setText(context.getString(R.string.rdngNanori));
                    details = (LinearLayout) inflater.inflate(R.layout.kanji_detail, new LinearLayout(context), false);
                    details.setTag(tag);
                    detailLayout.addView(details);
                    rdngNanoriLayout = details;
                    rdngNanoriText = (TextView) details.findViewById(R.id.detailList);
                    break;
                case MEANING:
                    textView.setText(context.getString(R.string.meaning));
                    details = (LinearLayout) inflater.inflate(R.layout.kanji_detail, new LinearLayout(context), false);
                    details.setTag(tag);
                    detailLayout.addView(details);
                    //detailContainer.meaningLayout = details;
                    meaningText = (TextView) details.findViewById(R.id.detailList);
                    break;
                case EXAMPLE:
                    textView.setText(context.getString(R.string.example));
                    exampleWords = new ArrayList<ExampleDetail>();
                    for(int i = 0; i < 12; i ++){
                        ExampleDetail exampleDetail = new ExampleDetail(context,inflater);
                        detailLayout.addView(exampleDetail.container);
                        exampleWords.add(exampleDetail);
                    }
                    exampleNames = new ArrayList<ExampleName>();
                    exampleNameGridLayout = (GridLayout) inflater.inflate(R.layout.kanji_example_name, new GridLayout(context), false);
                    detailLayout.addView(exampleNameGridLayout);
                    final int column = exampleNameGridLayout.getColumnCount();
                    final int row = 4;
                    final int total = column *row;
                    exampleNameGridLayout.setRowCount(row);
                    final int width = exampleNameGridLayout.getWidth()/column;
                    for(int r = 0, c = 0, i = 0; i < total; i ++, c ++ ){
                        if (c == column)
                        {
                            c = 0;
                            r ++;
                        }
                        ExampleName exampleName = new ExampleName(context,inflater);
                        exampleNames.add(exampleName);
                        GridLayout.LayoutParams params = new GridLayout.LayoutParams();
                        params.height = GridLayout.LayoutParams.WRAP_CONTENT;
                        params.width = GridLayout.LayoutParams.WRAP_CONTENT;
                        // params.setGravity(Gravity.CENTER_HORIZONTAL);
                        params.columnSpec = GridLayout.spec(c);
                        params.rowSpec = GridLayout.spec(r);
                        exampleName.container.setLayoutParams(params);
                        exampleNameGridLayout.addView(exampleName.container);
                    }
                    break;

            }
        }        
        public void invalidate(){
            container.setVisibility(View.GONE);
            valid = false;
        }
        public boolean isValid(){
            return valid;
        }        
        public void setViews(MyActivity.KanjiData kanjiData, Context context){

            canFlipInfoToUser = false;
            valid = true;
            data = kanjiData.bundle;
            kanjiPaths = kanjiData.kanjiPaths;
            final String kanji = kanjiData.bundle.getString(UserSettings.KEY_KANJI);
            kanjiLayout.setVisibility(View.VISIBLE);
            userStrokeInfo.setVisibility(View.GONE);
            kanjiInfo.setVisibility(View.VISIBLE);

            brushText.setText(kanji);
            gothicText.setText(kanji);
            handWritingText.setText(kanji);

            final String heisigId = Integer.toString(data.getInt(UserSettings.KEY_HEISIG));
            heisigText.setText(heisigId);
            final String strokeCount = Integer.toString(data.getInt(UserSettings.KEY_STROKE));
            strokeText.setText(strokeCount);

            kanjiCanvas.setKanjiPaths(kanjiPaths);


            rdngAllLayout.setVisibility(View.VISIBLE);


            rdngOnLayout.setVisibility(View.VISIBLE);
            rdngOnText.setText(kanjiData.rdngOn != null && kanjiData.rdngOn.length() > 0 ? kanjiData.rdngOn : "");
            rdngKunLayout.setVisibility(View.VISIBLE);
            rdngKunText.setText(kanjiData.rdngKun != null && kanjiData.rdngKun.length() > 0 ? kanjiData.rdngKun : "");
            rdngNanoriLayout.setVisibility(View.VISIBLE);
            rdngNanoriText.setText(kanjiData.rdngNanori != null && kanjiData.rdngNanori.length() > 0 ? kanjiData.rdngNanori: "");



            meaningLayout.setVisibility(View.VISIBLE);
            meaningText.setText(kanjiData.meaning != null && kanjiData.meaning.length() > 0 ? kanjiData.meaning : "");


            exampleLayout.setVisibility(View.VISIBLE);

            final int layoutCount = exampleWords.size();
            for(int i = 0; i < layoutCount; i ++){
                ExampleDetail exampleDetail = exampleWords.get(i);
                if(exampleDetail.container.getVisibility() == View.VISIBLE)
                    exampleDetail.container.setVisibility(View.INVISIBLE);
            }
           


            int curWordSize = exampleWords.size();
            final int incWordSize = kanjiData.wordData != null ? kanjiData.wordData.size() : -1;
            if (incWordSize != -1 && curWordSize < incWordSize)

            {
                
                LayoutInflater inflater = LayoutInflater.from(context);
                while (curWordSize < incWordSize) {
                    exampleWords.add(new ExampleDetail(context, inflater));
                    curWordSize++;
                }
            }

            for ( int i = 0; i < curWordSize; i++)
            {
                ExampleDetail exampleDetail = exampleWords.get(i);
                if (i < incWordSize) {
                    KanjiHelper.ExampleWordData wordData = kanjiData.wordData.get(i);
                    exampleDetail.container.setTag(wordData.containerTag);
                    exampleDetail.container.setVisibility(View.VISIBLE);
       
                    exampleDetail.kanjiLayout.setTag(wordData.kanjiTag);
                    exampleDetail.kanjiLayout.setVisibility(View.VISIBLE);
                    exampleDetail.kanjiText.setText(wordData.kanji);

                    exampleDetail.readingLayout.setTag(wordData.readingTag);
                    exampleDetail.readingLayout.setVisibility(View.VISIBLE);
                    exampleDetail.readingText.setText(wordData.reading);

                    exampleDetail.meaningLayout.setTag(wordData.meaningTag);
                    exampleDetail.meaningLayout.setVisibility(View.VISIBLE);
                    exampleDetail.meaningText.setText(wordData.meaning);
                    //sentence container
                    if (wordData.sentJ != null) {
                        exampleDetail.sentenceLayout.setTag(wordData.sentJTag);
                        exampleDetail.sentenceLayout.setVisibility(View.VISIBLE);
                        exampleDetail.sentenceText.setText(wordData.sentJ);
                        exampleDetail.sentenceMeaningLayout.setTag(wordData.sentETag);
                        exampleDetail.sentenceMeaningLayout.setVisibility(View.VISIBLE);
                        exampleDetail.sentenceMeaningText.setText(wordData.sentE);
                    } else {
                        exampleDetail.sentenceLayout.setTag(null);
                        exampleDetail.sentenceLayout.setVisibility(View.GONE);
                        exampleDetail.sentenceText.setText("");
                        exampleDetail.sentenceMeaningLayout.setTag(null);
                        exampleDetail.sentenceMeaningLayout.setVisibility(View.GONE);
                        exampleDetail.sentenceMeaningText.setText("");
                    }
                } else {
                    exampleDetail.container.setTag(null);
                    exampleDetail.container.setVisibility(View.GONE);
                    exampleDetail.kanjiLayout.setTag(null);
                    exampleDetail.readingLayout.setTag(null);
                    exampleDetail.meaningLayout.setTag(null);
                    exampleDetail.sentenceLayout.setTag(null);
                    exampleDetail.sentenceMeaningLayout.setTag(null);
                }
            }
                        
            
        
        exampleNameGridLayout.setVisibility(View.VISIBLE);
        final int curNameSize = exampleNames.size();
        final int incNameSize = kanjiData.nameData != null? kanjiData.nameData.size() : -1;
        for (int i = 0; i < curNameSize; i++) {

            ExampleName exampleName = exampleNames.get(i);
            if (i < incNameSize) {
                KanjiHelper.ExampleNameData nameData = kanjiData.nameData.get(i);
                exampleName.container.setVisibility(View.VISIBLE);
                exampleName.kanjiText.setText(nameData.kanji);
                exampleName.kanjiLayout.setTag(nameData.tag);
                exampleName.typeText.setText(nameData.type);
            } else {
                exampleName.container.setVisibility(View.GONE);
                exampleName.kanjiLayout.setTag(null);
                exampleName.kanjiText.setText("");
                exampleName.typeText.setText("");
            }
        }

        }

    }
    private static class ExampleName{
        public LinearLayout container;
        public LinearLayout kanjiLayout;
        public TextView kanjiText;
        public TextView typeText;
        public ExampleName(Context context, LayoutInflater inflater){            
            container = (LinearLayout) inflater.inflate(R.layout.kanji_detailcontainer, new LinearLayout(context), false);
            kanjiLayout = (LinearLayout) inflater.inflate(R.layout.kanji_detail, new LinearLayout(context), false);
            container.addView(kanjiLayout);
            kanjiText = (TextView) kanjiLayout.findViewById(R.id.detailList);
            View view = inflater.inflate(R.layout.kanji_detail, new LinearLayout(context), false);
            container.addView(view);
            typeText = (TextView) view.findViewById(R.id.detailList);            
        }
    }
    private static class ExampleDetail{
        //LinearLayout container
        public LinearLayout container;
        //Text Views and Layouts
        public TextView kanjiText;
        public LinearLayout kanjiLayout;
        public TextView readingText;
        public LinearLayout readingLayout;
        public TextView meaningText;
        public LinearLayout meaningLayout;
        public TextView sentenceText;
        public LinearLayout sentenceLayout;
        public LinearLayout sentenceMeaningLayout;
        public TextView sentenceMeaningText;
        
        public ExampleDetail(Context context, LayoutInflater inflater){
            container = (LinearLayout) inflater.inflate(R.layout.kanji_detailcontainer, new LinearLayout(context), false);
            //Kanji
            kanjiLayout = (LinearLayout) inflater.inflate(R.layout.kanji_detail, new LinearLayout(context), false);
            container.addView(kanjiLayout);
            kanjiText = (TextView) kanjiLayout.findViewById(R.id.detailList);
            //Readings
            readingLayout = (LinearLayout) inflater.inflate(R.layout.kanji_detail, new LinearLayout(context), false);
            container.addView(readingLayout);
            readingLayout.findViewById(R.id.exampleTab).setVisibility(View.INVISIBLE);
            readingText = (TextView) readingLayout.findViewById(R.id.detailList);
            //Meaning
            meaningLayout = (LinearLayout) inflater.inflate(R.layout.kanji_detail, new LinearLayout(context), false);
            container.addView(meaningLayout);
            meaningLayout.findViewById(R.id.exampleTab).setVisibility(View.INVISIBLE);
            meaningText = (TextView) meaningLayout.findViewById(R.id.detailList);
            //Sentence
            sentenceLayout = (LinearLayout) inflater.inflate(R.layout.kanji_detail, new LinearLayout(context), false);

            container.addView(sentenceLayout);
            sentenceLayout.findViewById(R.id.exampleTab).setVisibility(View.VISIBLE);
            sentenceText = (TextView) sentenceLayout.findViewById(R.id.detailList);
            sentenceText.setClickable(true);
            sentenceText.setOnClickListener(new TextView.OnClickListener() {
                public boolean isVisible = false;

                @Override
                public void onClick(View widget) {
                    final TextView textView = (TextView) widget;
                    final SpannedString span = (SpannedString) textView.getText();
                    final FuriganaSpan[] furiganaSpans = span.getSpans(0, span.length(), FuriganaSpan.class);
                    isVisible = !isVisible;
                    for (int i = 0; i < furiganaSpans.length; i++) {
                        furiganaSpans[i].setVisibility(isVisible);
                    }
                    textView.setText(span);

                }
            });

            sentenceMeaningLayout = (LinearLayout) inflater.inflate(R.layout.kanji_detail, new LinearLayout(context), false);
            sentenceMeaningText = (TextView) sentenceMeaningLayout.findViewById(R.id.detailList);
            container.addView(sentenceMeaningLayout);
            sentenceMeaningLayout.findViewById(R.id.exampleTab).setVisibility(View.VISIBLE);
        }

    }

    private final class SetDetailsTask extends AsyncTask<MyActivity.KanjiData,Void, Void>{
        Context context;
        DetailContainer detailContainer;
        public SetDetailsTask (DetailContainer detailContainer){
            this.detailContainer = detailContainer;
        }

        @Override
        protected void onPreExecute() {
            context = getActivity();
        }

        @Override
        protected Void doInBackground(final MyActivity.KanjiData... kanjiDatas) {
            Activity activity = getActivity();
            if (activity != null) {
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        detailContainer.setViews(kanjiDatas[0], context);
                    }
                });

            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            if (detailContainer == curDetailContainer) {
                animateContainerEntrance();
            }
        }
    }
    public static enum TestModeState{
        OFF, STUDY, TRUE, FALSE
    }

    public static enum Mode{
        DICTIONARY, TEST_FRONT, TEST_BACK
    }
    private Mode mode;

    private OnFragmentInteractionListener mListener;

    public static CardFragment newInstance(Bundle args) {
        CardFragment fragment = new CardFragment();
        fragment.setArguments(args);
        return fragment;
    }

    public CardFragment(){

    }
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_card);
        /*if (savedInstanceState == null) {
            getFragmentManager().beginTransaction()
                    .add(R.id.container, new PlaceholderFragment())
                    .commit();
        }*/
       //Bundle bundle = savedInstanceState != null? savedInstanceState:getArguments(); //change for fragment
        //if (savedInstanceState == null)
       //initVariables( bundle);



    }




    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedIntanceState){
        mInflater = inflater;

        userKanjiLayoutList = new ArrayList<LinearLayout>();

        baseLayout = (RelativeLayout) inflater.inflate(R.layout.activity_card, container, false);
        scrollView = (ScrollView) baseLayout.findViewById(R.id.card_scrollView);
        containerInScroll = (RelativeLayout) baseLayout.findViewById(R.id.containerInScroll) ;
        //scrollView.removeView(baseLayout.findViewById(R.id.container));

        //New card Container
        curDetailContainer = new DetailContainer(inflater);
        //cardContainer = inflateCardContainer(inflater, cardDetailContainer);
        //cardContainer.setVisibility(View.INVISIBLE);
        curDetailContainer.container.setVisibility(View.INVISIBLE);
        containerInScroll.addView( curDetailContainer.container);
        
        nextDetailContainer = new DetailContainer(inflater);
        nextDetailContainer.invalidate();
        containerInScroll.addView(nextDetailContainer.container);
        //logViewPosition(nextDetailContainer.container, "nextCard");
        nextDetailContainer.container.setX(0);
        nextDetailContainer.container.setY(0);
        nextDetailContainer.container.setTranslationX(SCREEN_WIDTH);
        //logViewPosition(nextDetailContainer.container, "nextCard");
        prevDetailContainer = new DetailContainer(inflater);
        prevDetailContainer.invalidate();
        containerInScroll.addView(prevDetailContainer.container);
        //logViewPosition(prevDetailContainer.container, "prevCard");
        prevDetailContainer.container.setY(0);
        prevDetailContainer.container.setX(0);
        prevDetailContainer.container.setTranslationX(SCREEN_WIDTH*-1);
        //logViewPosition(prevDetailContainer.container, "prevCard");




        
        progressBar = (ProgressBar) baseLayout.findViewById(R.id.fragmentCardProgressBar);

        canvasButtonLayout = (RelativeLayout) baseLayout.findViewById(R.id.canvasButtonLayout);
        buttonBar = (LinearLayout) baseLayout.findViewById(R.id.buttonBar);
        buttonCanvasButton = (Button) baseLayout.findViewById(R.id.buttonCanvasOkay);
        buttonCanvasButton.setOnClickListener(this);
        buttonLeft = (Button) baseLayout.findViewById(R.id.buttonLeft);
        buttonLeft.setOnClickListener(this);
        buttonRight = (Button) baseLayout.findViewById(R.id.buttonRight);
        buttonRight.setOnClickListener(this);
        buttonMiddle = (Button) baseLayout.findViewById(R.id.buttonMiddle);
        buttonMiddle.setOnClickListener(this);
        drawingCanvas = (DrawingCanvas) baseLayout.findViewById(R.id.drawingCanvas);
        ImageButton close = (ImageButton) baseLayout.findViewById(R.id.buttonClose);
        close.setOnClickListener(this);
        Button clear = (Button) baseLayout.findViewById(R.id.buttonClear);
        clear.setOnClickListener(this);
        Button undo = (Button) baseLayout.findViewById(R.id.buttonUndo);
        undo.setOnClickListener(this);
        //setButtons();
        //setViews();
        return baseLayout;

    }
    private void logViewPosition(View container, String name){
        Log.i("CardActivity", name + "\n: x - " + container.getX() + " y - " + container.getY() +
                "\ttranslationX - " + container.getTranslationX() + " translationY - " + container.getTranslationY()+
        "\nvisibility -" + container.getVisibility());


    }

    @Override
    public void onViewCreated(View view, Bundle savedIntance){
        super.onViewCreated(view, savedIntance);
        Bundle args = new Bundle();
        /*if (isTestMode) {
            args.putInt(UserSettings.KEY_ID, kanjiId);
            args.putInt(UserSettings.KEY_PENDING, pending);
            mListener.updateKanji(args);
        }*/
    }
    private static final String TAG_SET_DATA = "CardActivity.setData";

    @Override
    public void onSaveInstanceState(Bundle state){
        super.onSaveInstanceState(state);
        //state.putInt("mode", mode.ordinal());
    }

    @Override
    public void onResume(){
        super.onResume();
        requestData();
    }
    private void requestData(){
        waitingForData = true;
        mListener.cardFragmentRequestData();
    }
    public boolean isWaitingForData(){return waitingForData;}

    @Override
    public void onPause(){
        super.onPause();
        waitingForData = false;
        final int size = userKanjiLayoutList.size();
        for (int i = 0; i < size; i ++){
            curDetailContainer.container.removeView(userKanjiLayoutList.get(i));
        }
        if (currentTask != null) currentTask.cancel(true);
        if (nextTask != null) nextTask.cancel(true);
        if (previousTask != null) previousTask.cancel(true);
        //scrollView.removeView(cardContainer);
    }
    @Override
    public void onDetach() {
        super.onDetach();
        //scrollView.removeView(cardContainer);
        mListener = null;
    }

    public void reloadFragment(MyActivity.KanjiData kanjiData){
        //setArguments(arguments);
        final long start = System.nanoTime();
        //Log.i("CardFragment", "reloadFragment start: " + System.nanoTime());
        waitingForData = false;
        //logViewPosition(curDetailContainer.container, "reload cur");
        if (curDetailContainer.isValid() && curDetailContainer.container.getVisibility() != View.VISIBLE){
            initVariables(curDetailContainer.data);
            animateContainerEntrance();
        }
        else {
            initVariables(kanjiData.bundle);
            setCurCard(kanjiData);
        }
        setButtons();

         if (isTestMode && isNew){
             Bundle newKanji = new Bundle();
             pending = 0;
             newKanji.putInt(UserSettings.KEY_ID, kanjiId);
             //newKanji.putInt(UserSettings.KEY_PENDING, pending);
             mListener.insertKanji(newKanji);
             isNew = false;
             kanjiData.bundle.putBoolean(UserSettings.KEY_IS_NEW, isNew);
         }

    }
    private void initVariables(Bundle bundle){
        final long start = System.nanoTime();
        //Log.i("CardFragment", "initVariables start: " + System.nanoTime());
        //Bundle bundle = getArguments();
        //from activity
        mode = Mode.values()[bundle.getInt("mode")];
        isTestMode = mode != Mode.DICTIONARY;


        //from static database
        kanjiId = bundle.getInt(UserSettings.KEY_ID);
        kanji = bundle.getString(UserSettings.KEY_KANJI);
        strokeCount = bundle.getInt(UserSettings.KEY_STROKE);
        heisigId = bundle.getInt(UserSettings.KEY_HEISIG);
        rdngJSON = bundle.getString(UserSettings.KEY_RDNG);
        meaningJSON = bundle.getString(UserSettings.KEY_MEANING);
        exampleJSON = bundle.getString(UserSettings.KEY_EXAMPLE);
        svgJSON = bundle.getString(UserSettings.KEY_SVG);

        //From user database
        stageId = bundle.getInt(UserSettings.KEY_STAGE);
        intervalId = bundle.getInt(UserSettings.KEY_INTERVAL);
        historyJSON = bundle.getString(UserSettings.KEY_HISTORY);
        needsCorrect = bundle.getInt(UserSettings.KEY_NEEDS_CORRECT);
        pending = bundle.getInt(UserSettings.KEY_PENDING);
        historyList = ParseJSON.historyToList(historyJSON);


        userSettings = UserSettings.getInstance();
        stage = userSettings.getStage(stageId);

        if (stage == userSettings.getInitialStage() && userSettings.isSkipInitialStage()){
            List<LayoutTag.Type> rdngTypesAvailable = ParseJSON.rdngTypesAvailable(rdngJSON);
            stageId = userSettings.getNextStage(stageId, stage, rdngTypesAvailable);
            stage = userSettings.getStage(stageId);
        }
        if (stage == userSettings.getInitialStage())
            mode = Mode.DICTIONARY;
        excludedTags = (isTestMode) ? stage.excludedTypes: new LayoutTag.Type[]{};

        isNew = bundle.getBoolean(UserSettings.KEY_IS_NEW);
        if (isTestMode) pending = 1;
        //Log.i("CardFragment", "initVariables end: " + (System.nanoTime()-start));
    }
    private void setButtons(){
        final long start = System.nanoTime();
        //Log.i("CardFragment", "setButtons start: " + System.nanoTime());
        //Turn on button bar;
        buttonBar.setVisibility(View.VISIBLE);
        buttonLeft.setText(R.string.button_practice);
        switch(mode){
            case DICTIONARY:
                buttonMiddle.setText(R.string.button_previous);
                if (!mListener.isPreviousAvailable(isTestMode? TestModeState.STUDY: TestModeState.OFF)) {
                    //buttonMiddle.setTextColor(getResources().getColor(R.color.button_inactive_text));
                    buttonMiddle.setEnabled(false);
                }
                else{
                    //buttonMiddle.setTextColor(getResources().getColor(R.color.layout_darkgray));
                    buttonMiddle.setEnabled(true);
                }
                buttonRight.setText(R.string.button_next);
                if(!isTestMode && !mListener.isNextAvailable()) {
                    //buttonRight.setTextColor(getResources().getColor(R.color.button_inactive_text));
                    buttonRight.setEnabled(false);
                }
                else{
                    //buttonRight.setTextColor(getResources().getColor(R.color.layout_darkgray));
                    buttonRight.setEnabled(true);
                }
                buttonCanvasButton.setText(R.string.button_save);
                break;
            case TEST_FRONT:
                if(Arrays.asList(excludedTags).contains(LayoutTag.Type.KANJI)){
                    buttonLeft.setText(R.string.button_canvas);
                    buttonCanvasButton.setText(R.string.button_okay);
                }
                buttonMiddle.setText(R.string.button_skip);
                if(!mListener.isPreviousAvailable(TestModeState.STUDY)) {
                    //buttonMiddle.setTextColor(getResources().getColor(R.color.button_inactive_text));
                    buttonMiddle.setEnabled(false);
                }
                else{
                    buttonMiddle.setTextColor(getResources().getColor(R.color.layout_darkgray));
                    buttonMiddle.setEnabled(true);
                }
                buttonRight.setText(R.string.button_okay);
                break;
            case TEST_BACK:
                buttonMiddle.setText(R.string.button_wrong);
                buttonMiddle.setEnabled(true);
                buttonRight.setText(R.string.button_correct);
                buttonCanvasButton.setText(R.string.button_save);
                break;
        }

    }
    private void setCurCard(final MyActivity.KanjiData kanjiData){
        //final long start = System.nanoTime();
        //final Context context = getActivity();
        if (currentTask != null) currentTask.cancel(true);
        currentTask = new SetDetailsTask(curDetailContainer);
        currentTask.execute(kanjiData);
    }
    public void setNextCard(MyActivity.KanjiData kanjiData){
        if (nextTask != null) nextTask.cancel(true);
        nextTask = new SetDetailsTask(nextDetailContainer);
        nextTask.execute(kanjiData);
    }
    public void setPrevCard(MyActivity.KanjiData kanjiData){
        if (previousTask != null) previousTask.cancel(true);
        previousTask = new SetDetailsTask(prevDetailContainer);
        previousTask.execute(kanjiData);
    }
    private void animateContainerEntrance(){
        Activity mainActivity = getActivity();
        final float translationX = curDetailContainer.container.getTranslationX();
        if (isTestMode) {
            toggleViews(curDetailContainer.container, true, excludedTags);
            if (Arrays.asList(excludedTags).contains(LayoutTag.Type.KANJI))
            //activate canvas
            {
                drawingCanvas.setVisibility(View.VISIBLE);
                canvasButtonLayout.setVisibility(View.VISIBLE);
                buttonBar.setVisibility(View.GONE);
            }
        }
        baseLayout.setBackgroundColor(stage.backgroundColor);

        if (translationX > 0f){
            //animate entrance from the right
            //previousContainer moves left and is invalidated
            curDetailContainer.container.setVisibility(View.VISIBLE);
            curDetailContainer.container.setTranslationX(0);
            //RelativeLayout.LayoutParams params =(RelativeLayout.LayoutParams) curDetailContainer.container.getLayoutParams();

            /*TranslateAnimation animation = new TranslateAnimation(0f, -1* prevDetailContainer.container.getWidth(), 0f, 0f);
            animation.setDuration(800);
            animation.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {
                }

                @Override
                public void onAnimationEnd(Animation animation) {
                    prevDetailContainer.container.setVisibility(View.GONE);
                    prevDetailContainer.container.setTranslationX(-1 * SCREEN_WIDTH);
                    curDetailContainer.container.setX(0);
                }

                @Override
                public void onAnimationRepeat(Animation animation) {
                }
            });
            containerInScroll.startAnimation(animation);*/

            TranslateAnimation curCardAnimation =  new TranslateAnimation(SCREEN_WIDTH, 0, 0,0);

            curCardAnimation.setDuration(SLIDE_DURATION);
            TranslateAnimation prevCardAnimation =  new TranslateAnimation(0f, -1* SCREEN_WIDTH, 0,0);
            prevCardAnimation.setDuration(SLIDE_DURATION);
            prevCardAnimation.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {
                }

                @Override
                public void onAnimationEnd(Animation animation) {
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            prevDetailContainer.container.setVisibility(View.GONE);
                            prevDetailContainer.container.setTranslationX(SCREEN_WIDTH*-1);
                        }
                    });
                    //logViewPosition(prevDetailContainer.container, "after prevCard");

                }

                @Override
                public void onAnimationRepeat(Animation animation) {
                }
            });
            //logViewPosition(prevDetailContainer.container, "before prevCard");
            //logViewPosition(curDetailContainer.container, "before curCard");
            curDetailContainer.container.startAnimation(curCardAnimation);
            prevDetailContainer.container.startAnimation(prevCardAnimation);


        }
        else if (translationX < 0){
            //animate entrance from the right
            //nextContainer moves right and is invalidated
            curDetailContainer.container.setVisibility(View.VISIBLE);
            curDetailContainer.container.setTranslationX(0f);
            TranslateAnimation curCardAnimation = new TranslateAnimation(-1*SCREEN_WIDTH, 0, 0, 0);
            curCardAnimation.setDuration(SLIDE_DURATION);
            nextDetailContainer.container.setTranslationX(0f);
            TranslateAnimation nextCardAnimation = new TranslateAnimation(0,SCREEN_WIDTH, 0 ,0);
            nextCardAnimation.setDuration(SLIDE_DURATION);
            nextCardAnimation.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {
                }

                @Override
                public void onAnimationEnd(Animation animation) {
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            nextDetailContainer.container.setVisibility(View.GONE);
                            nextDetailContainer.container.setTranslationX(SCREEN_WIDTH);
                        }
                    });
                }

                @Override
                public void onAnimationRepeat(Animation animation) {
                }
            });
            curDetailContainer.container.startAnimation(curCardAnimation);
            nextDetailContainer.container.startAnimation(nextCardAnimation);

        }
        else if (curDetailContainer.container.getVisibility() != View.VISIBLE){
            curDetailContainer.container.setVisibility(View.VISIBLE);
            AlphaAnimation alphaAnimation = new AlphaAnimation(0f, 1f);
            alphaAnimation.setDuration(1000);
            alphaAnimation.setFillAfter(true);
            alphaAnimation.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {
                }

                @Override
                public void onAnimationEnd(Animation animation) {
                    progressBar.setVisibility(View.GONE);

                }

                @Override
                public void onAnimationRepeat(Animation animation) {

                }
            });
            curDetailContainer.container.startAnimation(alphaAnimation);
        }
    }
    private static void toggleViews(ViewGroup root, boolean toggle, LayoutTag.Type... tagTypes){
        final int childCount = root.getChildCount();
        for (int i = 0; i < childCount; i++) {
            final View child = root.getChildAt(i);
            if (child instanceof ViewGroup) {
               toggleViews((ViewGroup) child, toggle, tagTypes);
            }

            final Object tagObj = child.getTag();
            if (tagObj != null && tagObj instanceof LayoutTag) {
                ((LayoutTag) tagObj).toggleLayout(child, toggle, tagTypes);
            }

        }
    }


    @Override
    public void onClick(View v){
        DetailContainer invalidatedContainer;
        final int id = v.getId();
        switch(id){
            case(R.id.buttonUndo):
                drawingCanvas.undoLast();
                break;
            case(R.id.buttonClear):
                drawingCanvas.clearCanvas();
                break;
            case(R.id.buttonClose):
                drawingCanvas.setVisibility(View.GONE);
                canvasButtonLayout.setVisibility(View.GONE);
                buttonBar.setVisibility(View.VISIBLE);
                if (!Arrays.asList(excludedTags).contains(LayoutTag.Type.KANJI)){
                    toggleViews(curDetailContainer.container, false, LayoutTag.Type.RDNG_ALL, LayoutTag.Type.EXAMPLE, LayoutTag.Type.MEANING);
                    final int size = userKanjiLayoutList.size();
                    for (int i = 0; i < size; i ++){
                        curDetailContainer.container.removeView(userKanjiLayoutList.get(i));
                    }
                }
                break;
            case(R.id.buttonCanvasOkay):
                if (isTestMode && Arrays.asList(excludedTags).contains(LayoutTag.Type.KANJI)) {
                    canvasButtonCheck();
                    drawingCanvas.clearCanvas();
                    excludedTags = new LayoutTag.Type[]{};
                }
                else
                    canvasButtonSave();
                break;
            case(R.id.buttonLeft):

                drawingCanvas.setVisibility(View.VISIBLE);
                canvasButtonLayout.setVisibility(View.VISIBLE);
                buttonBar.setVisibility(View.GONE);
                if (!Arrays.asList(excludedTags).contains(LayoutTag.Type.KANJI) || mode == Mode.TEST_BACK){
                    //Practice Label
                    toggleViews(curDetailContainer.container, true, LayoutTag.Type.RDNG_ALL, LayoutTag.Type.EXAMPLE, LayoutTag.Type.MEANING);
                    userKanjiLayoutList.clear();
                }
                break;
            case(R.id.buttonRight):
                switch(mode){
                    case DICTIONARY:
                        //NEXT KANJI - Only if available
                        if (isTestMode){
                            Bundle oldValues = new Bundle();
                            oldValues.putInt(UserSettings.KEY_ID, kanjiId);
                            oldValues.putString(UserSettings.KEY_HISTORY,ParseJSON.historytoJSON(historyList));
                            oldValues.putInt(UserSettings.KEY_NEEDS_CORRECT, needsCorrect);
                            oldValues.putInt(UserSettings.KEY_PENDING, pending);
                            oldValues.putInt(UserSettings.KEY_STAGE, stageId);
                            oldValues.putInt(UserSettings.KEY_INTERVAL, intervalId);
                            oldValues.putInt(UserSettings.KEY_USER_HISTORY, stageId);
                            //mListener.updateHistory(stageId);
                            Bundle newValues = (Bundle) oldValues.clone();
                            newValues.putLong(UserSettings.KEY_DUE_DATE, System.currentTimeMillis());
                            newValues.putInt(UserSettings.KEY_STAGE, 1);
                            newValues.putInt(UserSettings.KEY_INTERVAL, 0);
                            newValues.putInt(UserSettings.KEY_USER_HISTORY, stageId);
                            mListener.updateKanji(newValues, oldValues);
                        }
                        nextCard();
                        waitingForData = true;
                        mListener.nextKanji(isTestMode ? TestModeState.STUDY :TestModeState.OFF);
                        break;
                    case TEST_FRONT:
                        //CHECK KANJI

                        mode = Mode.TEST_BACK;
                        //Bundle bundle = getArguments();
                        //bundle.putInt("mode", mode.ordinal());
                        //onSaveInstanceState(bundle);
                        setButtons();
                        toggleViews(curDetailContainer.container, false, excludedTags);
                        excludedTags = new LayoutTag.Type[]{};
                        drawingCanvas.setVisibility(View.GONE);
                        canvasButtonLayout.setVisibility(View.GONE);
                        buttonBar.setVisibility(View.VISIBLE);
                        scrollView.fullScroll(View.FOCUS_UP);
                        break;
                    case TEST_BACK:
                        //USER THOUGHT OF CORRECT KANJI - MOVE TO NEXT KANJI
                        Bundle oldValues = new Bundle();
                        oldValues.putInt(UserSettings.KEY_ID, kanjiId);
                        oldValues.putString(UserSettings.KEY_HISTORY,ParseJSON.historytoJSON(historyList));
                        oldValues.putInt(UserSettings.KEY_NEEDS_CORRECT, needsCorrect);
                        oldValues.putInt(UserSettings.KEY_PENDING, pending);
                        oldValues.putInt(UserSettings.KEY_STAGE, stageId);
                        oldValues.putInt(UserSettings.KEY_INTERVAL, intervalId);
                        oldValues.putInt(UserSettings.KEY_USER_HISTORY, stageId);
                        //mListener.updateHistory(stageId);

                        if (needsCorrect != 1)
                            historyList.add(1); //don't add to history if already tested
                        if (historyList.size() > userSettings.getHistoryCap())
                            historyList.remove(0);
                        pending = 0;
                        needsCorrect = 0;
                        List<LayoutTag.Type> rdngTypesAvailable = ParseJSON.rdngTypesAvailable(rdngJSON);
                        //Log.i("CardActivity", "rdngTypesAvailable: " + rdngTypesAvailable.toString());
                        Bundle args = userSettings.calculateNextInterval(stageId, intervalId, historyList, rdngTypesAvailable);
                        args.putInt(UserSettings.KEY_ID, kanjiId);
                        args.putString(UserSettings.KEY_HISTORY, ParseJSON.historytoJSON(historyList));
                        args.putInt(UserSettings.KEY_NEEDS_CORRECT, needsCorrect);
                        args.putInt(UserSettings.KEY_PENDING, pending);
                        args.putInt(UserSettings.KEY_USER_HISTORY, stageId);
                        mListener.updateKanji(args, oldValues);

                        nextCard();
                        waitingForData = true;
                        mListener.nextKanji(TestModeState.TRUE);
                        break;
                }
                break;
            case(R.id.buttonMiddle):
                switch(mode){
                    case DICTIONARY:
                        //PREVIOUS KANJI
                        waitingForData = true;
                        prevCard();
                        mListener.previousKanji(isTestMode? TestModeState.STUDY : TestModeState.OFF);
                        break;
                    case TEST_FRONT:
                        //UNDO KANJI - PREVIOUS KANJI
                        waitingForData = true;
                        prevCard();
                        mListener.previousKanji(isTestMode? TestModeState.STUDY : TestModeState.TRUE);

                        break;
                    case TEST_BACK:
                        //USER THOUGHT OF *WRONG* KANJI - MOVE TO NEXT KANJI
                        Bundle oldValues = new Bundle();
                        oldValues.putInt(UserSettings.KEY_ID, kanjiId);
                        oldValues.putString(UserSettings.KEY_HISTORY, ParseJSON.historytoJSON(historyList));
                        oldValues.putInt(UserSettings.KEY_NEEDS_CORRECT, needsCorrect);
                        historyList.add(0);
                        if (historyList.size() > userSettings.getHistoryCap())
                            historyList.remove(0);
                        needsCorrect = 1;
                        Bundle args = new Bundle();
                        args.putInt(UserSettings.KEY_ID, kanjiId);
                        args.putString(UserSettings.KEY_HISTORY, ParseJSON.historytoJSON(historyList));
                        args.putInt(UserSettings.KEY_NEEDS_CORRECT, needsCorrect);
                        args.putLong(UserSettings.KEY_DUE_DATE, System.currentTimeMillis());
                        mListener.updateKanji(args, oldValues);
                        nextCard();
                        waitingForData = true;
                        mListener.nextKanji(TestModeState.FALSE);
                        break;
                }
            default:
                break;
        }


    }
    public void nextCard(){
        curDetailContainer.kanjiCanvas.removeSyncToKanjiCanvas();
        curDetailContainer.userKanjiCanvas.removeSyncToKanjiCanvas();
        DetailContainer invalidatedContainer = prevDetailContainer;
        invalidatedContainer.invalidate();
        prevDetailContainer = curDetailContainer;
        curDetailContainer = nextDetailContainer;
        nextDetailContainer = invalidatedContainer;
        scrollView.fullScroll(View.FOCUS_UP);
        //nextDetailContainer.container.setX(0);
        nextDetailContainer.container.setTranslationX(SCREEN_WIDTH);
    }
    public void prevCard(){
        curDetailContainer.kanjiCanvas.removeSyncToKanjiCanvas();
        curDetailContainer.userKanjiCanvas.removeSyncToKanjiCanvas();
        scrollView.fullScroll(View.FOCUS_UP);
        DetailContainer invalidatedContainer = nextDetailContainer;
        invalidatedContainer.invalidate();
        nextDetailContainer = curDetailContainer;
        curDetailContainer = prevDetailContainer;
        prevDetailContainer = invalidatedContainer;
        //prevDetailContainer.container.setX(0);
        prevDetailContainer.container.setTranslationX(-1*SCREEN_WIDTH);
    }
    public void clearData(){
        waitingForData = true;
        scrollView.fullScroll(View.FOCUS_UP);
        curDetailContainer.invalidate();
        prevDetailContainer.invalidate();
        nextDetailContainer.invalidate();
        progressBar.setVisibility(View.VISIBLE);

    }


    /*
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_card, menu);

        modeDictionary = menu.findItem(R.id.action_mode_dictionary);
        modeTestBack = menu.findItem(R.id.action_mode_test_back);
        modeTestFront = menu.findItem(R.id.action_mode_test_front);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        //noinspection SimplifiableIfStatement
        boolean isFound = false;
        switch(id){
            case R.id.action_mode_dictionary:
                isFound = true;
                if (item.isChecked()) break;
                item.setChecked(true);
                modeTestBack.setChecked(false);
                modeTestFront.setChecked(false);
                mode = Mode.DICTIONARY;
                setButtons();
                break;
            case R.id.action_mode_test_back:
                isFound = true;
                if (item.isChecked()) break;
                item.setChecked(true);
                modeDictionary.setChecked(false);
                modeTestFront.setChecked(false);
                mode = Mode.TEST_BACK;
                setButtons();
                break;

            case R.id.action_mode_test_front:
                isFound = true;
                if (item.isChecked()) break;
                item.setChecked(true);
                modeDictionary.setChecked(false);
                modeTestBack.setChecked(false);
                mode = Mode.TEST_FRONT;
                setButtons();
                break;
            case R.id.action_tag_kanji:
                isFound = true;
                toggleViews(cardContainer, item.isChecked(), LayoutTag.Type.KANJI);
                item.setChecked(!item.isChecked());
                break;
            case R.id.action_tag_rdng_all:
                isFound = true;
                toggleViews(cardContainer, item.isChecked(), LayoutTag.Type.RDNG_ALL);
                item.setChecked(!item.isChecked());
                break;
            case R.id.action_tag_rdng_on:
                isFound = true;
                toggleViews(cardContainer, item.isChecked(), LayoutTag.Type.RDNG_ON);
                item.setChecked(!item.isChecked());
                break;
            case R.id.action_tag_rdng_kun:
                isFound = true;
                toggleViews(cardContainer, item.isChecked(), LayoutTag.Type.RDNG_KUN);
                item.setChecked(!item.isChecked());
                break;
            case R.id.action_tag_rdng_nanori:
                isFound = true;
                toggleViews(cardContainer, item.isChecked(), LayoutTag.Type.RDNG_NANORI);
                item.setChecked(!item.isChecked());
                break;
            case R.id.action_tag_meaning:
                isFound = true;
                toggleViews(cardContainer, item.isChecked(), LayoutTag.Type.MEANING);
                item.setChecked(!item.isChecked());
                break;
            case R.id.action_tag_example:
                isFound = true;
                toggleViews(cardContainer, item.isChecked(), LayoutTag.Type.EXAMPLE);
                item.setChecked(!item.isChecked());
                break;
            case R.id.action_cycle_stage:
                isFound = true;
                stageId = userSettings.getNextStageId(stageId);
                if (stageId < 0) stageId = 0;
                stage = userSettings.getStage(stageId);
                scrollView.setBackgroundColor(stage.backgroundColor);

                toggleViews(cardContainer, false, excludedTags);
                excludedTags = stage.excludedTypes;
                setButtons();
                toggleViews(cardContainer, true, excludedTags);
                break;
        }
        if (isFound){
            //final int length = excludedTags.size();
            //for (int i = 0; i < length; i ++){
                //toggleViews(cardContainer, excludedTags.get(i));
            //}
            return true;
        }
        /*if (id == R.id.action_tag_example) {
            return true;
        }*/

     //   return super.onOptionsItemSelected(item);
   // }
    private void canvasButtonCheck(){

        List<Path> userPaths = drawingCanvas.getPaths();
        final int kanjiLength = curDetailContainer.kanjiPaths.size();
        final int userLength = userPaths.size();
        Paint paint = new Paint();
        paint.setAntiAlias(true);
        paint.setDither(false);
        paint.setStrokeWidth(5);
        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeCap(Paint.Cap.ROUND);
        List<KanjiPath> userKanjiPaths = new ArrayList<KanjiPath>();
        for(int i = 0; i  < userLength; i ++){
            paint = new Paint(paint);
            if (i < kanjiLength)
                paint.setColor(curDetailContainer.kanjiPaths.get(i).getPaint().getColor());
            else
                paint.setColor(getResources().getColor(R.color.over_stroke));
            KanjiPath kanjiPath = new KanjiPath(userPaths.get(i),paint);
            userKanjiPaths.add(kanjiPath);
        }
        //kanjiStroke = (TextView) baseLayout.findViewById(R.id.textKanjiStroke);

        curDetailContainer.kanjiStrokeText.setText(Integer.toString(kanjiLength));
        //final TextView userStrokeText = (TextView) baseLayout.findViewById(R.id.textUserKanjiStroke);
        curDetailContainer.userStrokeText.setText(Integer.toString(userLength));
        if (userLength != kanjiLength)
            curDetailContainer.userStrokeText.setTextColor(getResources().getColor(R.color.over_stroke_text));
        else
            curDetailContainer.userStrokeText.setTextColor(getResources().getColor(R.color.draw_canvas_stroke));

        curDetailContainer.kanjiInfo.setVisibility(View.GONE);
        curDetailContainer.userStrokeInfo.setVisibility(View.VISIBLE);
        curDetailContainer.userKanjiCanvas.setVisibility(View.VISIBLE);
        curDetailContainer.userKanjiCanvas.setKanjiPaths(userKanjiPaths, drawingCanvas.getHeight());
        curDetailContainer.kanjiCanvas.syncToKanjiCanvas(curDetailContainer.userKanjiCanvas);
        curDetailContainer.canFlipInfoToUser = true;

        mode = Mode.TEST_BACK;
        //Bundle bundle = getArguments();
        //bundle.putInt("mode", mode.ordinal());
        //onSaveInstanceState(bundle);


        setButtons();
        toggleViews(curDetailContainer.container,false,excludedTags);
        drawingCanvas.setVisibility(View.GONE);
        canvasButtonLayout.setVisibility(View.GONE);
        buttonBar.setVisibility(View.VISIBLE);
    }

    private void canvasButtonSave(){


        final int size = userKanjiLayoutList.size();
        LinearLayout linearLayout;
        if (size == 0) {
            linearLayout = (LinearLayout) mInflater.inflate(R.layout.linearlayout_horizontal,
                    new LinearLayout(curDetailContainer.container.getContext()), false);
            userKanjiLayoutList.add(linearLayout);
            curDetailContainer.container.addView(linearLayout, 2);
        } else if (userKanjiLayoutList.get(size - 1).getChildCount() == 3){
            linearLayout = (LinearLayout) mInflater.inflate(R.layout.linearlayout_horizontal,
                    new LinearLayout(curDetailContainer.container.getContext()), false);
            userKanjiLayoutList.add(linearLayout);
            curDetailContainer.container.addView(linearLayout, 2);
        }
        else linearLayout = userKanjiLayoutList.get(size - 1);


        Bitmap bitmap = drawingCanvas.getBitmap();
        final int width = (curDetailContainer.container.getWidth() -
                curDetailContainer.container.getPaddingLeft() - curDetailContainer.container.getPaddingRight() -
                getResources().getDrawable(R.drawable.horizontal_clear_divider).getIntrinsicWidth()*2)/3;
        ImageView imageView = new ImageView(getActivity().getBaseContext()); //count number of image views
        bitmap = Bitmap.createScaledBitmap(bitmap, width,width,true);
        imageView.setImageBitmap(bitmap);
        linearLayout.addView(imageView);
        drawingCanvas.clearCanvas();
    }

    /**
     * A placeholder fragment containing a simple view.

    public static class PlaceholderFragment extends Fragment {

        public PlaceholderFragment() {
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_card, container, false);
            return rootView;
        }
    }*/
    public interface OnFragmentInteractionListener {
        public void nextKanji(TestModeState state);
        public void previousKanji(TestModeState state);
        public void undoToPreviousKanji();
        public void updateKanji(Bundle newValues, Bundle oldValues);
        public void insertKanji(Bundle values);
        public boolean isPreviousAvailable(TestModeState state);
        public boolean isNextAvailable(); //only used for dictionary mode
        public boolean isUndoAvailable();
        public void cardFragmentRequestData();
        public LinearLayout getKanjiDetails();
        public List<KanjiPath> getKanjiPaths();

    }
}
