package com.brian.kanjielementtests;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Brian on 3/11/2015.
 */
public class TotalsView extends View {

    private int boxCount;
    private List<Paint> colorList;
    private final int boxSize;
    private final int boxPadding;
    private boolean readyToDraw;
    private Canvas mCanvas;
    private Bitmap mBitmap;
    private Paint mPaintBitmap;
    private Paint mPaintClear;

    private int mWidth;
    private int mHeight;

    private Paint boxPaint;
    private int colCount;
    private int rowCount;


    private List<UserSettings.Stage> itemList;




    public TotalsView (Context context, AttributeSet attrs){
        super(context, attrs);
        UserSettings userSettings = UserSettings.getInstance();
        final int stageCount = userSettings.getStageCount();
        boxSize = context.getResources().getDimensionPixelSize(R.dimen.totals_box_size);
        boxPadding = context.getResources().getDimensionPixelSize(R.dimen.totals_box_padding);
        /*
        mPaintClear = new Paint();
        mPaintClear.setStyle(Paint.Style.FILL);
        mPaintClear.setAntiAlias(true);
        mPaintClear.setColor(0x00FFFFFF);
        colorList = new ArrayList<Paint>();
        for (int i = 0; i < stageCount; i++){
            Paint stagePaint = new Paint(mPaintClear);
            stagePaint.setColor(userSettings.getStage(i).backgroundColor);
            colorList.add(stagePaint);
        }*/

        boxPaint = new Paint();
        boxPaint.setStyle(Paint.Style.FILL);
        boxPaint.setAntiAlias(true);

        mPaintBitmap = new Paint();
        mPaintBitmap.setDither(true);
        readyToDraw = false;
        mWidth = context.getResources().getDimensionPixelSize(R.dimen.totals_view_width);


    }

    public void resetView(int count){
        boxCount = count;
        itemList = new ArrayList<UserSettings.Stage>(boxCount);
        mHeight = calculateHeight();
        ViewGroup.LayoutParams params = getLayoutParams();
        params.height = mHeight;
        setLayoutParams(params);
        Log.i("TotalsView", "View width, height: " + mWidth + ", " + mHeight);
        mBitmap = Bitmap.createBitmap(mWidth, mHeight, Bitmap.Config.ARGB_8888);
        mCanvas = new Canvas(mBitmap);
        readyToDraw = false;
        //colCount =  width/(boxPadding+boxSize);
        //rowCount = (int)Math.ceil((double)boxCount / (double)colCount);
    }
    public void addItem(UserSettings.Stage stage){
        final int pos = itemList.size();
        itemList.add(stage);
        drawItem(pos, stage);
    }
    public void setItem(int pos, UserSettings.Stage stage){
        itemList.set(pos, stage);
        drawItem(pos, stage);
    }
    public int getSumForStage(UserSettings.Stage stage){
        int sum = 0;
        for (int i = 0; i < boxCount; i ++){
            if(itemList.get(i) == stage) sum ++;
        }
        return sum;
    }
    public void drawItem(int pos, UserSettings.Stage stage){
        final int row = pos / colCount;
        final int col = pos % colCount;
        final int cellSize = boxSize+boxPadding;
        boxPaint.setColor(stage.backgroundColor);
        mCanvas.save();
        mCanvas.translate(col*cellSize, row*cellSize);
        mCanvas.drawRect(0,0,boxSize, boxSize,boxPaint);
        mCanvas.restore();
        readyToDraw = true;
    }
    private int calculateHeight(){
        colCount = mWidth/(boxSize + boxPadding);
        rowCount = (int)Math.ceil((double)boxCount / (double)colCount);
        return rowCount*(boxSize+boxPadding);
    }
    @Override
    public void onDraw(Canvas canvas){
        if(readyToDraw){
            canvas.drawBitmap(mBitmap, 0,0,mPaintBitmap);
        }
    }
}
