package com.brian.kanjielementtests;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Typeface;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.SpannedString;
import android.text.method.LinkMovementMethod;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android.text.style.StyleSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.larvalabs.svgandroid.SVGParser;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;


@SuppressWarnings("ForLoopReplaceableByForEach")
public class ParseJSON {
    public static final int RDNG_ON = 1;
    public static final int RDNG_KUN = 2;
    public static final int RDNG_NANORI = 3;
    private static String RDNG_SEP = ", ";
    private static String MEANING_SEP = ", ";

    private static int codePointStart = 0x2460;
    private static int codePointEnd = 0x2473;

    private static String getNameType(String in){
        if (in.length() == 0) return "";
        char character = in.charAt(0);
        switch(character){
            case 'p':
                return "PLACE NAME";
            case 's':
                return "SURNAME";
            case 'f':
                return "FEMALE NAME";
            case 'm':
                return "MALE NAME";
            default:
                return "";
        }
    }
    public static String historytoJSON(List<Integer> list){
        JSONArray output = new JSONArray();
        final int length = list.size();
        for(int i = 0; i< length; i ++){
            output.put(list.get(i));
        }
        return output.toString();
    }
    public static List<Integer> historyToList(String in){
        List<Integer> list = new ArrayList<Integer>();
        try{
            JSONArray reader = new JSONArray(in);
            final int length = reader.length();
            for (int i = 0; i < length; i ++){
                list.add(reader.getInt(i));
            }
        }catch (Exception e){

            e.printStackTrace();
        }
        return list;
    }
    public static List<LayoutTag.Type> rdngTypesAvailable(String in){
        List<LayoutTag.Type> list = new ArrayList<LayoutTag.Type>();
        try {
            JSONArray reader = new JSONArray(in);
            final int count = reader.length();
            boolean rdngOn = false;
            boolean rdngKun = false;
            boolean rdngNanori = false;
            for (int i = 0; i < count; i++) {
                JSONObject each = reader.getJSONObject(i);
                final int eachType = each.getInt("rtype");
                if (eachType ==RDNG_ON && !rdngOn){
                    list.add(LayoutTag.Type.RDNG_ON);
                    rdngOn = true;
                }
                if (eachType == RDNG_KUN && !rdngKun){
                    list.add(LayoutTag.Type.RDNG_KUN);
                    rdngKun = true;
                }
                if (eachType == RDNG_NANORI && ! rdngNanori){
                    list.add(LayoutTag.Type.RDNG_NANORI);
                    rdngNanori = true;
                }
                if (rdngOn && rdngNanori && rdngKun)
                    break;
            }
        } catch (Exception e){
            e.printStackTrace();
        }
        return list;
    }
    public static String rdngToString(String in, int rdngType){
        String out = "";
        try {
            JSONArray reader = new JSONArray(in);
            final int count = reader.length();

            for (int i = 0; i < count; i++) {
                JSONObject each = reader.getJSONObject(i);
                final int eachType = each.getInt("rtype");
                if (eachType ==rdngType){
                    if (out.length() != 0) {
                        out += RDNG_SEP;
                    }
                    out += each.getString("txt");
                }
            }

        } catch (Exception e){

            e.printStackTrace();
        }
        return out;
    }

    public static String meaningToString(String in){
        String out = "";
        try{
            JSONArray reader = new JSONArray(in);

            final int count = reader.length();

            for (int i = 0; i < count; i ++){
                JSONObject each = reader.getJSONObject(i);
                if (out.length() != 0) {
                    out += RDNG_SEP;
                }
                out += each.getString("txt");
            }

        } catch (Exception e){
            e.printStackTrace();
        }
        return out;
    }
    public static void rdngToLayout(String in, ViewGroup parent){
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);
        int containerID = context.getResources().getInteger(R.integer.rdng_all);
        LayoutTag tag = new LayoutTag(LayoutTag.Type.RDNG_ALL);
        LinearLayout layout = (LinearLayout) inflater.inflate(R.layout.kanji_detailcontainer, new LinearLayout(context), false);
        layout.setId(containerID);
        layout.setTag(tag);
        parent.addView(layout);
        jsonToLayout(in, layout,LayoutTag.Type.RDNG_ON);
        jsonToLayout(in,layout,LayoutTag.Type.RDNG_KUN);
        jsonToLayout(in,layout,LayoutTag.Type.RDNG_NANORI);
    }
    public static void meaningToLayout(String in, ViewGroup parent){
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);
        int containerID = context.getResources().getInteger(R.integer.meaning);
        LayoutTag tag = new LayoutTag(LayoutTag.Type.MEANING);
        LinearLayout layout = (LinearLayout) inflater.inflate(R.layout.kanji_detailcontainer, new LinearLayout(context), false);
        layout.setId(containerID);
        layout.setTag(tag);
        parent.addView(layout);
        jsonToLayout(in, layout, LayoutTag.Type.MEANING);
    }
    public static CharSequence makeTextFromJSON(String in, LayoutTag.Type type){

        CharSequence output = null;
        try{
            JSONArray reader = new JSONArray(in);
            SpannableStringBuilder reading = new SpannableStringBuilder();
            final int count = reader.length();
            float baseValue = 1;
            int rType;
            switch (type)
            {
                case RDNG_ON:
                    rType = RDNG_ON;
                    break;
                case RDNG_KUN:
                    rType = RDNG_KUN;
                    break;
                case RDNG_NANORI:
                    rType = RDNG_NANORI;
                    break;
                default:
                    rType = -1;
                    break;
            }

            boolean needsSep = false;
            for (int i = 0; i<count; i++){

                JSONObject each = reader.getJSONObject(i);
                if (type != LayoutTag.Type.MEANING && each.getInt("rtype") != rType) continue;

                final String txt = needsSep? RDNG_SEP + each.getString("txt") : each.getString("txt");


                int len = txt.length();
                if (i==0) baseValue = each.getLong("freq");
                float freq = (float)( each.getDouble("freq") / baseValue);

                SpannableString string = new SpannableString(txt);
                if (freq  >= 0.9f) string.setSpan(new StyleSpan(Typeface.BOLD),0,len, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                if (freq  >= 0 && freq < 0.5f) string.setSpan(new RelativeSizeSpan(0.8f),0,len, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                if (freq == 0) string.setSpan(new ForegroundColorSpan(Color.LTGRAY),0, len, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                reading.append(string);
                needsSep = true;
            }
            output = reading;

            //out.add(layout);

        } catch (Exception e){
            e.printStackTrace();
        }

        return output;
    }
    private static void jsonToLayout(String in, ViewGroup parent, LayoutTag.Type type){
        //List<LinearLayout> out = new ArrayList<LinearLayout>();
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);
        int containerID;
        String title;
        LayoutTag tag;
        int rType;


        switch(type){
            case RDNG_ON:
                containerID = context.getResources().getInteger(R.integer.rdng_on);
                title = context.getResources().getString(R.string.rdngOn);
                tag = new LayoutTag(LayoutTag.Type.RDNG_ON);
                rType = RDNG_ON;
                break;
            case RDNG_KUN:
                containerID = context.getResources().getInteger(R.integer.rdng_kun);
                title = context.getResources().getString(R.string.rdngKun);
                tag = new LayoutTag(LayoutTag.Type.RDNG_KUN);
                rType = RDNG_KUN;
                break;
            case RDNG_NANORI:
                containerID = context.getResources().getInteger(R.integer.rdng_nanori);
                title = context.getResources().getString(R.string.rdngNanori);
                tag = new LayoutTag(LayoutTag.Type.RDNG_NANORI);
                rType = RDNG_NANORI;
                break;
            case MEANING:
            default:
                containerID = context.getResources().getInteger(R.integer.meaning) + 1;
                title = context.getResources().getString(R.string.meaning);
                tag = new LayoutTag(LayoutTag.Type.MEANING);
                rType = 0;
                break;
            //containerID =0;
            //title = "String";
            //tag = new LayoutTag(new LayoutTag.Type[]{LayoutTag.Type.MEANING});
            //break;
        }


        /*
         * CREATE LAYOUT TITLE
         */
        LinearLayout titleLayout = (LinearLayout) inflater.inflate(R.layout.kanji_detailtitle, new LinearLayout(context), false);
        TextView titleTextView = (TextView) titleLayout.findViewById(R.id.detailTitle);
        titleTextView.setText(title);
        titleLayout.setTag(tag);
        titleLayout.setId(containerID);
        parent.addView(titleLayout);
        containerID += 1;
        try{
            JSONArray reader = new JSONArray(in);
            SpannableStringBuilder reading = new SpannableStringBuilder();
            final int count = reader.length();
            float baseValue = 1;

            boolean needsSep = false;
            for (int i = 0; i<count; i++){

                JSONObject each = reader.getJSONObject(i);
                if (type != LayoutTag.Type.MEANING && each.getInt("rtype") != rType) continue;

                final String txt = needsSep? RDNG_SEP + each.getString("txt") : each.getString("txt");


                int len = txt.length();
                if (i==0) baseValue = each.getLong("freq");
                float freq = (float)( each.getDouble("freq") / baseValue);

                SpannableString string = new SpannableString(txt);
                if (freq  >= 0.9f) string.setSpan(new StyleSpan(Typeface.BOLD),0,len, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                if (freq  >= 0 && freq < 0.5f) string.setSpan(new RelativeSizeSpan(0.8f),0,len, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                if (freq == 0) string.setSpan(new ForegroundColorSpan(Color.LTGRAY),0, len, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                reading.append(string);
                needsSep = true;
            }
            LinearLayout layout = (LinearLayout) inflater.inflate(R.layout.kanji_detail, new LinearLayout(context), false);
            TextView textView = (TextView) layout.findViewById(R.id.detailList);
            textView.setText(reading);
            layout.setTag(tag);
            layout.setId(containerID);
            parent.addView(layout);
            //out.add(layout);

        } catch (Exception e){
            e.printStackTrace();
        }
        //return out;
    }
    public static List<LinearLayout> readingToLayout(String in, LayoutInflater inflater, int ReadingType){
        List<LinearLayout> out = new ArrayList<LinearLayout>();
        try{
            JSONArray reader = new JSONArray(in);
            SpannableStringBuilder reading = new SpannableStringBuilder();
            final int count = reader.length();
            float baseValue = 1;

            boolean needsSep = false;
            for (int i = 0; i<count; i++){

                JSONObject each = reader.getJSONObject(i);

                if (each.getInt("rtype") != ReadingType) continue;
                if (needsSep) reading.append(RDNG_SEP);

                int len = each.getString("txt").length();
                if (i==0) baseValue = each.getLong("freq");
                float freq = (float)( each.getDouble("freq") / baseValue);
                SpannableString string = new SpannableString(each.getString("txt"));
                if (freq  >= 0.9f) string.setSpan(new StyleSpan(Typeface.BOLD),0,len, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                if (freq  >= 0 && freq < 0.5f) string.setSpan(new RelativeSizeSpan(0.8f),0,len, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                if (freq == 0) string.setSpan(new ForegroundColorSpan(Color.LTGRAY),0, len, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                reading.append(string);
                needsSep = true;
            }
            LinearLayout layout = (LinearLayout) inflater.inflate(R.layout.kanji_detail, null);
            TextView textView = (TextView) layout.findViewById(R.id.detailList);
            textView.setText(reading);
            out.add(layout);

        } catch (Exception e){
            e.printStackTrace();
        }
        return out;
    }

    public static List<LinearLayout> meaningToLayout(String in, LayoutInflater inflater){
        List<LinearLayout> out = new ArrayList<LinearLayout>();
        try{
            JSONArray reader = new JSONArray(in);
            SpannableStringBuilder meaning = new SpannableStringBuilder();
            final int count = reader.length();
            float baseValue = 1;
            for (int i = 0; i<count; i++){
                if (i>0) meaning.append(MEANING_SEP);
                JSONObject each = reader.getJSONObject(i);
                int len = each.getString("txt").length();


                if (i==0) baseValue = each.getLong("freq");
                float freq = (float)( each.getDouble("freq") / baseValue);
                SpannableString string = new SpannableString(each.getString("txt"));
                if (freq  >= 0.9f) string.setSpan(new StyleSpan(Typeface.BOLD),0,len, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                if (freq  >= 0 && freq < 0.5f) string.setSpan(new RelativeSizeSpan(0.8f),0,len, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                if (freq == 0) string.setSpan(new ForegroundColorSpan(Color.LTGRAY),0, len, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                meaning.append(string);
            }
            LinearLayout layout = (LinearLayout) inflater.inflate(R.layout.kanji_detail, null);
            TextView textView = (TextView) layout.findViewById(R.id.detailList);
            textView.setText(meaning);
            out.add(layout);

        } catch (Exception e){
            e.printStackTrace();
        }
        return out;
    }

    public static List<KanjiPath> svgToKanjiPath(String in){
        List<KanjiPath> kanjiPaths = new ArrayList<KanjiPath>(0);
        try{
            JSONArray reader = new JSONArray(in);
            final int count = reader.length();

            for (int i = 0; i < count; i++){
                JSONArray group = reader.getJSONArray(i);
                int glen = group.length();
                for (int q = 0; q < glen; q ++){
                    JSONObject path = group.getJSONObject(q);
                    Path svgPath = SVGParser.parsePath(path.getString("d"));
                    Paint paint = new Paint();
                    paint.setColor(Color.parseColor(path.getString("color")));
                    paint.setAntiAlias(true);
                    paint.setDither(false);
                    paint.setStrokeWidth(5);
                    paint.setStyle(Paint.Style.STROKE);
                    paint.setStrokeCap(Paint.Cap.ROUND);
                    KanjiPath kanjiPath = new KanjiPath(svgPath, paint);

                    kanjiPaths.add(kanjiPath);
                }
            }
        } catch (Exception e){
            e.printStackTrace();
        }

        return kanjiPaths;




    }
    private static void setKanjiReplacement(String kanji, SpannableStringBuilder span, String replacement){
        //SpannableStringBuilder replacementBuilder = new SpannableStringBuilder(span);

        int index = span.toString().indexOf(kanji);
        while (index > -1) {
            span.replace(index, index + kanji.length(), replacement);
            index = span.toString().indexOf(kanji);
        }
        // return replacementBuilder;
    }
    public static ArrayList<KanjiHelper.ExampleWordData> makeExampleWordData(String in, final String kanjiString, final String replacement){
        KanjiHelper.ExampleWordData wordData = null;
        ArrayList<KanjiHelper.ExampleWordData> wordDataList = new ArrayList<KanjiHelper.ExampleWordData>();
        LayoutTag tag;
        String workingString;
        SpannableString workingSpan;
        SpannableStringBuilder workingBuilder;// = new SpannableStringBuilder();

        try {

            JSONArray exampleArray = new JSONArray(in);
            final int exampleLength = exampleArray.length();
            for (int i = 0; i < exampleLength; i ++) {
                final JSONObject example = exampleArray.getJSONObject(i);
                wordData = new KanjiHelper.ExampleWordData();
                switch(example.getInt("rt")){
                    case 1:
                        wordData.containerTag = new LayoutTag(LayoutTag.Type.RDNG_ON);
                        break;
                    case 2:
                        wordData.containerTag = new LayoutTag(LayoutTag.Type.RDNG_KUN);
                        break;
                    case 3:
                    default:
                        continue;

                }

                wordDataList.add(wordData);
                int len;
                workingBuilder = new SpannableStringBuilder();
                //region KANJI parsing
                JSONArray kanjiArray = example.getJSONArray("k");
                len = kanjiArray.length();
                for (int q = 0; q < len; q++) {
                    workingString = "";
                    if (q > 0) workingString += RDNG_SEP;

                    JSONObject kanji = kanjiArray.getJSONObject(q);
                    workingString += kanji.getString("txt");

                    workingSpan = new SpannableString(workingString);
                    workingSpan.setSpan(new StyleSpan(Typeface.BOLD), 0, workingString.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                    if (q > 0)
                        workingSpan.setSpan(new RelativeSizeSpan(0.9f), 0, workingString.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                    workingBuilder.append(workingSpan);
                }

                //tag = new LayoutTag(new LayoutTag.Type[]{LayoutTag.Type.KANJI}, workingBuilder);
                wordData.kanji = workingBuilder;
                wordData.kanjiTag = new LayoutTag(new LayoutTag.Helper() {
                    @Override
                    public void toggleView(View v, LayoutTag.Type... types) {
                        final int length = types.length;
                        final SpannedString spannedString = (SpannedString) ((TextView) v).getText();
                        SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder(spannedString);
                        for (int i = 0; i < length; i++) {
                            if (types[i] == LayoutTag.Type.KANJI){
                                setKanjiReplacement(kanjiString, spannableStringBuilder, replacement);
                                break;
                            }
                        }
                        ((TextView) v).setText(spannableStringBuilder);
                    }

                    @Override
                    public void unToggleView(View v, LayoutTag.Type... types) {
                        final int length = types.length;
                        final SpannedString spannedString = (SpannedString) ((TextView) v).getText();
                        SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder(spannedString);
                        for (int i = 0; i < length; i++) {
                            if (types[i] == LayoutTag.Type.KANJI){
                                setKanjiReplacement( replacement, spannableStringBuilder,kanjiString);
                                break;
                            }
                        }
                        ((TextView) v).setText(spannableStringBuilder);
                    }
                }, LayoutTag.Type.KANJI);



                JSONArray rdngArray = example.getJSONArray("r");
                len = rdngArray.length();

                workingString = "";
                if (len > 0) workingString += "(";
                for (int q = 0; q < len; q++){
                    if (q>0) workingString += RDNG_SEP;
                    JSONObject rdng = rdngArray.getJSONObject(q);
                    workingString += rdng.getString("txt");
                }
                if (len > 0) workingString += ")";
                wordData.readingTag = new LayoutTag(LayoutTag.Type.RDNG_ALL);
                workingSpan = new SpannableString(workingString);
                workingSpan.setSpan(new RelativeSizeSpan(0.8f), 0, workingString.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

                wordData.reading = workingSpan;
                //endregion


                //region MEANINGS
                int codePoint = codePointStart;
                //if (len > 0) workingString += ": ";
                final JSONArray senseArray = example.getJSONArray("s");
                len = senseArray.length();
                workingString = "";
                for (int q = 0; q < len; q++){
                    JSONObject sense = senseArray.getJSONObject(q);

                    /*
                     * Set sense to front of the string
                     */

                    //TODO Replace codePoint with a specialized drawable span.
                    if (codePoint <= codePointEnd){
                        workingString += Character.toString((char)codePoint);
                        codePoint++;
                    }
                    else{
                        workingString += "(" + Integer.toString(q) + ")";
                    }

                    /*
                     * Get Part of Speech
                     */
                    JSONArray posArray = sense.getJSONArray("p");
                    if (posArray.length() > 0) workingString += "(";
                    for (int r = 0; r < posArray.length(); r++){
                        if (r>0) workingString += MEANING_SEP;
                        workingString += posArray.getString(r);
                    }
                    if (posArray.length() > 0) workingString += ") ";

                    /*
                     * Get any kanji/readings specific to sense
                     */
                    //TODO Add Span to change color of kanji/reading specific tags - remove "only" marker
                    JSONArray krArray = sense.getJSONArray("kr");
                    if (krArray.length() > 0 ) workingString += "<";
                    for (int r = 0; r < krArray.length(); r++){
                        if (r>0) workingString += RDNG_SEP;
                        workingString += krArray.getString(r);
                    }
                    if (krArray.length() >0) workingString += " only> ";

                    /*
                     * Get gloss
                     */

                    JSONArray glossArray = sense.getJSONArray("g");
                    for (int r = 0; r < glossArray.length(); r++){
                        if (r>0) workingString += RDNG_SEP;
                        workingString += glossArray.getString(r);
                    }
                }

                wordData.meaningTag = new LayoutTag(LayoutTag.Type.MEANING);
                workingSpan = new SpannableString(workingString);
                workingSpan.setSpan(new RelativeSizeSpan(0.8f), 0, workingString.length(),Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                wordData.meaning = workingSpan;

                //endregion



                //region SENTENCES
                JSONArray sentArray = example.getJSONArray("snt");
                len = sentArray.length();

                for (int q = 0; q < len; q++){

                    JSONObject sentence = sentArray.getJSONObject(q);
                    JSONArray words = sentence.getJSONArray("wl");
                    final int w_len = words.length();
                    String sentText = sentence.getString("sentJ");
                    SpannableString sentSpan = new SpannableString(sentence.getString("sentJ"));
                    SpannableString rdngReplacement = new SpannableString(sentence.getString("sentJ"));

                    //TODO always check for word in
                    for (int r = 0; r < w_len; r++){
                        JSONObject furi = words.getJSONObject(r);
                        final int start = sentText.indexOf(furi.getString("word"),0);
                        final int end = start + furi.getString("word").length();

                        FuriganaSpan furiSpan = new FuriganaSpan(furi.getString("furi"), false);
                        sentSpan.setSpan(furiSpan,start,end, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

                        if (furi.getString("word").contains(kanjiString)){
                            furiSpan.setReplacement(replacement.charAt(0));
                            /*
                            final int furiLength =  furi.getString("furi").length();
                            String maruString = "";
                            for (int p = 0; p < furiLength; p++){
                                maruString += context.getResources().getString(R.string.kanji_replacement);
                            }
                            furiSpan = new FuriganaSpan(maruString,false);*///
                        }
                        rdngReplacement.setSpan(furiSpan,start,end, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                    }

                    workingSpan = new SpannableString(sentSpan);
                    workingSpan.setSpan(new RelativeSizeSpan(1.1f),0,workingSpan.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                    wordData.sentJ = workingSpan;
                    wordData.sentJTag = new LayoutTag(new LayoutTag.Helper() {
                        boolean oldVisibility;
                        @Override
                        public void toggleView(View v, LayoutTag.Type... types) {
                            final int length = types.length;
                            final SpannedString spannedString = (SpannedString) ((TextView) v).getText();
                            final FuriganaSpan[] furiganaSpans = spannedString.getSpans(0, spannedString.length(), FuriganaSpan.class);
                            SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder(spannedString);
                            final int furiCount = furiganaSpans.length;
                            for (int i = 0; i < length; i++) {
                                switch (types[i]) {
                                    case KANJI:
                                        setKanjiReplacement(kanjiString, spannableStringBuilder, replacement);
                                        for (int q = 0; q < furiCount; q++) {
                                            if(furiganaSpans[q].getReplacement() != '\u0000') //null
                                            {
                                                oldVisibility = furiganaSpans[q].getVisibility();
                                                furiganaSpans[q].setAlwaysVisible(true);
                                            }
                                        }
                                        break;
                                    case RDNG_ALL:
                                        for (int q = 0; q < furiCount; q++) {
                                            if(furiganaSpans[q].getReplacement() != '\u0000') //null
                                                furiganaSpans[q].replaceFurigana();
                                        }
                                        break;
                                }
                            }
                            ((TextView) v).setText(spannableStringBuilder);
                        }

                        @Override
                        public void unToggleView(View v, LayoutTag.Type... types) {
                            final int length = types.length;

                            final SpannedString spannedString = (SpannedString) ((TextView) v).getText();
                            final FuriganaSpan[] furiganaSpans = spannedString.getSpans(0, spannedString.length(), FuriganaSpan.class);
                            final int furiCount = furiganaSpans.length;
                            SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder(spannedString);

                            for (int i = 0; i < length; i++) {
                                switch (types[i]) {
                                    case KANJI:
                                        setKanjiReplacement(replacement, spannableStringBuilder,kanjiString);
                                        for (int q =0 ; q < furiCount; q++){
                                            if(furiganaSpans[q].getReplacement() == '\u0000') {
                                                oldVisibility = furiganaSpans[q].getVisibility();
                                                break;
                                            }

                                        }
                                        for (int q = 0; q < furiCount; q++) {
                                            if(furiganaSpans[q].getReplacement() != '\u0000') //null
                                            {
                                                furiganaSpans[q].setAlwaysVisible(false);
                                                furiganaSpans[q].setVisibility(oldVisibility);
                                            }

                                        }
                                        break;
                                    case RDNG_ALL:
                                        for (int q = 0; q < furiCount; q++) {
                                            if(furiganaSpans[q].getReplacement() != '\u0000') //null
                                                furiganaSpans[q].restoreFurigana();
                                        }
                                        break;
                                }
                            }
                            ((TextView) v).setText(spannableStringBuilder);
                        }
                    }, LayoutTag.Type.RDNG_ALL, LayoutTag.Type.KANJI);
                    //tag = new LayoutTag(new LayoutTag.Type[]{LayoutTag.Type.RDNG_ALL, LayoutTag.Type.KANJI},sentSpan);
                    //tag.addReplacementString(LayoutTag.Type.RDNG_ALL,rdngReplacement);
                    //tag.addReplacementString(LayoutTag.Type.KANJI,getKanjiReplacement(kanjiString, sentSpan, context.getResources().getString(R.string.kanji_replacement)));



                    workingSpan = new SpannableString(sentence.getString("sentE"));
                    workingSpan.setSpan(new RelativeSizeSpan(0.8f), 0, workingSpan.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                    wordData.sentE = workingSpan;
                    wordData.sentETag = new LayoutTag(LayoutTag.Type.MEANING);

                }
                //endregion


            }
        } catch(Exception e){
            final String string = e.getMessage() != null ? e.getMessage() : "";
            Log.e(e.getClass().getName(), string, e);
            //e.printStackTrace();
        }
        return wordDataList;
    }
    private static void exampleDetails(String in, String kanjiString, ViewGroup parent){
        final Context context = parent.getContext();
        final LayoutInflater inflater = LayoutInflater.from(context);
        final String kanjiFinal = kanjiString;

        String workingString;
        SpannableString workingSpan;
        SpannableStringBuilder workingBuilder;// = new SpannableStringBuilder();


        final int containerID = context.getResources().getInteger(R.integer.example_entry);
        LayoutTag tag;
        ImageView detailTab;

        try {
            JSONArray exampleArray = new JSONArray(in);
            final int exampleLength = exampleArray.length();
            for (int i = 0; i < exampleLength; i ++) {
                final JSONObject example = exampleArray.getJSONObject(i);

                switch(example.getInt("rt")){
                    case 1:
                        tag = new LayoutTag(LayoutTag.Type.RDNG_ON);
                        break;
                    case 2:
                        tag = new LayoutTag(LayoutTag.Type.RDNG_KUN);
                        break;
                    case 3:
                    default:
                        continue;

                }

                LinearLayout entryLayout = (LinearLayout) inflater.inflate(R.layout.kanji_detailcontainer, new LinearLayout(context), false);
                entryLayout.setId(containerID+i);


                entryLayout.setTag(tag);
                parent.addView(entryLayout);
                int len;
                workingBuilder = new SpannableStringBuilder();
                //region KANJI parsing
                JSONArray kanjiArray = example.getJSONArray("k");
                len = kanjiArray.length();
                for (int q = 0; q < len; q++) {
                    workingString = "";
                    if (q > 0) workingString += RDNG_SEP;

                    JSONObject kanji = kanjiArray.getJSONObject(q);
                    workingString += kanji.getString("txt");

                    workingSpan = new SpannableString(workingString);
                    workingSpan.setSpan(new StyleSpan(Typeface.BOLD), 0, workingString.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                    if (q > 0)
                        workingSpan.setSpan(new RelativeSizeSpan(0.9f), 0, workingString.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                    workingBuilder.append(workingSpan);
                }
                LinearLayout detail = (LinearLayout) inflater.inflate(R.layout.kanji_detail, new LinearLayout(context), false);
                //tag = new LayoutTag(new LayoutTag.Type[]{LayoutTag.Type.KANJI}, workingBuilder);
                tag = new LayoutTag(new LayoutTag.Helper() {
                    @Override
                    public void toggleView(View v, LayoutTag.Type... types) {
                        final int length = types.length;
                        final SpannedString spannedString = (SpannedString) ((TextView) v).getText();
                        SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder(spannedString);
                        for (int i = 0; i < length; i++) {
                            if (types[i] == LayoutTag.Type.KANJI){
                                setKanjiReplacement(kanjiFinal, spannableStringBuilder, context.getResources().getString(R.string.kanji_replacement));
                                break;
                            }
                        }
                        ((TextView) v).setText(spannableStringBuilder);
                    }

                    @Override
                    public void unToggleView(View v, LayoutTag.Type... types) {
                        final int length = types.length;
                        final SpannedString spannedString = (SpannedString) ((TextView) v).getText();
                        SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder(spannedString);
                        for (int i = 0; i < length; i++) {
                            if (types[i] == LayoutTag.Type.KANJI){
                                setKanjiReplacement( context.getResources().getString(R.string.kanji_replacement), spannableStringBuilder,kanjiFinal);
                                break;
                            }
                        }
                        ((TextView) v).setText(spannableStringBuilder);
                    }
                }, LayoutTag.Type.KANJI);
                //tag.addReplacementString(LayoutTag.Type.KANJI, getKanjiReplacement(kanjiString, workingBuilder, context.getResources().getString(R.string.kanji_replacement)));

                detail.setTag(tag);
                detail.setId(context.getResources().getInteger(R.integer.example_kanji));

                TextView detailText = (TextView) detail.findViewById(R.id.detailList);
                detailText.setText(workingBuilder);
                entryLayout.addView(detail);
                //endregion


                //region READINGS



                JSONArray rdngArray = example.getJSONArray("r");
                len = rdngArray.length();

                workingString = "";
                if (len > 0) workingString += "(";
                for (int q = 0; q < len; q++){
                    if (q>0) workingString += RDNG_SEP;
                    JSONObject rdng = rdngArray.getJSONObject(q);
                    workingString += rdng.getString("txt");
                }
                if (len > 0) workingString += ")";
                tag = new LayoutTag(LayoutTag.Type.RDNG_ALL);
                detail = (LinearLayout) inflater.inflate(R.layout.kanji_detail, new LinearLayout(context),false);
                detail.setTag(tag);
                detail.setId(context.getResources().getInteger(R.integer.example_rdng));
                detailTab = (ImageView) detail.findViewById(R.id.exampleTab);
                detailTab.setVisibility(View.INVISIBLE);
                detailText = (TextView) detail.findViewById(R.id.detailList);
                workingSpan = new SpannableString(workingString);
                workingSpan.setSpan(new RelativeSizeSpan(0.8f), 0, workingString.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                detailText.setText(workingString);
                entryLayout.addView(detail);
                //endregion


                //region MEANINGS
                int codePoint = codePointStart;
                //if (len > 0) workingString += ": ";
                final JSONArray senseArray = example.getJSONArray("s");
                len = senseArray.length();
                workingString = "";
                for (int q = 0; q < len; q++){
                    JSONObject sense = senseArray.getJSONObject(q);

                    /*
                     * Set sense to front of the string
                     */

                    //TODO Replace codePoint with a specialized drawable span.
                    if (codePoint <= codePointEnd){
                        workingString += Character.toString((char)codePoint);
                        codePoint++;
                    }
                    else{
                        workingString += "(" + Integer.toString(q) + ")";
                    }

                    /*
                     * Get Part of Speech
                     */
                    JSONArray posArray = sense.getJSONArray("p");
                    if (posArray.length() > 0) workingString += "(";
                    for (int r = 0; r < posArray.length(); r++){
                        if (r>0) workingString += MEANING_SEP;
                        workingString += posArray.getString(r);
                    }
                    if (posArray.length() > 0) workingString += ") ";

                    /*
                     * Get any kanji/readings specific to sense
                     */
                    //TODO Add Span to change color of kanji/reading specific tags - remove "only" marker
                    JSONArray krArray = sense.getJSONArray("kr");
                    if (krArray.length() > 0 ) workingString += "<";
                    for (int r = 0; r < krArray.length(); r++){
                        if (r>0) workingString += RDNG_SEP;
                        workingString += krArray.getString(r);
                    }
                    if (krArray.length() >0) workingString += " only> ";

                    /*
                     * Get gloss
                     */

                    JSONArray glossArray = sense.getJSONArray("g");
                    for (int r = 0; r < glossArray.length(); r++){
                        if (r>0) workingString += RDNG_SEP;
                        workingString += glossArray.getString(r);
                    }
                }

                detail = (LinearLayout) inflater.inflate(R.layout.kanji_detail, null);
                tag = new LayoutTag(LayoutTag.Type.MEANING);
                detail.setTag(tag);
                detail.setId(context.getResources().getInteger(R.integer.example_meaning));
                detailTab = (ImageView) detail.findViewById(R.id.exampleTab);
                detailTab.setVisibility(View.INVISIBLE);
                detailText = (TextView) detail.findViewById(R.id.detailList);
                workingSpan = new SpannableString(workingString);
                workingSpan.setSpan(new RelativeSizeSpan(0.8f), 0, workingString.length(),Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                detailText.setText(workingSpan);
                entryLayout.addView(detail);
                //endregion



                //region SENTENCES
                JSONArray sentArray = example.getJSONArray("snt");
                len = sentArray.length();

                for (int q = 0; q < len; q++){

                    JSONObject sentence = sentArray.getJSONObject(q);
                    JSONArray words = sentence.getJSONArray("wl");
                    final int w_len = words.length();
                    String sentText = sentence.getString("sentJ");
                    SpannableString sentSpan = new SpannableString(sentence.getString("sentJ"));
                    SpannableString rdngReplacement = new SpannableString(sentence.getString("sentJ"));

                    //TODO always check for word in
                    for (int r = 0; r < w_len; r++){
                        JSONObject furi = words.getJSONObject(r);
                        final int start = sentText.indexOf(furi.getString("word"),0);
                        final int end = start + furi.getString("word").length();

                        FuriganaSpan furiSpan = new FuriganaSpan(furi.getString("furi"), false);
                        sentSpan.setSpan(furiSpan,start,end, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

                        if (furi.getString("word").contains(kanjiString)){
                            furiSpan.setReplacement(context.getResources().getString(R.string.kanji_replacement).charAt(0));
                            /*
                            final int furiLength =  furi.getString("furi").length();
                            String maruString = "";
                            for (int p = 0; p < furiLength; p++){
                                maruString += context.getResources().getString(R.string.kanji_replacement);
                            }
                            furiSpan = new FuriganaSpan(maruString,false);*///
                        }
                        rdngReplacement.setSpan(furiSpan,start,end, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                    }
                    detail = sentenceDetail(sentSpan, context, true);

                    tag = new LayoutTag(new LayoutTag.Helper() {
                        boolean oldVisibility;
                        @Override
                        public void toggleView(View v, LayoutTag.Type... types) {
                            final int length = types.length;
                            final SpannedString spannedString = (SpannedString) ((TextView) v).getText();
                            final FuriganaSpan[] furiganaSpans = spannedString.getSpans(0, spannedString.length(), FuriganaSpan.class);
                            SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder(spannedString);
                            final int furiCount = furiganaSpans.length;
                            for (int i = 0; i < length; i++) {
                                switch (types[i]) {
                                    case KANJI:
                                        setKanjiReplacement(kanjiFinal, spannableStringBuilder, context.getResources().getString(R.string.kanji_replacement));
                                        for (int q = 0; q < furiCount; q++) {
                                            if(furiganaSpans[q].getReplacement() != '\u0000') //null
                                            {
                                                oldVisibility = furiganaSpans[q].getVisibility();
                                                furiganaSpans[q].setAlwaysVisible(true);
                                            }
                                        }
                                        break;
                                    case RDNG_ALL:
                                        for (int q = 0; q < furiCount; q++) {
                                            if(furiganaSpans[q].getReplacement() != '\u0000') //null
                                                furiganaSpans[q].replaceFurigana();
                                        }
                                        break;
                                }
                            }
                            ((TextView) v).setText(spannableStringBuilder);
                        }

                        @Override
                        public void unToggleView(View v, LayoutTag.Type... types) {
                            final int length = types.length;

                            final SpannedString spannedString = (SpannedString) ((TextView) v).getText();
                            final FuriganaSpan[] furiganaSpans = spannedString.getSpans(0, spannedString.length(), FuriganaSpan.class);
                            final int furiCount = furiganaSpans.length;
                                    SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder(spannedString);

                            for (int i = 0; i < length; i++) {
                                switch (types[i]) {
                                    case KANJI:
                                        setKanjiReplacement(context.getResources().getString(R.string.kanji_replacement), spannableStringBuilder,kanjiFinal);
                                        for (int q =0 ; q < furiCount; q++){
                                            if(furiganaSpans[q].getReplacement() == '\u0000') {
                                                oldVisibility = furiganaSpans[q].getVisibility();
                                                break;
                                            }

                                        }
                                        for (int q = 0; q < furiCount; q++) {
                                            if(furiganaSpans[q].getReplacement() != '\u0000') //null
                                            {
                                                furiganaSpans[q].setAlwaysVisible(false);
                                                furiganaSpans[q].setVisibility(oldVisibility);
                                            }

                                        }
                                        break;
                                    case RDNG_ALL:
                                        for (int q = 0; q < furiCount; q++) {
                                            if(furiganaSpans[q].getReplacement() != '\u0000') //null
                                                furiganaSpans[q].restoreFurigana();
                                        }
                                        break;
                                }
                            }
                            ((TextView) v).setText(spannableStringBuilder);
                        }
                    }, LayoutTag.Type.RDNG_ALL, LayoutTag.Type.KANJI);
                    //tag = new LayoutTag(new LayoutTag.Type[]{LayoutTag.Type.RDNG_ALL, LayoutTag.Type.KANJI},sentSpan);
                    //tag.addReplacementString(LayoutTag.Type.RDNG_ALL,rdngReplacement);
                    //tag.addReplacementString(LayoutTag.Type.KANJI,getKanjiReplacement(kanjiString, sentSpan, context.getResources().getString(R.string.kanji_replacement)));

                    detail.setTag(tag);
                    detail.setId(context.getResources().getInteger(R.integer.example_sent_j));

                    entryLayout.addView(detail);


                    workingSpan = new SpannableString(sentence.getString("sentE"));
                    workingSpan.setSpan(new RelativeSizeSpan(0.8f), 0, workingSpan.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                    detail = sentenceDetail(workingSpan, context, false);
                    tag = new LayoutTag(LayoutTag.Type.MEANING);
                    detail.setTag(tag);
                    detail.setId(context.getResources().getInteger(R.integer.example_sent_e));
                    entryLayout.addView(detail);
                }
                //endregion


            }
        } catch(Exception e){
            final String string = e.getMessage() != null ? e.getMessage() : "";
            Log.e(e.getClass().getName(), string, e);
            //e.printStackTrace();
        }
    }

    public static ArrayList<KanjiHelper.ExampleNameData> makeExampleNameData(String in, final String kanjiString, final String replacement){
        KanjiHelper.ExampleNameData exampleNameData;
        ArrayList<KanjiHelper.ExampleNameData> nameData = new ArrayList<KanjiHelper.ExampleNameData>();
        LayoutTag tag;
        String workingString;
        SpannableString workingSpan;
        SpannableStringBuilder workingBuilder;// = new SpannableStringBuilder();

        try {
            JSONArray exampleArray = new JSONArray(in);
            final int exampleLength = exampleArray.length();
            for (int i = 0; i < exampleLength; i ++){
                JSONObject example = exampleArray.getJSONObject(i);
                switch(example.getInt("rt")){
                    case RDNG_NANORI:
                        break;
                    case RDNG_ON:
                    case RDNG_KUN:
                    default:
                        continue;
                }
                exampleNameData = new KanjiHelper.ExampleNameData();
                nameData.add(exampleNameData);
                int len;

                //region KANJI parsing
                JSONArray kanjiArray = example.getJSONArray("k");
                len = kanjiArray.length();
                workingBuilder = new SpannableStringBuilder();
                for (int q = 0; q < len; q++) {
                    workingString = "";
                    if (q > 0)  break;

                    JSONObject kanji = kanjiArray.getJSONObject(q);
                    workingString += kanji.getString("txt");
                    workingSpan = new SpannableString(workingString);
                    workingSpan.setSpan(new StyleSpan(Typeface.BOLD), 0, workingString.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                    workingBuilder.append(workingSpan);
                }
                //SpannableStringBuilder replacementBuilder = getKanjiReplacement(kanjiString, workingBuilder, context.getResources().getString(R.string.kanji_replacement));

                JSONArray rdngArray = example.getJSONArray("r");
                len = rdngArray.length();
                workingString = "";
                for (int q = 0; q < len; q++) {
                    if (q > 0)  break;
                    JSONObject rdng = rdngArray.getJSONObject(q);
                    workingString += rdng.getString("txt");
                }
                //SpannableStringBuilder replacementReading = new SpannableStringBuilder(workingBuilder);
                workingBuilder.setSpan(new FuriganaSpan(workingString, true, false), 0, workingBuilder.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);




                exampleNameData.kanji = workingBuilder;
                exampleNameData.tag = new LayoutTag(new LayoutTag.Helper() {
                    @Override
                    public void toggleView(View v, LayoutTag.Type... types) {
                        final int length = types.length;
                        final SpannedString spannedString = (SpannedString) ((TextView) v).getText();
                        final FuriganaSpan[] furiganaSpans = spannedString.getSpans(0, spannedString.length(), FuriganaSpan.class);
                        SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder(spannedString);
                        for (int i = 0; i < length; i++) {
                            switch (types[i]) {
                                case KANJI:
                                    setKanjiReplacement(kanjiString, spannableStringBuilder, replacement);
                                    break;
                                case RDNG_ALL:
                                    for (int q = 0; q < furiganaSpans.length; q++) {
                                        furiganaSpans[q].setVisibility(false);
                                    }
                                    break;
                            }
                        }
                        ((TextView) v).setText(spannableStringBuilder);
                    }

                    @Override
                    public void unToggleView(View v, LayoutTag.Type... types) {
                        final int length = types.length;
                        final SpannedString spannedString = (SpannedString) ((TextView) v).getText();
                        final FuriganaSpan[] furiganaSpans = spannedString.getSpans(0, spannedString.length(), FuriganaSpan.class);

                        SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder(spannedString);

                        for (int i = 0; i < length; i++) {
                            switch (types[i]) {
                                case KANJI:
                                    setKanjiReplacement(replacement, spannableStringBuilder, kanjiString);
                                    break;
                                case RDNG_ALL:
                                    for (int q = 0; q < furiganaSpans.length; q++) {
                                        furiganaSpans[q].setVisibility(true);
                                    }
                                    break;
                            }
                        }
                        ((TextView) v).setText(spannableStringBuilder);

                    }
                }, LayoutTag.Type.KANJI, LayoutTag.Type.RDNG_ALL);

                JSONArray senseArray = example.getJSONArray("s");
                len = senseArray.length();
                workingBuilder = new SpannableStringBuilder();
                workingBuilder.append(" ");

                for (int q = 0; q<len; q++){
                    if (q > 0) break;
                    JSONObject sense = senseArray.getJSONObject(q);
                    JSONArray posArray = sense.getJSONArray("p");
                    for (int r = 0; r < posArray.length(); r++){
                        if (r>0) break;
                        workingBuilder.append(getNameType(posArray.getString(r)));
                    }
                }
                workingBuilder.setSpan(new ForegroundColorSpan(Color.LTGRAY),0,workingBuilder.length(),Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                workingBuilder.setSpan(new RelativeSizeSpan(0.8f),0,workingBuilder.length(),Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                exampleNameData.type = workingBuilder;

            }

        } catch(Exception e){
            final String string = e.getMessage() != null ? e.getMessage() : "";
            Log.e(e.getClass().getName(), string, e);
            //e.printStackTrace();
        }
        return nameData;
    }
    private static void exampleNames(String in, String kanjiString, ViewGroup parent){
        final Context context = parent.getContext();
        final LayoutInflater inflater = LayoutInflater.from(context);
        LayoutTag tag;

        GridLayout nameLayout = (GridLayout) inflater.inflate(R.layout.name_layout, new GridLayout(context), false);
        tag = new LayoutTag(LayoutTag.Type.RDNG_NANORI);
        nameLayout.setTag(tag);
        boolean isAddedGrid = false;
        String workingString;
        SpannableString workingSpan;
        SpannableStringBuilder workingBuilder;// = new SpannableStringBuilder();
        int containerID = context.getResources().getInteger(R.integer.example_entry);
        try {
            JSONArray exampleArray = new JSONArray(in);
            final int exampleLength = exampleArray.length();
            for (int i = 0; i < exampleLength; i ++){
                JSONObject example = exampleArray.getJSONObject(i);
                switch(example.getInt("rt")){
                    case RDNG_NANORI:
                        break;
                    case RDNG_ON:
                    case RDNG_KUN:
                    default:
                        continue;
                }
                if (!isAddedGrid) {
                    parent.addView(nameLayout);
                    isAddedGrid = true;
                }
                LinearLayout entryLayout = (LinearLayout) inflater.inflate(R.layout.kanji_detailcontainer, new LinearLayout(context), false);
                entryLayout.setId(containerID+i);
                nameLayout.addView(entryLayout);

                int len;

                //region KANJI parsing
                JSONArray kanjiArray = example.getJSONArray("k");
                len = kanjiArray.length();
                workingBuilder = new SpannableStringBuilder();
                for (int q = 0; q < len; q++) {
                    workingString = "";
                    if (q > 0)  break;

                    JSONObject kanji = kanjiArray.getJSONObject(q);
                    workingString += kanji.getString("txt");
                    workingSpan = new SpannableString(workingString);
                    workingSpan.setSpan(new StyleSpan(Typeface.BOLD), 0, workingString.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                    workingBuilder.append(workingSpan);
                }
                //SpannableStringBuilder replacementBuilder = getKanjiReplacement(kanjiString, workingBuilder, context.getResources().getString(R.string.kanji_replacement));

                JSONArray rdngArray = example.getJSONArray("r");
                len = rdngArray.length();
                workingString = "";
                for (int q = 0; q < len; q++) {
                    if (q > 0)  break;
                    JSONObject rdng = rdngArray.getJSONObject(q);
                    workingString += rdng.getString("txt");
                }
                //SpannableStringBuilder replacementReading = new SpannableStringBuilder(workingBuilder);
                workingBuilder.setSpan(new FuriganaSpan(workingString, true, false), 0, workingBuilder.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

                final String kanjiFinal = kanjiString;


                LinearLayout detail = (LinearLayout) inflater.inflate(R.layout.kanji_detail, new LinearLayout(context), false);
                tag = new LayoutTag(new LayoutTag.Helper() {
                    @Override
                    public void toggleView(View v, LayoutTag.Type... types) {
                        final int length = types.length;
                        final SpannedString spannedString = (SpannedString) ((TextView) v).getText();
                        final FuriganaSpan[] furiganaSpans = spannedString.getSpans(0, spannedString.length(), FuriganaSpan.class);
                        SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder(spannedString);
                        for (int i = 0; i < length; i++) {
                            switch (types[i]) {
                                case KANJI:
                                    setKanjiReplacement(kanjiFinal, spannableStringBuilder, context.getResources().getString(R.string.kanji_replacement));
                                    break;
                                case RDNG_ALL:
                                    for (int q = 0; q < furiganaSpans.length; q++) {
                                        furiganaSpans[q].setVisibility(false);
                                    }
                                    break;
                            }
                        }
                        ((TextView) v).setText(spannableStringBuilder);
                    }

                    @Override
                    public void unToggleView(View v, LayoutTag.Type... types) {
                        final int length = types.length;
                        final SpannedString spannedString = (SpannedString) ((TextView) v).getText();
                        final FuriganaSpan[] furiganaSpans = spannedString.getSpans(0, spannedString.length(), FuriganaSpan.class);

                        SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder(spannedString);

                        for (int i = 0; i < length; i++) {
                            switch (types[i]) {
                                case KANJI:
                                    setKanjiReplacement(context.getResources().getString(R.string.kanji_replacement), spannableStringBuilder, kanjiFinal);
                                    break;
                                case RDNG_ALL:
                                    for (int q = 0; q < furiganaSpans.length; q++) {
                                        furiganaSpans[q].setVisibility(true);
                                    }
                                    break;
                            }
                        }
                        ((TextView) v).setText(spannableStringBuilder);

                    }
                }, LayoutTag.Type.KANJI, LayoutTag.Type.RDNG_ALL);
                //tag = new LayoutTag(new LayoutTag.Type[]{LayoutTag.Type.KANJI, LayoutTag.Type.RDNG_ALL}, workingBuilder);
                //tag.addReplacementString(LayoutTag.Type.KANJI, replacementBuilder);
                //tag.addReplacementString(LayoutTag.Type.RDNG_ALL, replacementReading);

                detail.setTag(tag);
                detail.setId(context.getResources().getInteger(R.integer.example_kanji));
                TextView detailText = (TextView) detail.findViewById(R.id.detailList);
                detailText.setText(workingBuilder);
                entryLayout.addView(detail);

                JSONArray senseArray = example.getJSONArray("s");
                len = senseArray.length();
                //workingSpan = new SpannableString("NAME ");
                //workingSpan.setSpan(new ForegroundColorSpan(Color.LTGRAY),0,workingSpan.length(),Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                workingBuilder = new SpannableStringBuilder();
                workingBuilder.append(" ");
                //workingBuilder.append(workingSpan);

                for (int q = 0; q<len; q++){
                    if (q > 0) break;
                    JSONObject sense = senseArray.getJSONObject(q);
                    JSONArray posArray = sense.getJSONArray("p");
                    for (int r = 0; r < posArray.length(); r++){
                        if (r>0) break;
                        workingBuilder.append(getNameType(posArray.getString(r)));
                    }
                }
                workingBuilder.setSpan(new ForegroundColorSpan(Color.LTGRAY),0,workingBuilder.length(),Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                workingBuilder.setSpan(new RelativeSizeSpan(0.8f),0,workingBuilder.length(),Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

                //SpannableString meaningSpan = new SpannableString(meaningStr);
                //meaningSpan.setSpan(new RelativeSizeSpan(0.8f), 0, meaningStr.length(),Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                //exampleStr.append(meaningSpan);
                //exampleStr.setSpan(new LeadingMarginSpan.Standard(0,15),0,exampleStr.length(),Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                detail = (LinearLayout) inflater.inflate(R.layout.kanji_detail, new LinearLayout(context),false);
                detailText = (TextView) detail.findViewById(R.id.detailList);
                detailText.setText(workingBuilder);
                entryLayout.addView(detail);

            }

        } catch(Exception e){
            final String string = e.getMessage() != null ? e.getMessage() : "";
            Log.e(e.getClass().getName(), string, e);
            //e.printStackTrace();
        }

    }

    public static void examplesToLayout(String in, String kanjiString, ViewGroup parent){
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);
        int containerID = context.getResources().getInteger(R.integer.example);
        String title = context.getResources().getString(R.string.example);
        LayoutTag tag = new LayoutTag(LayoutTag.Type.EXAMPLE);

        LinearLayout container = (LinearLayout) inflater.inflate(R.layout.kanji_detailcontainer, new LinearLayout(context),false);
        container.setId(containerID);
        container.setTag(tag);
        parent.addView(container);

        LinearLayout layout = (LinearLayout) inflater.inflate(R.layout.kanji_detailtitle, new LinearLayout(context),false);
        layout.setId(containerID+1);
        TextView textView = (TextView) layout.findViewById(R.id.detailTitle);
        textView.setText(title);
        container.addView(layout);

        exampleDetails(in,kanjiString, container);
        exampleNames(in,kanjiString,container);

    }
    public static List<LinearLayout> examplesToList(String in, LayoutInflater inflater){
        List<LinearLayout> out = new ArrayList<LinearLayout>();

        String workingString;
        SpannableString workingSpan;
        SpannableStringBuilder workingBuilder;
        LinearLayout container;
        LinearLayout detail;
        TextView detailText;

        try{
            JSONArray exampleArray = new JSONArray(in);

            int count = exampleArray.length();
            for (int i = 0; i < count; i++){

                container = (LinearLayout) inflater.inflate(R.layout.kanji_detailcontainer,null);
                JSONObject example = exampleArray.getJSONObject(i);
                SpannableStringBuilder exampleStr = new SpannableStringBuilder();

                //exampleStr += example.toString();


                /*
                 *  Get kanji and add to kanji_detail
                 */
                //String kanjiStr = "";
                //SpannableStringBuilder kanjiString = new SpannableStringBuilder();
                workingBuilder = new SpannableStringBuilder();
                JSONArray kanjiArray = example.getJSONArray("k");
                //SpannableString kanjiSpan;
                int len = kanjiArray.length();
                for (int q = 0; q < len; q++){
                    workingString = "";
                    if (q>0) workingString += RDNG_SEP;
                    JSONObject kanji = kanjiArray.getJSONObject(q);
                    workingString += kanji.getString("txt");
                    workingSpan = new SpannableString(workingString);
                    workingSpan.setSpan(new StyleSpan(Typeface.BOLD), 0, workingString.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                    if (q>0) workingSpan.setSpan(new RelativeSizeSpan(0.9f), 0, workingString.length(),Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                    workingBuilder.append(workingSpan);
                }
                // SpannableString kanjiSpan = new SpannableString(kanjiStr);
                // kanjiSpan.setSpan(new StyleSpan(Typeface.BOLD),0,kanjiStr.length(),Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                detail = (LinearLayout) inflater.inflate(R.layout.kanji_detail,container,true);
                detailText = (TextView) detail.findViewById(R.id.detailList);
                detailText.setText(workingBuilder);

                //exampleStr.append(kanjiSpan);

                /*
                 * Get readings and add to string
                 */
                JSONArray rdngArray = example.getJSONArray("r");
                len = rdngArray.length();
                //String rdngStr = "";
                workingString = "";
                if (len > 0) workingString += "(";
                for (int q = 0; q < len; q++){
                    if (q>0) workingString += RDNG_SEP;
                    JSONObject rdng = rdngArray.getJSONObject(q);
                    workingString += rdng.getString("txt");
                }
                if (len > 0) workingString += ")";
                detail = (LinearLayout) inflater.inflate(R.layout.example_detail, null);
                detailText = (TextView) detail.findViewById(R.id.exampleText);
                detailText.setText(workingString);
                container.addView(detail);
                //SpannableString rdngSpan = new SpannableString(rdngStr);
                //rdngSpan.setSpan(new ForegroundColorSpan(Color.DKGRAY), 0, rdngStr.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                //exampleStr.append(rdngSpan);
                /*
                 *
                 * Get sense: includes Part of Speech, and Gloss
                 * If word is nanori type modify output
                 *
                 */

                JSONArray senseArray = example.getJSONArray("s");
                len = senseArray.length();

                //String meaningStr = "";
                //Check for NANORI type
                //Loop will continue if found

                if (example.getInt("rt") == RDNG_NANORI){
                    //exampleStr.insert(0, "(name)");
                    workingSpan = new SpannableString("NAME ");
                    workingSpan.setSpan(new ForegroundColorSpan(Color.LTGRAY),0,workingSpan.length(),Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                    workingBuilder = new SpannableStringBuilder();
                    workingBuilder.append(workingSpan);

                    workingString = "";
                    for (int q = 0; q<len; q++){
                        JSONObject sense = senseArray.getJSONObject(q);
                        JSONArray posArray = sense.getJSONArray("p");
                        if (posArray.length() > 0) workingString += "(";
                        for (int r = 0; r < posArray.length(); r++){
                            if (r>0) workingString += RDNG_SEP;
                            workingString += posArray.getString(r);
                        }
                        if (posArray.length() > 0) workingString += ") ";
                    }
                    workingBuilder.append(workingString);
                    //SpannableString meaningSpan = new SpannableString(meaningStr);
                    //meaningSpan.setSpan(new RelativeSizeSpan(0.8f), 0, meaningStr.length(),Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                    //exampleStr.append(meaningSpan);
                    //exampleStr.setSpan(new LeadingMarginSpan.Standard(0,15),0,exampleStr.length(),Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                    detail = (LinearLayout) inflater.inflate(R.layout.example_detail,null);
                    detailText = (TextView) detail.findViewById(R.id.exampleText);
                    detailText.setText(workingBuilder);
                    container.addView(detail);
                    out.add(container);
                    continue;
                }
                /*
                 * Add meanings for all other types
                 */
                workingString = "";
                int codePoint = codePointStart;
                //if (len > 0) workingString += ": ";

                for (int q = 0; q < len; q++){
                    JSONObject sense = senseArray.getJSONObject(q);

                    /*
                     * Set sense to front of the string
                     */

                    //TODO Replace codePoint with a specialized drawable span.
                    if (codePoint <= codePointEnd){
                        workingString += Character.toString((char)codePoint);
                        codePoint++;
                    }
                    else{
                        workingString += "(" + Integer.toString(q) + ")";
                    }

                    /*
                     * Get Part of Speech
                     */
                    JSONArray posArray = sense.getJSONArray("p");
                    if (posArray.length() > 0) workingString += "(";
                    for (int r = 0; r < posArray.length(); r++){
                        if (r>0) workingString += MEANING_SEP;
                        workingString += posArray.getString(r);
                    }
                    if (posArray.length() > 0) workingString += ") ";

                    /*
                     * Get any kanji/readings specific to sense
                     */

                    JSONArray krArray = sense.getJSONArray("kr");
                    if (krArray.length() > 0 ) workingString += "<";
                    for (int r = 0; r < krArray.length(); r++){
                        if (r>0) workingString += RDNG_SEP;
                        workingString += krArray.getString(r);
                    }
                    if (krArray.length() >0) workingString += " only> ";

                    /*
                     * Get gloss
                     */

                    JSONArray glossArray = sense.getJSONArray("g");
                    for (int r = 0; r < glossArray.length(); r++){
                        if (r>0) workingString += RDNG_SEP;
                        workingString += glossArray.getString(r);
                    }
                }

                detail = (LinearLayout) inflater.inflate(R.layout.example_detail, null);
                detailText = (TextView) detail.findViewById(R.id.exampleText);
                detailText.setText(workingString);
                container.addView(detail);
                //SpannableString meaningSpan = new SpannableString(meaningStr);
                // meaningSpan.setSpan(new RelativeSizeSpan(0.8f), 0, meaningStr.length(),Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                //exampleStr.append(meaningSpan);
                //exampleStr.setSpan(new LeadingMarginSpan.Standard(0,20),0,exampleStr.length(),Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

                /*
                 * Get Sentence
                 */
                JSONArray sentArray = example.getJSONArray("snt");
                len = sentArray.length();
                //String sentStr = "";

                //if (len > 0) sentStr += "\n";
                for (int q = 0; q < len; q++){
                    //workingString = "";
                    JSONObject sentence = sentArray.getJSONObject(q);
                    JSONArray words = sentence.getJSONArray("wl");
                    final int w_len = words.length();
                    String sentText = sentence.getString("sentJ");
                    SpannableString sentSpan = new SpannableString(sentence.getString("sentJ"));
                    for (int r = 0; r < w_len; r++){
                        JSONObject furi = words.getJSONObject(r);
                        final int start = sentText.indexOf(furi.getString("word"),0);
                        final int end = start + furi.getString("word").length();
                        FuriganaSpan furiSpan = new FuriganaSpan(furi.getString("furi"), false);
                        sentSpan.setSpan(furiSpan,start,end, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                        //sentSpan.setSpan(new ClickableFuriganaSpan(furiSpan), start, end, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

                    }
                    //detail = sentenceDetail(sentSpan, inflater, true);
                    //container.addView(detail);

                    //detail = sentenceDetail(sentence.getString("sentJ"), inflater, false);
                    //container.addView(detail);

                    //detail = sentenceDetail(sentence.getString("sentE"), inflater, false);
                    // container.addView(detail);

                    //sentStr += sentence.getString("sentJ") + "\n";
                    //sentStr += sentence.getString("sentE");
                }
                //SpannableString sentSpan = new SpannableString(sentStr);
                //sentSpan.setSpan(new RelativeSizeSpan(0.9f),0,sentSpan.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                //exampleStr.append(sentSpan);



                //LinearLayout layout = (LinearLayout) inflater.inflate(R.layout.kanji_detail,null);
                //TextView textView = (TextView) layout.findViewById(R.id.detailList);
                //textView.setText(exampleStr);
                out.add(container);
            }

        } catch (Exception e){
            e.printStackTrace();
        }
        return out;
    }
    private static LinearLayout sentenceDetail(CharSequence string, Context context, boolean isClickable){
        final LayoutInflater inflater = LayoutInflater.from(context);
        final SpannableString workingSpan = new SpannableString(string);
        workingSpan.setSpan(new RelativeSizeSpan(1.1f),0,workingSpan.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

        final LinearLayout detail = (LinearLayout) inflater.inflate(R.layout.kanji_detail, new LinearLayout(context), false);
        final ImageView detailTab = (ImageView) detail.findViewById(R.id.exampleTab);
        detailTab.setVisibility(View.VISIBLE);
        final TextView detailText = (TextView) detail.findViewById(R.id.detailList);

        detailText.setText(workingSpan);


        if (isClickable) {
            detailText.setClickable(true);
            detailText.setOnClickListener(new TextView.OnClickListener() {
                public boolean isVisible = false;
                @Override
                public void onClick(View widget) {
                    final TextView textView = (TextView) widget;
                    final SpannedString span = (SpannedString) textView.getText();
                    final FuriganaSpan[] furiganaSpans = span.getSpans(0, span.length(), FuriganaSpan.class);
                    isVisible = !isVisible;
                    for (int i = 0; i < furiganaSpans.length; i++) {
                        furiganaSpans[i].setVisibility(isVisible);
                    }
                    textView.setText(span);

                }
            });
            //detailText.setMovementMethod(LinkMovementMethod.getInstance());
        }
        return detail;
    }
    public static List<LinearLayout> examplesToListSpannable(String in, LayoutInflater inflater){
        List<LinearLayout> out = new ArrayList<LinearLayout>();

        String workingString;
        SpannableString workingSpan;
        SpannableStringBuilder workingBuilder;
        LinearLayout container;
        LinearLayout detail;
        TextView detailText;
        ImageView detailTab;

        try{
            JSONArray exampleArray = new JSONArray(in);

            int count = exampleArray.length();
            for (int i = 0; i < count; i++){

                container = (LinearLayout) inflater.inflate(R.layout.kanji_detailcontainer,null);
                JSONObject example = exampleArray.getJSONObject(i);
                SpannableStringBuilder exampleStr = new SpannableStringBuilder();

                //exampleStr += example.toString();


                /*
                 *  Get kanji and add to kanji_detail
                 */
                //String kanjiStr = "";
                //SpannableStringBuilder kanjiString = new SpannableStringBuilder();
                workingBuilder = new SpannableStringBuilder();
                JSONArray kanjiArray = example.getJSONArray("k");
                //SpannableString kanjiSpan;
                int len = kanjiArray.length();
                for (int q = 0; q < len; q++){
                    workingString = "";
                    if (q>0) workingString += RDNG_SEP;
                    JSONObject kanji = kanjiArray.getJSONObject(q);
                    workingString += kanji.getString("txt");
                    workingSpan = new SpannableString(workingString);
                    workingSpan.setSpan(new StyleSpan(Typeface.BOLD), 0, workingString.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                    if (q>0) workingSpan.setSpan(new RelativeSizeSpan(0.9f), 0, workingString.length(),Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                    workingBuilder.append(workingSpan);
                }
                exampleStr.append(workingBuilder);
                // SpannableString kanjiSpan = new SpannableString(kanjiStr);
                // kanjiSpan.setSpan(new StyleSpan(Typeface.BOLD),0,kanjiStr.length(),Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                /*detail = (LinearLayout) inflater.inflate(R.layout.kanji_detail,container,true);
                detailText = (TextView) detail.findViewById(R.id.detailList);
                detailText.setText(workingBuilder);*/

                //exampleStr.append(kanjiSpan);

                /*
                 * Get readings and add to string
                 */
                exampleStr.append("\n\t");
                JSONArray rdngArray = example.getJSONArray("r");
                len = rdngArray.length();
                //String rdngStr = "";
                workingString = "";
                if (len > 0) workingString += "(";
                for (int q = 0; q < len; q++){
                    if (q>0) workingString += RDNG_SEP;
                    JSONObject rdng = rdngArray.getJSONObject(q);
                    workingString += rdng.getString("txt");
                }
                if (len > 0) workingString += ")";
                exampleStr.append(workingString);

                /*
                detail = (LinearLayout) inflater.inflate(R.layout.example_detail, null);
                detailText = (TextView) detail.findViewById(R.id.exampleText);
                detailText.setText(workingString);
                container.addView(detail);
                */
                //SpannableString rdngSpan = new SpannableString(rdngStr);
                //rdngSpan.setSpan(new ForegroundColorSpan(Color.DKGRAY), 0, rdngStr.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                //exampleStr.append(rdngSpan);
                /*
                 *
                 * Get sense: includes Part of Speech, and Gloss
                 * If word is nanori type modify output
                 *
                 */

                JSONArray senseArray = example.getJSONArray("s");
                len = senseArray.length();

                //String meaningStr = "";
                //Check for NANORI type
                //Loop will continue if found

                if (example.getInt("rt") == RDNG_NANORI){
                    exampleStr.append("\n\t");
                    //exampleStr.insert(0, "(name)");
                    workingSpan = new SpannableString("NAME ");
                    workingSpan.setSpan(new ForegroundColorSpan(Color.LTGRAY),0,workingSpan.length(),Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                    workingBuilder = new SpannableStringBuilder();
                    workingBuilder.append(workingSpan);

                    workingString = "";
                    for (int q = 0; q<len; q++){
                        JSONObject sense = senseArray.getJSONObject(q);
                        JSONArray posArray = sense.getJSONArray("p");
                        if (posArray.length() > 0) workingString += "(";
                        for (int r = 0; r < posArray.length(); r++){
                            if (r>0) workingString += RDNG_SEP;
                            workingString += posArray.getString(r);
                        }
                        if (posArray.length() > 0) workingString += ") ";
                    }
                    workingBuilder.append(workingString);
                    exampleStr.append(workingBuilder);
                    //SpannableString meaningSpan = new SpannableString(meaningStr);
                    //meaningSpan.setSpan(new RelativeSizeSpan(0.8f), 0, meaningStr.length(),Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                    //exampleStr.append(meaningSpan);
                    //exampleStr.setSpan(new LeadingMarginSpan.Standard(0,15),0,exampleStr.length(),Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

                    detail = (LinearLayout) inflater.inflate(R.layout.kanji_detail,null);
                    detailText = (TextView) detail.findViewById(R.id.detailList);
                    detailText.setText(exampleStr);
                    container.addView(detail);
                    out.add(container);
                    continue;
                }
                /*
                 * Add meanings for all other types
                 */
                workingString = "";
                int codePoint = codePointStart;
                //if (len > 0) workingString += ": ";
                exampleStr.append("\n\t");
                for (int q = 0; q < len; q++){

                    JSONObject sense = senseArray.getJSONObject(q);

                    /*
                     * Set sense to front of the string
                     */

                    //TODO Replace codePoint with a specialized drawable span.
                    if (codePoint <= codePointEnd){
                        workingString += Character.toString((char)codePoint);
                        codePoint++;
                    }
                    else{
                        workingString += "(" + Integer.toString(q) + ")";
                    }

                    /*
                     * Get Part of Speech
                     */
                    JSONArray posArray = sense.getJSONArray("p");
                    if (posArray.length() > 0) workingString += "(";
                    for (int r = 0; r < posArray.length(); r++){
                        if (r>0) workingString += MEANING_SEP;
                        workingString += posArray.getString(r);
                    }
                    if (posArray.length() > 0) workingString += ") ";

                    /*
                     * Get any kanji/readings specific to sense
                     */

                    JSONArray krArray = sense.getJSONArray("kr");
                    if (krArray.length() > 0 ) workingString += "<";
                    for (int r = 0; r < krArray.length(); r++){
                        if (r>0) workingString += RDNG_SEP;
                        workingString += krArray.getString(r);
                    }
                    if (krArray.length() >0) workingString += " only> ";

                    /*
                     * Get gloss
                     */

                    JSONArray glossArray = sense.getJSONArray("g");
                    for (int r = 0; r < glossArray.length(); r++){
                        if (r>0) workingString += RDNG_SEP;
                        workingString += glossArray.getString(r);
                    }

                }
                exampleStr.append(workingString);
                /*
                detail = (LinearLayout) inflater.inflate(R.layout.example_detail, null);
                detailText = (TextView) detail.findViewById(R.id.exampleText);
                detailText.setText(workingString);
                container.addView(detail);*/
                //SpannableString meaningSpan = new SpannableString(meaningStr);
                // meaningSpan.setSpan(new RelativeSizeSpan(0.8f), 0, meaningStr.length(),Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                //exampleStr.append(meaningSpan);
                //exampleStr.setSpan(new LeadingMarginSpan.Standard(0,20),0,exampleStr.length(),Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

                /*
                 * Get Sentence
                 */
                JSONArray sentArray = example.getJSONArray("snt");
                len = sentArray.length();
                //String sentStr = "";

                //if (len > 0) sentStr += "\n";
                for (int q = 0; q < len; q++){
                    //workingString = "";
                    JSONObject sentence = sentArray.getJSONObject(q);
                    JSONArray words = sentence.getJSONArray("wl");
                    final int w_len = words.length();
                    String sentText = sentence.getString("sentJ");
                    SpannableString sentSpan = new SpannableString(sentence.getString("sentJ"));
                    for (int r = 0; r < w_len; r++){
                        JSONObject furi = words.getJSONObject(r);
                        final int start = sentText.indexOf(furi.getString("word"),0);
                        int end = start + furi.getString("word").length();
                       // Log.i("ParseJSON", "end before: "+ end);
                        for (int s = start + furi.getString("word").length()-1; s >= start; s --){
                            final char c = sentText.charAt(s);
                            if (Character.UnicodeBlock.of(c) != Character.UnicodeBlock.HIRAGANA &&
                                    Character.UnicodeBlock.of(c) != Character.UnicodeBlock.KATAKANA){
                                end = s;
                                break;
                            }
                        }
                        //Log.i("ParseJSON", "end after: " + end);
                        FuriganaSpan furiSpan = new FuriganaSpan(furi.getString("furi"), false);
                        sentSpan.setSpan(furiSpan,start,end, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                        sentSpan.setSpan(new ClickableFuriganaSpan(furiSpan), start, end, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

                    }
                    exampleStr.append("\n\t");
                    exampleStr.append(sentSpan);
                    exampleStr.append("\n\t");
                    exampleStr.append(sentence.getString("sentE"));
                    /*
                    detail = sentenceDetail(sentSpan, inflater, true);
                    container.addView(detail);

                    //detail = sentenceDetail(sentence.getString("sentJ"), inflater, false);
                    //container.addView(detail);

                    detail = sentenceDetail(sentence.getString("sentE"), inflater, false);
                    container.addView(detail);
                    */
                    //sentStr += sentence.getString("sentJ") + "\n";
                    //sentStr += sentence.getString("sentE");
                }
                //SpannableString sentSpan = new SpannableString(sentStr);
                //sentSpan.setSpan(new RelativeSizeSpan(0.9f),0,sentSpan.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                //exampleStr.append(sentSpan);



                detail = (LinearLayout) inflater.inflate(R.layout.kanji_detail,null);
                detailText = (TextView) detail.findViewById(R.id.detailList);
                detailText.setText(exampleStr);
                detailText.setMovementMethod(LinkMovementMethod.getInstance());
                out.add(detail);
            }

        } catch (Exception e){
            e.printStackTrace();
        }
        return out;
    }

}
