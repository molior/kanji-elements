package com.brian.kanjielementtests;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

/**
 * Created by Brian on 3/2/2015.
 */
public abstract class EndlessListAdapter extends BaseAdapter implements AbsListView.OnScrollListener {



    private List<? extends RowData> mList;
    
    private boolean mDataValid;
    private int mLayout;
    private ViewBinder mViewBinder;
    private Context mContext;
    
    private static int LOADING_ROW = 1;

    //Loader variables
    private boolean mIsLoading;
    private int mCurrentOffset;
    private int mServerListSize;
    private int mThreshold;
    
    public EndlessListAdapter (Context context, List<? extends RowData> list,int layout, ViewBinder viewBinder, int threshold, int currentOffset){
        mContext = context;
        mList = list;
        mLayout = layout;
        mViewBinder = viewBinder;
        mCurrentOffset = currentOffset;
        mThreshold = threshold;
        if (mList != null && mList.size() > 0)
        {
            mDataValid = true;
            mIsLoading = false;
            mServerListSize = mList.size();
        }
        else
        {
            mDataValid = false;
            mIsLoading = true;
            mServerListSize = 0;
        }
    }
    public void setServerListSize(int size){
        mServerListSize = size;
    }
    public int getServerListSize(){
        return mServerListSize;
    }
    public void changeList(List<? extends RowData> list){
        mList = list;
        mDataValid = mList != null && mList.size() > 0;
        Log.i("EndlessListAdapter", "updatingList: " + mDataValid);
        notifyDataSetChanged();
    }
    public void notifyLoadComplete(){
        if (mDataValid) mIsLoading = false;
        else mIsLoading = true;
    }
    @Override
    public int getCount(){
        if (mDataValid){
            return mList.size();
        }
        return 0;
    }
    @Override
    public Object getItem(int position){
        if (mDataValid)
            return mList.get(position);
        else
            throw new IllegalStateException("this should only be called when the cursor is valid");
    }
    @Override
    public boolean isEmpty(){
        return !mDataValid;
    }
    @Override
    public int getViewTypeCount(){
        return 1;
    }
    @Override
    public int getItemViewType(int position){
        return 0;
    }
    
    @Override
    public boolean isEnabled(int position){        
        return mDataValid;
    }
    @Override
    public boolean hasStableIds(){
        return true;
    }
    @Override
    public long getItemId(int position){
        return mList.get(position).id;
    }
    @Override
    public View getView(int position, View convertView, ViewGroup parent){
        if(mList.size() == 0)
            throw new IllegalStateException("this should only be called when the cursor is valid");
        View v;
        if (convertView == null) v = newView(parent);
        else v = convertView;
        mViewBinder.bindView(v, mList.get(position));
        return v;
    }

    public View newView(ViewGroup parent){
        LayoutInflater inflater = LayoutInflater.from(mContext);
        View newView = inflater.inflate(mLayout, parent, false);
        newView.setTag(mViewBinder.getViewHolder(newView));
        return newView;
    }
    @Override
    public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount){
        if(!mIsLoading)
        {
            final int size = mList.size();
            if(firstVisibleItem < mThreshold && mCurrentOffset > 0){
                Log.i("EndlessListAdapter", "load previous");
                mCurrentOffset = mCurrentOffset - size/2;
                if (mCurrentOffset < 0) mCurrentOffset = 0;
                mIsLoading = true;
                loadData(mCurrentOffset,size);
            }
            else if (firstVisibleItem + visibleItemCount > totalItemCount - mThreshold && mCurrentOffset+size < mServerListSize){
                Log.i("EndlessListAdapter", "load next");
                mCurrentOffset = mCurrentOffset + size / 2;
                mIsLoading = true;
                loadData(mCurrentOffset, size);
            }
        }
    }

    @Override
    public void onScrollStateChanged(AbsListView View, int scrollState){}

    public abstract void loadData(int offset, int listSize);

    public interface ViewBinder{
        public void bindView(View v, RowData row);
        public Object getViewHolder(View vew);
    }
    public static class RowData{
        public final long id;
        public RowData(long id){
            this.id = id;
        }
    }
}
