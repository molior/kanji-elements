package com.brian.kanjielementtests;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.Random;

/**
 * Created by BRIAN on 2/24/2015.
 */
public class UserSettings {
    private static final String TAG = UserSettings.class.getSimpleName();

    public static String KEY_IS_NEW = "new_kanji";
    public static String KEY_KANJI = "kanji";
    public static String KEY_STROKE = "stroke_count";
    public static String KEY_HEISIG = "heisig";
    public static String KEY_RDNG = "reading";
    public static String KEY_MEANING = "meaning";
    public static String KEY_EXAMPLE = "example";
    public static String KEY_SVG = "svg";
    public static String KEY_STAGE= "stage_id";
    public static String KEY_INTERVAL = "interval_id";
    public static String KEY_HISTORY = "history";
    public static String KEY_ID = "_id";
    public static String KEY_PENDING = "pending";
    public static String KEY_NEEDS_CORRECT = "needs_correct";
    public static String KEY_DUE_DATE = "due_date";
    public static String KEY_USER_HISTORY = "user_history";
    
    public class Stage{

        public int backgroundColor;
        public LayoutTag.Type[] excludedTypes;
        public int[] intervals;
        public Stage(int backgroundColor, LayoutTag.Type[] excludedTypes, int[] intervals){
            this.backgroundColor = backgroundColor;
            this.excludedTypes = excludedTypes;
            this.intervals = intervals;
        }
    }

    private static UserSettings instance;

    /*
     * Default Stages
     */
    public static UserSettings getInstance(){
        if(instance == null) instance = getSync();
        return instance;
    }
    private static synchronized UserSettings getSync(){
        if(instance == null) instance = new UserSettings();
        return instance;
    }

    private Context context;
    private final Stage[] defaultStages;
    private List<Stage> userStages;
    private static int checkNone = -1;
    private static int checkAll = 0;
    private int historyCheckforIntervalSkip;           //Number of previous tests to check for 0 for all

    private final Stage initialStage;
    private final Stage finalStage;
    private int userHistoryCap;
    private int userSessionLimit;
    private int userNewPerSessionLimit;
    private boolean userSkipInitialStage;
    private Random random;
    private long randomSeed;
    private UserSettings(){
        context = App.getInstance().getApplicationContext();
        initialStage = new Stage(context.getResources().getColor(R.color.layout_darkgray),
                new LayoutTag.Type[]{}, new int[]{});
        finalStage =  new Stage(context.getResources().getColor(R.color.complete_stage),
                new LayoutTag.Type[]{}, new int[]{-1});


        defaultStages = new Stage[]{
                new Stage(context.getResources().getColor(R.color.kanji_stage),
                        new LayoutTag.Type[]{LayoutTag.Type.KANJI}, new int[]{1, 2}),
                new Stage(context.getResources().getColor(R.color.meaning_stage),
                        new LayoutTag.Type[]{LayoutTag.Type.KANJI, LayoutTag.Type.MEANING}, new int[]{3, 7}),
                new Stage(context.getResources().getColor(R.color.rdng_stage),
                        new LayoutTag.Type[]{LayoutTag.Type.KANJI, LayoutTag.Type.MEANING, LayoutTag.Type.RDNG_KUN},
                        new int[]{7, 14}),
                new Stage(context.getResources().getColor(R.color.example_stage),
                        new LayoutTag.Type[]{LayoutTag.Type.MEANING, LayoutTag.Type.RDNG_ALL},
                        new int[]{30, 45, 60, 120})
        };
        Log.i(TAG, "defaultStage length: " + defaultStages.length);

        setStagesFromJSON(context.getString(R.string.defualt_stages));
        /*userStages = new ArrayList<Stage>();
        userStages.addAll(Arrays.asList(defaultStages));
        userStages.add(0,initialStage);
        userStages.add(finalStage);*/
        userHistoryCap = context.getResources().getInteger(R.integer.default_history_cap);
        userSessionLimit = context.getResources().getInteger(R.integer.default_session_limit);
        userNewPerSessionLimit = context.getResources().getInteger(R.integer.default_new_per_session_limit);
        historyCheckforIntervalSkip = checkAll;
        userSkipInitialStage = context.getResources().getBoolean(R.bool.default_skip_initial_stage);
        random = new Random();
        randomSeed = random.nextLong();
        random.setSeed(randomSeed);
    }
    public Stage getInitialStage(){
        return initialStage;
    }
    public long getRandomSeed() {return randomSeed;}
    public void setRandomSeed(long newSeed){
        randomSeed = newSeed;
        random.setSeed(newSeed);
    }
    public int getUserSessionLimit(){
        return userSessionLimit;
    }
    public int getUserNewPerSessionLimit(){
        return userNewPerSessionLimit;
    }
    public void setUserSessionLimit(int newLimit){
        userSessionLimit = newLimit;
    }
    public void setUserNewPerSessionLimit(int newNewPerSessionLimit){
        userNewPerSessionLimit = newNewPerSessionLimit;
    }

    public int getHistoryCap(){
        return userHistoryCap;
    }
    public void setUserHistoryCap(int userHistoryCap) {
        this.userHistoryCap = userHistoryCap;
    }

    public boolean isSkipInitialStage(){
        return userSkipInitialStage;
    }
    public void setUserSkipInitialStage(boolean userSkipInitialStage) {
        this.userSkipInitialStage = userSkipInitialStage;
    }

    public Stage getStage(int stageId){
        if (stageId+1 > userStages.size()) return userStages.get(userStages.size() - 1);
        return userStages.get(stageId);
    }
    public String stagesToJSON(){

        JSONArray stages = new JSONArray();
        try {
            final int stage_length = userStages.size();
            for (int i = 1; i < stage_length -1; i ++) {
                Stage stage = userStages.get(i);
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("color",stage.backgroundColor);

                JSONArray types = new JSONArray();
                for (LayoutTag.Type type: stage.excludedTypes){
                    types.put(type.ordinal());
                }
                jsonObject.put("types",types);

                JSONArray intervals =new JSONArray();
                for (int q: stage.intervals){
                    intervals.put(q);
                }
                jsonObject.put("intervals",intervals);
                stages.put(jsonObject);
            }
        } catch (JSONException e){
            e.printStackTrace();
        }
        return stages.toString();
    }

    public void setStagesFromJSON(String input){
        try {
            userStages = new ArrayList<Stage>();
            JSONArray stages = new JSONArray(input);
            final int stages_count = stages.length();
            for (int i = 0; i < stages_count; i ++){
                JSONObject jsonStage = stages.getJSONObject(i);
                int color = jsonStage.getInt("color");

                JSONArray jsonTypes = jsonStage.getJSONArray("types");
                final int type_count = jsonTypes.length();
                LayoutTag.Type[] types = new LayoutTag.Type[type_count]  ;

                for (int q = 0; q < type_count; q ++){
                    types[q] = LayoutTag.Type.values()[jsonTypes.getInt(q)];
                }


                JSONArray jsonIntervals = jsonStage.getJSONArray("intervals");
                final int interval_count = jsonIntervals.length();
                int[] intervals = new int[interval_count];
                for (int q = 0; q < interval_count; q ++){
                    intervals[q] = jsonIntervals.getInt(q);
                }

                userStages.add(new Stage(color, types, intervals));
            }
            userStages.add(0,initialStage);
            userStages.add(finalStage);

        } catch (JSONException e){
            e.printStackTrace();
        }
    }
    public int getNextStageId(int currentStageId){
        if (currentStageId+1 < userStages.size())
            return currentStageId +1;
        else
            return -1;
    }

    public Bundle calculateNextInterval(int oldStageId, int oldIntervalId, List<Integer> history, List<LayoutTag.Type> rdngTypesAvailable){
        Bundle args = new Bundle();
        int stageId = oldStageId;
        int intervalId = oldIntervalId;

        //Check if history is correct for all previous
        final int length = history.size();
        Log.i("UserSettings", "calculateNextInterval.length: " + length);
        boolean skipInterval = true;
        if (historyCheckforIntervalSkip != checkNone && !(length == 0 && !userSkipInitialStage)){
            if(length > 0) {
                int historyCheck = historyCheckforIntervalSkip == checkAll ? length :historyCheckforIntervalSkip;
                if (historyCheckforIntervalSkip > length) historyCheck = length;
                for (int i = length - 1; i >= (length - historyCheck); i--) {
                    if (history.get(i) != 1) {
                        skipInterval = false;
                        break;
                    }
                }
            }
        }
        else{
            skipInterval = false;
        }
        Stage stage = getStage(stageId);
        int intervalLength = stage.intervals.length;
        int intervalChange = skipInterval ? 2:1;
        //if previous answer was wrong then zero-out intervalChange
        Log.i("UserSettings", "NextInterval.intervalChange: "+ intervalChange);
        if (length > 0) intervalChange = history.get(length-1) == 0 ? 0 : intervalChange;
        if (intervalId + intervalChange < intervalLength){
            intervalId += intervalChange;
        }
        else{
            //intervalChange = (intervalId + intervalChange) - intervalLength;
            intervalId = 0;
            //getNextStage
            stageId = getNextStage(stageId, stage, rdngTypesAvailable);
        }
        args.putInt(KEY_STAGE, stageId);
        args.putInt(KEY_INTERVAL,intervalId);

        //calculate due date
        Calendar c = Calendar.getInstance();
        c.setTimeInMillis(System.currentTimeMillis());
        c.set(Calendar.SECOND,0);
        c.set(Calendar.MINUTE,0);
        c.set(Calendar.HOUR_OF_DAY,0);
        stage = getStage(stageId);
        c.add(Calendar.DAY_OF_YEAR,stage.intervals[intervalId]);
        args.putLong(KEY_DUE_DATE, c.getTimeInMillis());

        return args;
    }
    public int getStageCount(){
        return userStages.size();
    }

    public int getNextStage(int stageId, Stage currentStage, List<LayoutTag.Type> rdngTypesAvailable){
        if (stageId + 1 == userStages.size())
            return stageId;
        stageId ++;
        Stage newStage = getStage(stageId);
        List<LayoutTag.Type> testList = new ArrayList< LayoutTag.Type>(Arrays.asList(newStage.excludedTypes));
        final int typesSize = currentStage.excludedTypes.length;
        //remove old items (to get only newly removed items)
        for (int i = 0; i < typesSize; i ++){
            testList.remove(currentStage.excludedTypes[i]);
        }

        //remove all rdngTypes available to this kanji
        final int rdngTypesSize = rdngTypesAvailable.size();
        for (int i = 0; i < rdngTypesSize; i ++){
            testList.remove(rdngTypesAvailable.get(i));
        }
        Log.i("UserSettings", "testList: " + testList.toString());
        Log.i("UserSettings", "stageId: " + stageId);
        //clear list if non rdngType found;
        final int remainingSize = testList.size();
        boolean clearList = false;
        for (int i =0; i < remainingSize; i ++){
            switch(testList.get(i)){
                case RDNG_ON:
                case RDNG_KUN:
                case RDNG_NANORI:
                    break;
                default:
                    clearList = true;
                    break;
            }
        }
        if (clearList) testList.clear();
        //If test list has remaining items then they are items not found in the kanji > skip this stage
        if (testList.size() > 0)
            return getNextStage(stageId, newStage, rdngTypesAvailable);
        else
            return stageId;
    }

    public int randomInt(int max, int min){
        if (max - min == 0) return min;
        return (random.nextInt((max - min) + 1) + min);
    }


}
