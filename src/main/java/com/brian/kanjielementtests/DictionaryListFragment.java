package com.brian.kanjielementtests;

import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.app.ListFragment;
import android.os.Parcelable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ArrayAdapter;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.util.List;

/**
 * A fragment representing a list of Items.
 * <p/>
 * <p/>
 * Activities containing this fragment MUST implement the {@link OnFragmentInteractionListener}
 * interface.
 */
public class DictionaryListFragment extends ListFragment {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private boolean waitingForData;

    public static class KanjiData extends EndlessListAdapter.RowData{
        //private final long mId;
        final String kanji;
        final String rdngOn;
        final String rdngKun;
        final String meaning;
        public KanjiData(long id, String kanji, String rdngOn, String rdngKun, String meaning){
            super(id);
            this.kanji = kanji;
            this.rdngOn = rdngOn;
            this.rdngKun = rdngKun;
            this.meaning = meaning;
        }
    }
    /*
    public static class KanjiViewBinder1 implements EndlessListAdapter.ViewBinder{

        private class ViewHolder{
            TextView kanjiTV;
            TextView rdngOnTV;
            TextView rdngKunTV;
            TextView meaningTV;
        }
        @Override
        public void bindView(View v, EndlessListAdapter.RowData row) {
            ViewHolder viewHolder = (ViewHolder) v.getTag();
            viewHolder.kanjiTV.setText(((KanjiData) row).kanji);
            viewHolder.rdngOnTV.setText(((KanjiData)row).rdngOn);
            viewHolder.rdngKunTV.setText(((KanjiData)row).rdngKun);
            viewHolder.meaningTV.setText(((KanjiData)row).meaning);
        }
        @Override
        public Object getViewHolder(View view) {
            ViewHolder viewHolder = new ViewHolder();
            ProgressBar progressBar = (ProgressBar) view.findViewById(R.id.progressBar);
            progressBar.setVisibility(View.GONE);
            viewHolder.kanjiTV = (TextView) view.findViewById(R.id.kanjiText);
            viewHolder.kanjiTV.setVisibility(View.VISIBLE);
            viewHolder.rdngOnTV = (TextView) view.findViewById(R.id.rdngOnText);
            viewHolder.rdngOnTV.setVisibility(View.VISIBLE);
            viewHolder.rdngKunTV = (TextView) view.findViewById(R.id.rdngKunText);
            viewHolder.rdngKunTV.setVisibility(View.VISIBLE);
            viewHolder.meaningTV = (TextView) view.findViewById(R.id.meaningText);
            viewHolder.meaningTV.setVisibility(View.VISIBLE);
            return viewHolder;
        }
    }*/

    public static class KanjiViewBinder implements DictionaryCursorAdapter.ViewBinder{
        private static final int RDNG_JSON = 0;
        private static final int MEANING_JSON = 1;

        private static final int RDNG_ON = 0;
        private static final int RDNG_KUN = 1;
        private static final int MEANING = 2;

        private static class ViewHolder{
            TextView kanji;
            TextView rdngOn;
            TextView rdngKun;
            TextView meaning;
            ParseJSONTask task;
        }

        public int getLayout(){
            return R.layout.item_kanjilist;
        }

        public Object newViewHolder(View view){
            ViewHolder viewHolder = new ViewHolder();
            ProgressBar progressBar = (ProgressBar) view.findViewById(R.id.progressBar);
            progressBar.setVisibility(View.GONE);
            viewHolder.kanji = (TextView) view.findViewById(R.id.kanjiText);
            viewHolder.kanji.setVisibility(View.VISIBLE);
            viewHolder.rdngKun = (TextView) view.findViewById(R.id.rdngKunText);
            viewHolder.rdngKun.setVisibility(View.VISIBLE);
            viewHolder.rdngOn = (TextView) view.findViewById(R.id.rdngOnText);
            viewHolder.rdngOn.setVisibility(View.VISIBLE);
            viewHolder.meaning = (TextView) view.findViewById(R.id.meaningText);
            viewHolder.meaning.setVisibility(View.VISIBLE);
            return viewHolder;
        }
        public void bindView(View view, Context context, Cursor cursor){
            ViewHolder viewHolder = (ViewHolder) view.getTag();
            if(viewHolder.task != null && viewHolder.task.getStatus() == AsyncTask.Status.RUNNING){
                viewHolder.task.cancel(true);
            }
            final int kanjiCol = cursor.getColumnIndex("txt");
            final int rdngCol = cursor.getColumnIndex("rdng");
            final int meaningCol = cursor.getColumnIndex("meaning");
            viewHolder.kanji.setText(cursor.getString(kanjiCol));
            viewHolder.meaning.setText("");
            viewHolder.rdngKun.setText("");
            viewHolder.rdngOn.setText("");
            final String rdngJSON = cursor.getString(rdngCol);
            final String meaningJSON = cursor.getString(meaningCol);
            //Log.i("KanjiViewBinder","Data from cursor: " + cursor.getString(kanjiCol));
            viewHolder.task = new ParseJSONTask(viewHolder);
            viewHolder.task.execute(rdngJSON, meaningJSON);
        }

        private final class ParseJSONTask extends AsyncTask<String,Void,String[]>{
            private ViewHolder mViewHolder;
            public ParseJSONTask(ViewHolder viewHolder){
                mViewHolder = viewHolder;
            }
            @Override
            protected String[] doInBackground(String... params) {
                final String[] strings = new String[3];
                strings[RDNG_ON] = ParseJSON.rdngToString(params[RDNG_JSON],ParseJSON.RDNG_ON);
                strings[RDNG_KUN] = ParseJSON.rdngToString(params[RDNG_JSON],ParseJSON.RDNG_KUN);
                strings[MEANING] = ParseJSON.meaningToString(params[MEANING_JSON]);
                return strings;
            }

            @Override
            protected void onPostExecute(String... strings) {
                mViewHolder.rdngOn.setText(strings[RDNG_ON]);
                mViewHolder.rdngKun.setText(strings[RDNG_KUN]);
                mViewHolder.meaning.setText(strings[MEANING]);
            }
        }
    }

    private OnFragmentInteractionListener mListener;


    //private KanjiCursorAdapter mAdapter;
    private EndlessListAdapter mAdapter2;
    private ListAdapter mAdapter;

    // TODO: Rename and change types of parameters
    public static DictionaryListFragment newInstance(String param1, String param2) {
        DictionaryListFragment fragment = new DictionaryListFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public DictionaryListFragment() {
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        waitingForData = false;

        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }

        // TODO: Change Adapter to display your content
        String[] from = {"txt","rdng", "meaning"};
        int[] to = {R.id.kanjiText, R.id.rdngOnText, R.id.rdngKunText, R.id.meaningText};
        mAdapter = new DictionaryCursorAdapter(getActivity(),null,new KanjiViewBinder());
        //mListener.loadKanjiCursor();

        //mAdapter = new KanjiCursorAdapter(getActivity(),R.layout.item_kanjilist, null, from, to);
        /*mAdapter2 = new EndlessListAdapter(getActivity(),null, R.layout.item_kanjilist, new KanjiViewBinder1(), 10,0){
            @Override
            public void loadData(int offset, int limit){
                mListener.loadKanjiData(offset,limit);
            }
        };
        //final int threshold = 10;
        //final int listSize = 50;
        //
        /*mAdapter = new EndlessSimpleCursorAdapter(getActivity(), R.layout.item_kanjilist, null, from, to, threshold, listSize ){
            public void loadCursor(int offset, int limit){
                mListener.loadCursor(offset, limit); //callback to main activity to load new cursor
            }
        };*/
        //setListAdapter(mAdapter);
        //mAdapter.setServerListSize(3000);

//        getListView().setOnScrollListener(mAdapter);

    }
    private FrameLayout headerView;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        headerView = (FrameLayout) inflater.inflate(R.layout.dictionary_header, container, false);


        return super.onCreateView(inflater, container, savedInstanceState);
    }

    private int listLimit = 100;
    @Override
    public void onViewCreated(View view, Bundle args){
        super.onViewCreated(view, args);
        getListView().setDivider(getResources().getDrawable(R.drawable.dictionary_list_divider));
        getListView().setDividerHeight(8);
        getListView().setPadding(16,8,16,0);
        getListView().setFocusable(true);
        getListView().setFocusableInTouchMode(true);
        
       // getListView().addHeaderView(headerView);
        setListAdapter(mAdapter);
        setListShown(false);
    }
    public void notifyDataChange(){
        if (isVisible()) {
            setListShown(false);
            waitingForData = true;
        }
        ((DictionaryCursorAdapter) mAdapter).swapCursor(null);
    }
    @Override
    public void onPause(){
        super.onPause();
        //save state of list
        waitingForData = false;
        mListener.setPreviousState(getListView().onSaveInstanceState());
        //remove cursor from list
        ((DictionaryCursorAdapter) mAdapter).swapCursor(null);
    }
    @Override
    public void onStart(){
        super.onStart();
        requestData();
    }

    private void requestData(){
        waitingForData = true;
        mListener.loadKanjiCursorToDictionary();
    }

    public boolean isWaitingForData(){
        return waitingForData;
    }
    @Override
    public void onResume(){
        super.onResume();
        requestData();
        //mListener.loadKanjiCursorToDictionary();
        /*
        Parcelable state = mListener.getDictionaryListPreviousState();
        if(state != null)
        {
            getListView().onRestoreInstanceState(state);
        }*/
        //setListShown(!mAdapter.isEmpty());
    }
    public void updateKanjiCount(long count){
        mAdapter2.setServerListSize((int) count);
    }




    @Override
    public void onDetach() {
        super.onDetach();
        mListener.setPreviousState(null);
        mListener = null;
        waitingForData = false;
        ((DictionaryCursorAdapter)mAdapter).swapCursor(null);
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);

        if (null != mListener) {
            // Notify the active callbacks interface (the activity, if the
            // fragment is attached to one) that an item has been selected.
            mListener.onItemSelected((int)id, position);
            //mListener.onFragmentInteraction(DummyContent.ITEMS.get(position).id);
        }
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    /*
     * only called when fragment is attached and active
     */
    public void setData(Cursor cursor){
        if (cursor != null) {
            waitingForData = false;
            ((DictionaryCursorAdapter) mAdapter).changeCursor(cursor);
            setListShown(true);
            Parcelable listState = mListener.getDictionaryListPreviousState();
            if (listState != null)
                getListView().onRestoreInstanceState(listState);
        }
        
        //if(isAdded()) setListShown(true);
        //mAdapter.changeCursor(cursor);
        //if (oldCursor != null && !oldCursor.isClosed()) oldCursor.close();

    }
    public void removeData(){
        if (mAdapter!= null)
            ((DictionaryCursorAdapter) mAdapter).changeCursor(null);
    }
    public void setList(List<KanjiData> list){
        // save index and top position
        final boolean isEmpty = mAdapter2.isEmpty();
        int index = 0, top = 0;
        int change = 0;
        ListView listView = getListView();
        if (mAdapter2.getCount()>0) {
            index = listView.getFirstVisiblePosition();
            View v = listView.getChildAt(0);
            top = (v == null) ? 0 : (v.getTop() - listView.getPaddingTop());
            if (index > listLimit/2) change = index - listLimit/2;
            else change = listLimit/2 + index;

        }
        mAdapter2.changeList(list);
        //Log.i("ListFragment", "Setting selection" +change +"," +top + "from" + index);
        getListView().setSelectionFromTop(change,top);
        mAdapter2.notifyLoadComplete();
    }
    public void updateData(Cursor cursor) {}
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        public void onItemSelected(int id, int position);
        public void loadKanjiCursorToDictionary();
        //public void loadCursor(int offset, int limit);
       // public void loadKanjiData(int offset, int limit);
        public Parcelable getDictionaryListPreviousState();
        public void setPreviousState(Parcelable state);
    }

}
