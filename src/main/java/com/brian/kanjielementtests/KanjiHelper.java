package com.brian.kanjielementtests;

/**
 * Created by BRIAN on 5/18/2015.
 */
public class KanjiHelper {
    public static class ExampleNameData{
        public LayoutTag tag;
        public CharSequence kanji;
        public CharSequence type;
    }
    public static class ExampleWordData{
        public LayoutTag containerTag;
        public LayoutTag kanjiTag;
        public CharSequence kanji;
        public LayoutTag readingTag;
        public CharSequence reading;
        public LayoutTag meaningTag;
        public CharSequence meaning;
        public LayoutTag sentJTag;
        public CharSequence sentJ;
        public LayoutTag sentETag;
        public CharSequence sentE;
    }
}
