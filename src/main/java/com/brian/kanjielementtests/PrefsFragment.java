package com.brian.kanjielementtests;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.opengl.Visibility;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

/**
 * Created by Brian on 5/8/2015.
 */
public class PrefsFragment extends PreferenceFragment implements SharedPreferences.OnSharedPreferenceChangeListener{

    OnPrefsInteraction mListener;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        addPreferencesFromResource(R.xml.preferences);

        Preference restoreDefaults = (Preference) findPreference(getString(R.string.pref_restore_defaults));
        restoreDefaults.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
                AlertDialog dialog = new AlertDialog.Builder(getActivity())
                        .setTitle("Restore Default Settings")
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .setPositiveButton(getString(R.string.dialog_yes), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                //Do some code
                                Toast.makeText(getActivity(), "Settings restored", Toast.LENGTH_SHORT).show();
                            }
                        })
                        .setNegativeButton(getString(R.string.dialog_no), null)
                        .show();

                return false;
            }
        });

        Preference clearDatabase = findPreference(getString(R.string.pref_clear_database));
        clearDatabase.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
                AlertDialog dialog = new AlertDialog.Builder(getActivity())
                        .setTitle("Clear User Database")
                        .setMessage("Clear ALL user and history data.")
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .setPositiveButton(getString(R.string.dialog_yes), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                //Do some code
                                mListener.clearUserData();
                                Toast.makeText(getActivity(), "Database cleared", Toast.LENGTH_SHORT).show();
                            }
                        })
                        .setNegativeButton(getString(R.string.dialog_no), null)
                        .show();
                return false;
            }
        });

        Preference backupDatabase = findPreference(getString(R.string.pref_backup_database));
        backupDatabase.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
                AlertDialog dialog = new AlertDialog.Builder(getActivity())
                        .setTitle("Backup User Database")
                        .setMessage("Backup user data and history to Downloads directory. File name: kanjielements_YYYY-MM-DD")
                        .setPositiveButton(getString(R.string.dialog_yes), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                mListener.backupUserDbToFile();
                                Toast.makeText(getActivity(), "Saved to file name: kanjielements.txt", Toast.LENGTH_SHORT).show();
                            }
                        })
                        .setNegativeButton(getString(R.string.dialog_no), null)
                        .show();
                return false;
            }
        });

        Preference restoreDatabase = findPreference(getString(R.string.pref_restore_database));
        restoreDatabase.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
                //get list of files
                //logic to handle if no files are found (ie only cancel button)
                AlertDialog dialog = new AlertDialog.Builder(getActivity())
                        .setTitle("Restore User Database")
                        .setMessage("Select file from list. \n" +
                                "Warning: All user data and history will be overwritten.")
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .setPositiveButton(getString(R.string.dialog_yes), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                //Do some code
                                String message = mListener.restoreUserDbFromFile() ? "Successfully restored Database": "Error. Failed to restore database";
                                Toast.makeText(getActivity(), "Restored Database", Toast.LENGTH_SHORT).show();
                            }
                        })
                        .setNegativeButton(getString(R.string.dialog_no), null)
                        .show();
                return false;
            }
        });
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view =  super.onCreateView(inflater, container, savedInstanceState);
        //if (view != null)
         //   view.setBackgroundColor(getResources().getColor(R.color.preference_fragment_background));
        return view;
    }

    private void dialogMaker(Preference pref){

    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnPrefsInteraction) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        getPreferenceScreen().getSharedPreferences().registerOnSharedPreferenceChangeListener(this);
        mListener.setBackgroundOfActivityContainer(Color.WHITE);
        mListener.setVisibilityOfContainer(View.GONE);

    }

    @Override
    public void onPause() {
        super.onPause();
        getPreferenceScreen().getSharedPreferences().registerOnSharedPreferenceChangeListener(this);
        mListener.setBackgroundOfActivityContainer(getResources().getColor(R.color.layout_darkgray));
        mListener.setVisibilityOfContainer(View.VISIBLE);
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String s) {
        //update UserSettings
        UserSettings userSettings = UserSettings.getInstance();
        if(s.equals("cards_per_session")) {
            userSettings.setUserSessionLimit(Integer.parseInt(sharedPreferences.getString(s,"200")));
        }
        if(s.equals("new_cards_per_session") ) {
            userSettings.setUserNewPerSessionLimit(Integer.parseInt(sharedPreferences.getString(s, "20")));
        }
        if(s.equals("study_stage_skip")){
            userSettings.setUserSkipInitialStage(sharedPreferences.getBoolean("s",false));
        }
    }

    public interface OnPrefsInteraction {
        public void clearUserData();
        public void backupUserDbToFile();
        public boolean restoreUserDbFromFile();
        public void setBackgroundOfActivityContainer(int color);
        public void setVisibilityOfContainer(int visibility);
    }

}
