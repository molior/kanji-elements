package com.brian.kanjielementtests;

import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;

/**
 * Created by BRIAN on 3/3/2015.
 */
public class DictionaryCursorAdapter extends CursorAdapter {
    private ViewBinder mViewBinder;

    public DictionaryCursorAdapter(Context context, Cursor cursor, ViewBinder viewBinder){
        super(context, cursor, false);
        mViewBinder = viewBinder;
    }

    public void setViewBinder(ViewBinder viewBinder){
        mViewBinder = viewBinder;
    }

    public ViewBinder getViewBinder(){
        return mViewBinder;
    }

    public View newView(Context context, Cursor cursor, ViewGroup parent){
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(mViewBinder.getLayout(),parent,false);
        view.setTag(mViewBinder.newViewHolder(view));
        return view;
    }
    public void bindView(View view, Context context, Cursor cursor){
        mViewBinder.bindView(view,context,cursor);
    }

    public static interface ViewBinder{
        public void bindView(View view, Context context, Cursor cursor);
        public Object newViewHolder(View view);
        public int getLayout();
    }
}
