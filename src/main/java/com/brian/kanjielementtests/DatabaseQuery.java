package com.brian.kanjielementtests;

import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteQuery;
import android.database.sqlite.SQLiteQueryBuilder;
import android.os.Bundle;
import android.util.Log;
import android.util.Pair;


import com.readystatesoftware.sqliteasset.SQLiteAssetHelper;

import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Map;

/**
 * Created by Brian on 3/18/2015.
 */
public class DatabaseQuery {

    private static String TAG = DatabaseQuery.class.getSimpleName();
    private static String KANJI_DB_TBL_KANJI = "kanji";
    public static String COL_KANJI_ID = "_id";
    public static String COL_KANJI_TXT = "txt";
    public static String COL_KANJI_RDNG = "rdng";
    public static String COL_KANJI_MEANING = "meaning";
    public static String COL_KANJI_EXAMPLE = "example";
    public static String COL_KANJI_SVG = "svg";
    public static String COL_KANJI_HEISIG = "heisig";
    public static String COL_KANJI_STROKE_COUNT = "stroke_count";

    public static String USER_DB_TBL_USER_KANJI = "user_kanji";
    public static String COL_USER_KANJI_ID = "_id";
    public static String COL_USER_KANJI_DUE_DATE = "due_date";
    public static String COL_USER_KANJI_HISTORY = "history";
    public static String COL_USER_KANJI_STAGE = "stage_id";
    public static String COL_USER_KANJI_INTERVAL = "interval_id";
    public static String COL_USER_KANJI_PENDING = "pending";
    public static String COL_USER_KANJI_NEEDS_CORRECT = "needs_correct";
    public static String USER_DB_TBL_USER_SETTINGS = "user_settings";
    public static String COL_USER_SETTINGS_CUSTOM_STAGES = "custom_stages";
    public static String COL_USER_SETTINGS_SESSION_LIMIT = "session_limit";
    public static String COL_USER_SETTINGS_NEW_PER_SESSION_LIMIT = "new_per_session_limit";
    private static String USER_DB_TBL_HISTORY = "history";
    public static String COL_HISTORY_ID = "_id";
    public static String COL_HISTORY_DATE = "date";


    public static Cursor getNeedsCorrectKanji(SQLiteDatabase database, int sessionLimit){
        Cursor cursor = null;
        try {
            UserSettings userSettings = UserSettings.getInstance();
            int finalStage_id = userSettings.getStageCount() - 1;

            SQLiteQueryBuilder query = new SQLiteQueryBuilder();
            query.setTables(USER_DB_TBL_USER_KANJI);
            String[] select = {COL_KANJI_ID};
            //query.setTables(USER_DB_TBL_USER_KANJI);
            //String[] select = {COL_USER_KANJI_ID};
            Calendar calendar = Calendar.getInstance();
            calendar.setTimeInMillis(System.currentTimeMillis());
            calendar.set(Calendar.HOUR_OF_DAY, 0);
            calendar.set(Calendar.MINUTE, 0);
            calendar.set(Calendar.SECOND, 0);
            calendar.set(Calendar.MILLISECOND, 0);
            calendar.add(Calendar.DAY_OF_YEAR, 1);
            String where = COL_USER_KANJI_DUE_DATE + "< " + Long.toString(calendar.getTimeInMillis())
                    + " AND stage_id <> " + Integer.toString(finalStage_id)
                    + " AND " + COL_USER_KANJI_NEEDS_CORRECT + " = 1";
            //      + " OR " + COL_USER_KANJI_PENDING + " = 1"
            //      + " OR " + COL_USER_KANJI_NEEDS_CORRECT + " = 1";
            //where = null;
            String orderBy = COL_USER_KANJI_PENDING + " DESC, " + COL_USER_KANJI_NEEDS_CORRECT + " DESC, " +
                    COL_USER_KANJI_DUE_DATE + " ASC";
            //orderBy = COL_KANJI_HEISIG;
            String limit = Integer.toString(sessionLimit);
            //Log.i(TAG+".getDueKanji", "Query: " + query.buildQuery(select,where,null,null,orderBy,limit));
            cursor = query.query(database, select, where, null, null, null, orderBy, limit);
            cursor.moveToFirst();
            //Log.i("DatabaseQuery", "Due Kanji Dump: " + DatabaseUtils.dumpCursorToString(cursor));
            cursor.moveToFirst();

        } catch (SQLiteException e){
            e.printStackTrace();
        }


        return cursor;
    }


    public static Cursor getDueKanji(SQLiteDatabase database, int sessionLimit){
        Cursor cursor = null;
        try {
            UserSettings userSettings = UserSettings.getInstance();
            int finalStage_id = userSettings.getStageCount() - 1;

            SQLiteQueryBuilder query = new SQLiteQueryBuilder();
            query.setTables(USER_DB_TBL_USER_KANJI);
            String[] select = {COL_KANJI_ID, COL_USER_KANJI_NEEDS_CORRECT};
            //query.setTables(USER_DB_TBL_USER_KANJI);
            //String[] select = {COL_USER_KANJI_ID};
            Calendar calendar = Calendar.getInstance();
            calendar.setTimeInMillis(System.currentTimeMillis());
            calendar.set(Calendar.HOUR_OF_DAY, 0);
            calendar.set(Calendar.MINUTE, 0);
            calendar.set(Calendar.SECOND, 0);
            calendar.set(Calendar.MILLISECOND, 0);
            calendar.add(Calendar.DAY_OF_YEAR, 1);
            String where = COL_USER_KANJI_DUE_DATE + "< " + Long.toString(calendar.getTimeInMillis())
                    + " AND stage_id <> " + Integer.toString(finalStage_id);
               //     + " AND " + COL_USER_KANJI_NEEDS_CORRECT + " > 0";
              //      + " OR " + COL_USER_KANJI_PENDING + " = 1"
              //      + " OR " + COL_USER_KANJI_NEEDS_CORRECT + " = 1";
            //where = null;
            String orderBy = COL_USER_KANJI_PENDING + " DESC, " + COL_USER_KANJI_NEEDS_CORRECT + " DESC, " +
                    COL_USER_KANJI_DUE_DATE + " ASC";
            //orderBy = COL_KANJI_HEISIG;
            String limit = Integer.toString(sessionLimit);
            //Log.i(TAG+".getDueKanji", "Query: " + query.buildQuery(select,where,null,null,orderBy,limit));
            cursor = query.query(database, select, where, null, null, null, orderBy, limit);
            cursor.moveToFirst();
            //Log.i("DatabaseQuery", "Due Kanji Dump: " + DatabaseUtils.dumpCursorToString(cursor));
            cursor.moveToFirst();

        } catch (SQLiteException e){
            e.printStackTrace();
        }


        return cursor;
    }

    public static Cursor getNewKanji(SQLiteDatabase userKanji, int limit){
        Cursor cursor = null;
        try {
            List<Pair<String, String>> list = userKanji.getAttachedDbs();
            //for(Pair i:list)
            //    Log.i("DatabaseQuery", "userKanji attached dbs: " + i.first + ", " + i.second);
            //String rawSql = "ATTACH '" + kanjiDict.getPath() + "' AS kanji";

            //userKanji.execSQL(rawSql);
            //list = userKanji.getAttachedDbs();
            /*for(Pair i:list)
                Log.i("DatabaseQuery", "userKanji attached dbs: " + i.first + ", " + i.second);
                */
            String select =
                    "SELECT kanji.kanji._id AS _id, kanji.kanji.txt FROM kanji.kanji"
                    + " LEFT JOIN main.user_kanji ON main.user_kanji._id = kanji.kanji._id"
                    + " WHERE main.user_kanji.due_date IS NULL " +
                            "AND main.user_kanji.pending IS NULL " +
                            "AND main.user_kanji.needs_correct IS NULL"
                    + " ORDER BY CASE"
                                    + " WHEN main.user_kanji.order_seq IS NOT NULL"
                                    + " THEN kanji.kanji.heisig * 100 + main.user_kanji.order_seq"
                                    + " ELSE kanji.kanji.heisig * 100"
                                    + " END"
                    +" ASC LIMIT " + Integer.toString(limit);
            cursor = userKanji.rawQuery(select, null);
            cursor.moveToFirst();

        } catch (SQLiteException e){
            e.printStackTrace();
        }
        return cursor;
    }

    public static Bundle getKanjiById(SQLiteDatabase userDB,int id){

        Bundle bundle = new Bundle();
        try {
            Cursor c;
            //String rawSql = "ATTACH '" + kanjiDB.getPath() + "' AS kanji";
            //userDB.execSQL(rawSql);

            SQLiteQueryBuilder qb = new SQLiteQueryBuilder();

            String select = "SELECT kanji.kanji._id AS _id, txt, heisig, stroke_count, rdng, meaning, example, kanji.svg AS svg, stage_id, interval_id, pending, needs_correct, history, user_kanji._id AS user_id";
            String from = " FROM kanji";
            String join = " LEFT JOIN user_kanji ON user_kanji._id = kanji._id";
            String where = " WHERE kanji._id = " + id;
            //String[] sqlSelect = {"_id, pending, interval_id, stage_id, due_date, txt, heisig, stroke_count, rdng, meaning, example, svg"};
            //String sqlTables = "kanji";
            //String orderBy = "heisig";
            //String whereBy = "_id = " + kanjiID;


            //qb.setTables(sqlTables);
            //c = qb.query(db, sqlSelect, whereBy, null, null, null, null);
            c = userDB.rawQuery(select + from + join + where, null);
            c.moveToFirst();
            //Log.i("DatabaseQuery", "loading id: " + id);

            //Log.i("DatabaseQuery", "query string: " + select + from + join + where);
            c.moveToFirst();
            bundle.putInt(UserSettings.KEY_ID, c.getInt(0));
            bundle.putString(UserSettings.KEY_KANJI, c.getString(1));
            bundle.putInt(UserSettings.KEY_HEISIG, c.getInt(2));
            bundle.putInt(UserSettings.KEY_STROKE, c.getInt(3));
            bundle.putString(UserSettings.KEY_RDNG, c.getString(4));
            bundle.putString(UserSettings.KEY_MEANING, c.getString(5));
            bundle.putString(UserSettings.KEY_EXAMPLE, c.getString(6));
            bundle.putString(UserSettings.KEY_SVG, c.getString(7));
            //stageId
            //Log.i("DatabaseQuery", "data: Stage - " + c.getInt(8) + " Interval -" + c.getInt(9) + " History - " + c.getString(12));
            bundle.putInt(UserSettings.KEY_STAGE, c.getInt(8));
            //intervalId
            bundle.putInt(UserSettings.KEY_INTERVAL, c.getInt(9));
            //pending
            bundle.putInt(UserSettings.KEY_PENDING, c.getInt(10));
            //needsCorrect
            bundle.putInt(UserSettings.KEY_NEEDS_CORRECT, c.getInt(11));
            //historyJSON
            bundle.putString(UserSettings.KEY_HISTORY, c.getString(12));

            bundle.putBoolean(UserSettings.KEY_IS_NEW,c.isNull(13));
            c.close();

        } catch (SQLiteException e){
            e.printStackTrace();
        }
        return bundle;
    }
    public static List<MyActivity.HistoryEntry> getSessionHistoryList(SQLiteDatabase userDB, long date){
        //Return 15 days of session history - If no history available add entry that is empty.
        List<MyActivity.HistoryEntry> historyEntryList = new ArrayList<MyActivity.HistoryEntry>(15);
        try{
            Calendar fifteenDaysAgo = Calendar.getInstance();
            fifteenDaysAgo.setTimeInMillis(date);
            fifteenDaysAgo.set(Calendar.MINUTE,0);
            fifteenDaysAgo.set(Calendar.HOUR_OF_DAY,0);
            fifteenDaysAgo.set(Calendar.SECOND,0);
            fifteenDaysAgo.set(Calendar.MILLISECOND, 0);
            fifteenDaysAgo.add(Calendar.DAY_OF_YEAR, -15);


            Calendar current = Calendar.getInstance();
            current.setTimeInMillis(date);
            current.set(Calendar.MINUTE, 0);
            current.set(Calendar.HOUR_OF_DAY, 0);
            current.set(Calendar.SECOND,0);
            current.set(Calendar.MILLISECOND,0);

            Cursor c = userDB.rawQuery("SELECT _id, date, history_json FROM user_history " +
                    "WHERE date > " + Long.toString(fifteenDaysAgo.getTimeInMillis())+ " ORDER BY date DESC",null);
            c.moveToFirst();
            while(!c.isAfterLast()){
                final long entry_date =c.getLong(1);

                if (entry_date < current.getTimeInMillis()){
                    while(current.getTimeInMillis() > entry_date){
                        historyEntryList.add(new MyActivity.HistoryEntry(current.getTimeInMillis()));
                        current.add(Calendar.DAY_OF_YEAR, -1);
                    }
                }
                historyEntryList.add(new MyActivity.HistoryEntry(c.getLong(1), c.getString(2)));
                current.add(Calendar.DAY_OF_YEAR, - 1);
                c.moveToNext();
            }
            for (int i = historyEntryList.size(); i < 15; i ++){
                historyEntryList.add(new MyActivity.HistoryEntry(current.getTimeInMillis()));
                current.add(Calendar.DAY_OF_YEAR, -1);
            }
            c.close();
        }
        catch (SQLiteException e){
            e.printStackTrace();
        }
        return historyEntryList;
    }
    public static void fillTotalsView(SQLiteDatabase userDb, TotalsView totalsView){
        try{
            Cursor c = userDb.rawQuery("SELECT ifnull(stage_id,0) FROM " + KANJI_DB_TBL_KANJI +
                    " LEFT JOIN "+  USER_DB_TBL_USER_KANJI +
                        " ON " + KANJI_DB_TBL_KANJI + "."+COL_KANJI_ID + " = " + USER_DB_TBL_USER_KANJI + "." + COL_USER_KANJI_ID +
                    " ORDER BY " + COL_KANJI_HEISIG + " ASC", null);
            c.moveToFirst();
            Log.i("DatabaseQuery", "fillTotalsView c.count: " + c.getCount());
            totalsView.resetView(c.getCount());
            UserSettings userSettings = UserSettings.getInstance();
            while(!c.isAfterLast()){
                totalsView.addItem(userSettings.getStage(c.getInt(0)));
                c.moveToNext();
            }
            c.close();

        } catch (SQLiteException e){
            e.printStackTrace();
        }
    }
    public static List<Map.Entry<Long,Integer>> getScheduleList(SQLiteDatabase userDb, long untilDate){
        List<Map.Entry<Long,Integer>> scheduleList = new ArrayList<Map.Entry<Long, Integer>>();
        try{
            UserSettings userSettings = UserSettings.getInstance();
            int finalStage_id = userSettings.getStageCount() - 1;
            Cursor c = userDb.rawQuery("SELECT due_date, stage_id FROM user_kanji WHERE due_date < "
                    + Long.toString(untilDate) + " AND stage_id <>" + Integer.toString(finalStage_id)
                    +  " ORDER BY due_date ASC", null);
            c.moveToFirst();
            //Log.i("DatabaseQuery", "schedule query dump: " + DatabaseUtils.dumpCursorToString(c));
            c.moveToFirst();
            while(!c.isAfterLast()){
                Map.Entry<Long, Integer> entry = new AbstractMap.SimpleEntry<Long, Integer>(c.getLong(0),c.getInt(1));
                scheduleList.add(entry);
                c.moveToNext();
            }
            c.close();

            Log.i("DatabaseQuery", "scheduleList length: " + scheduleList.size());

        } catch (SQLiteException e ){
            e.printStackTrace();
        }
        return scheduleList;
    }
}
